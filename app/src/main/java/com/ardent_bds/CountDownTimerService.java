package com.ardent_bds;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import com.ardent_bds.ui.home.GrantTestDataModel;

public class CountDownTimerService extends Service {

    private final static String TAG = "BroadcastService";

    public static final String COUNTDOWN_BR = "timer coundown";
    Intent bi = new Intent(COUNTDOWN_BR);

    GrantTestDataModel grantTestDataModel;

    CountDownTimer cdt = null;

    @Override
    public void onCreate() {
        super.onCreate();

        grantTestDataModel = GrantTestDataModel.getInstance();
        Log.i(TAG, "Starting timer...");

        long l = Long.parseLong(grantTestDataModel.getTestTotalTiming());

        cdt = new CountDownTimer(l, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.i(TAG, "Countdown seconds remaining: " + millisUntilFinished / 1000);
                bi.putExtra("countdown", millisUntilFinished);
                sendBroadcast(bi);
            }

            @Override
            public void onFinish() {
                Log.i(TAG, "Timer finished");
            }
        };

        cdt.start();
    }

    @Override
    public void onDestroy() {

        cdt.cancel();
        Log.i(TAG, "Timer cancelled");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}