package com.ardent_bds;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.ardent_bds.constants.Constants;
import com.ardent_bds.interfaces.IOnBackPressed;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.model.VedioSubDataModule;
import com.ardent_bds.project_utils.HttpPostHandlerVideoUrl;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.VedioUrlModule;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AllNotesFragment extends Fragment implements OnPageChangeListener, OnLoadCompleteListener, IOnBackPressed {


    private static final String TAG = "All Notes";
    public static final String SAMPLE_FILE = "android_tutorial.pdf";
    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;
    String HttpPostVideoURL = Constants.BaseUrl + "/ArdentDB/getVideoFromChildId.php";
    VedioDataModule vedioDataModule = VedioDataModule.getInstance();
    TextView tv_header;
    VedioUrlModule vedioUrlModule;
    ArrayList<VedioUrlModule> notesUrlModulesArrayList = new ArrayList<>();
    private ProgressDialog progress_bar_grandTest;

    public AllNotesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View view = inflater.inflate(R.layout.fragment_tb_notes, container, false);

        pdfView = view.findViewById(R.id.pdfView);
        tv_header = view.findViewById(R.id.tv_header);


        //vedioUrlModule = VedioUrlModule.getInstance();

        ConnectivityManager ConnectionManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getVideoOfSelectedChildIDData();
        } else {
            Toast.makeText(getActivity(), "Network Not Available", Toast.LENGTH_LONG).show();
        }


        return view;
    }

    public void download(String pdfUrlData) {
        //new DownloadFile().execute("https://www.learningcontainer.com/wp-content/uploads/2019/09/sample-pdf-with-images.pdf", "ardentBds.pdf");
        new DownloadFile().execute(pdfUrlData, "ardentBds.pdf");

    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");
    }

    @Override
    public void onPageChanged(int page, int pageCount) {

        pageNumber = page;
        getActivity().setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }


    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public boolean OnBackPressed() {
        return false;
    }


    /*public void view(View v) {
        File pdfFile = new File(getActivity().getFilesDir() + "/ardentpdf/" + "samplepdf1.pdf");  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }*/

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf


            String path = Environment.getExternalStorageDirectory() + "/FBDownloader/";
            File dir = new File(path);
            boolean isDirectoryCreated = dir.exists();
            if (!isDirectoryCreated) {
                isDirectoryCreated = dir.mkdir();
            }
            if (isDirectoryCreated) {
                // do something\
                dir.delete();
                isDirectoryCreated = dir.mkdir();
                Log.d("Folder", "Already Created");
            }


            File storageDir1 = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/ardentpdf/");
            storageDir1.mkdirs(); //create folders where write files


            File pdfFile = new File(storageDir1, "ardentBds.pdf");
            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            File pdfFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/ardentpdf/" + "ardentBds.pdf");
            Uri outputFileUri = FileProvider.getUriForFile(getActivity(), "com.ardent_bds.myprovider", pdfFile);
            pdfView.fromUri(outputFileUri)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false) // to enable page swiping
                    .enableDoubletap(true) // to enable double tap
                    .defaultPage(0) // default page. this can be used to save user last page location
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .scrollHandle(null)
                    .load();

        }

    }


    private void getVideoOfSelectedChildIDData() {

        new GetVideoOfSelectedChildIdFromServer(getActivity()).execute();

    }


    public class GetVideoOfSelectedChildIdFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetVideoOfSelectedChildIdFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress_bar_grandTest = new ProgressDialog(getActivity());
            progress_bar_grandTest.setMessage("Please wait...");
            progress_bar_grandTest.setCancelable(false);
            progress_bar_grandTest.setIndeterminate(true);
            progress_bar_grandTest.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerVideoUrl httpPostHandlerVideoUrl = new HttpPostHandlerVideoUrl();
            String responseFromDB = httpPostHandlerVideoUrl.makeServicePostVideoUrlCall(HttpPostVideoURL);
            Log.e("Notes Screen ", "Notes response From DB: " + responseFromDB);
            notesUrlModulesArrayList.clear();
            if (responseFromDB != null) {
                notesUrlModulesArrayList = BaseActivity.getVideoListFromChildID(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            progress_bar_grandTest.dismiss();
            if (notesUrlModulesArrayList.size() > 0) {
                //tv_header.setText("" + notesUrlModulesArrayList.get(0).getModuleName());
                tv_header.setText("" + VedioSubDataModule.getInstance().getSelectedModuleName());
                download(notesUrlModulesArrayList.get(0).getPdfUrl());
            } else {
                /* Toast.makeText(getActivity(), "PDF Not Available", Toast.LENGTH_SHORT).show();*/
                OnBackPressed();
            }

        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
