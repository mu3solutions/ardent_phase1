package com.ardent_bds;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.ardent_bds.constants.Constants;
import com.ardent_bds.interfaces.IOnBackPressed;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.model.VedioSubDataModule;
import com.ardent_bds.project_utils.HttpPostHandlerVideoUrl;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.VedioUrlModule;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;

public class AllSlidesFragment extends Fragment implements IOnBackPressed {

    ViewPager viewPager;


    String HttpPostVideoURL = Constants.BaseUrl + "/ArdentDB/getVideoFromChildId.php";

    ArrayList<VedioUrlModule> slideUrlModulesArrayList = new ArrayList<>();

    VedioUrlModule slideUrlModule;
    VedioDataModule vedioDataModule = VedioDataModule.getInstance();

    TextView tv_header;
    ImageView imageView;
    private ProgressDialog progress_bar_grandTest;

    public AllSlidesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View viewAllSlides = inflater.inflate(R.layout.fragment_tb_slides, container, false);

        viewPager = viewAllSlides.findViewById(R.id.pager);

        tv_header = viewAllSlides.findViewById(R.id.tv_header_slider);

        ConnectivityManager ConnectionManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getVideoOfSelectedChildIDData();
        } else {
            Toast.makeText(getActivity(), "Network Not Available", Toast.LENGTH_LONG).show();
        }


        return viewAllSlides;
    }


    private void getVideoOfSelectedChildIDData() {

        new GetVideoOfSelectedChildIdFromServer(getActivity()).execute();

    }

    @Override
    public boolean OnBackPressed() {
        return false;
    }


    public class GetVideoOfSelectedChildIdFromServer extends AsyncTask<Void, Void, Void> {
        private int currentPage = 0;
        private int NUM_PAGES = 0;

        public Context context;


        public GetVideoOfSelectedChildIdFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progress_bar_grandTest = new ProgressDialog(getActivity());
            progress_bar_grandTest.setMessage("Please wait...");
            progress_bar_grandTest.setCancelable(false);
            progress_bar_grandTest.setIndeterminate(true);
            progress_bar_grandTest.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {


            HttpPostHandlerVideoUrl httpPostHandlerVideoUrl = new HttpPostHandlerVideoUrl();
            String responseFromDB = httpPostHandlerVideoUrl.makeServicePostVideoUrlCall(HttpPostVideoURL);
            if (responseFromDB != null) {
                Log.e("Sub module data ", "Submodule data responseFromDB: " + responseFromDB);
                slideUrlModulesArrayList.clear();
                slideUrlModulesArrayList = BaseActivity.getVideoListFromChildID(responseFromDB);
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            progress_bar_grandTest.dismiss();
            if (slideUrlModulesArrayList.size() > 0) {
                //tv_header.setText("" + slideUrlModulesArrayList.get(0).getModuleName());

                tv_header.setText("" + VedioSubDataModule.getInstance().getSelectedModuleName());
                Log.i("Slider images", "" + slideUrlModulesArrayList.get(0).getSlideUrl());
                String[] slideUrlArray = null;
                slideUrlArray = slideUrlModulesArrayList.get(0).getSlideUrl().split(",");
                System.out.println("MA" + slideUrlArray);
                SlideCustomPagerAdapter slideCustomPagerAdapter = new SlideCustomPagerAdapter(getActivity(), slideUrlArray);
                viewPager.setAdapter(slideCustomPagerAdapter);

                CirclePageIndicator indicator = (CirclePageIndicator) getActivity().findViewById(R.id.indicator);

                indicator.setViewPager(viewPager);

                final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
                indicator.setRadius(5 * density);

                NUM_PAGES =slideUrlModulesArrayList.size();

                // Auto start of viewpager
                final Handler handler = new Handler();
                final Runnable Update = new Runnable() {
                    public void run() {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                        }
                        viewPager.setCurrentItem(currentPage++, true);
                    }
                };


                // Pager listener over indicator
                indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                    @Override
                    public void onPageSelected(int position) {
                        currentPage = position;

                    }

                    @Override
                    public void onPageScrolled(int pos, float arg1, int arg2) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int pos) {

                    }
                });

            } else {
                OnBackPressed();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
