package com.ardent_bds.project_utils;

import android.util.Log;

import com.ardent_bds.model.UserModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;

public class HttpPostHandler {
    private static final String TAG = HttpPostHandler.class.getSimpleName();


    private String responseStr;
    UserModel userModel;

    public HttpPostHandler() {
    }

    public String makeServicePostCall(String reqPostUrl) {

        userModel = UserModel.getInstance();
        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);

            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("useraccountid", userModel.getUserId()));
            nameValuePairs.add(new BasicNameValuePair("username", userModel.getUserName()));
            nameValuePairs.add(new BasicNameValuePair("useremail", userModel.getUserEmailId()));
            nameValuePairs.add(new BasicNameValuePair("userpassword", userModel.getUserPassword()));
            nameValuePairs.add(new BasicNameValuePair("usermobile", userModel.getUserMobileNo()));
            nameValuePairs.add(new BasicNameValuePair("userloginchannel", userModel.getUserLoggedInChannel()));
            nameValuePairs.add(new BasicNameValuePair("userstatus", userModel.getUserStatus()));
            nameValuePairs.add(new BasicNameValuePair("userlastlogin", userModel.getUserlastLoginTime()));
            nameValuePairs.add(new BasicNameValuePair("userprofilepic", userModel.getUserProfilePic()));
            nameValuePairs.add(new BasicNameValuePair("useracadamicyear", userModel.getUserAcadamicYear()));
            nameValuePairs.add(new BasicNameValuePair("userstate", userModel.getUserState()));
            nameValuePairs.add(new BasicNameValuePair("usercollege", userModel.getUserCollegeName()));
            nameValuePairs.add(new BasicNameValuePair("useraccesslevel", String.valueOf(userModel.getUserAccessLevel())));


            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "Response hello: " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }
}
