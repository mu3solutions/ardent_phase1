package com.ardent_bds.project_utils;

import android.util.Log;

import com.ardent_bds.model.DataModelTemp;
import com.ardent_bds.ui.home.GrantTestDataModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;

public class HttpPostHandlerGrandTestUpdate {
    private static final String TAG = HttpPostHandlerGrandTestUpdate.class.getSimpleName();
    private String responseStr;

    GrantTestDataModel grantTestDataModel;

    public HttpPostHandlerGrandTestUpdate() {
    }

    public String makeServicePostCall(String reqPostUrl) {


        grantTestDataModel = GrantTestDataModel.getInstance();

        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);

            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("completed_status", "1"));
            nameValuePairs.add(new BasicNameValuePair("testid", "" + grantTestDataModel.getSelectedTestId()));
            nameValuePairs.add(new BasicNameValuePair("total_correct_answers", "" + grantTestDataModel.getCorrectAnswerMap()));
            nameValuePairs.add(new BasicNameValuePair("completed_answer_list", "" + grantTestDataModel.getCorrectString()));
            nameValuePairs.add(new BasicNameValuePair("completed_selected_answer_list", "" + grantTestDataModel.getActualCorrectString()));
            nameValuePairs.add(new BasicNameValuePair("total_skipped_data", "" + grantTestDataModel.getSkippedString()));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post requestGrant Test data responseFromDB
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                    Log.v(TAG, "Update Grand test status http Response " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }
}
