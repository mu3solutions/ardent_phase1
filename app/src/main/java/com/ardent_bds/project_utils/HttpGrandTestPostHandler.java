package com.ardent_bds.project_utils;

import android.util.Log;

import com.ardent_bds.ui.home.GrantTestDataModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;

public class HttpGrandTestPostHandler {
    private static final String TAG = HttpGrandTestPostHandler.class.getSimpleName();
    private String responseStr;

    public HttpGrandTestPostHandler() {
    }

    public String makeServicePostCall(String reqPostUrl) {
        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);

            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

            Log.i("getSelectedTestId())122222", "" + GrantTestDataModel.getInstance().getSelectedTestId());


            nameValuePairs.add(new BasicNameValuePair("testId", "" + GrantTestDataModel.getInstance().getSelectedTestId()));

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);


            int responseCode = responses.getStatusLine().getStatusCode();
            switch (responseCode) {
                case 200:
                    HttpEntity entity = responses.getEntity();
                    if (entity != null) {
                        String responseBody = EntityUtils.toString(entity);
                        responseStr = responseBody;
                        Log.v(TAG, "Response hello: 4444555 " + responseStr);
                    }
                    break;

            }

            /*HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "Response hello: " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }*/
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }
}
