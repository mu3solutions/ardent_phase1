package com.ardent_bds.project_utils;

import android.util.Log;

import com.ardent_bds.model.UserModel;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.model.VedioSubDataModule;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;

public class HttpPostHandlerVideoUrl {
    private static final String TAG = HttpPostHandlerVideoUrl.class.getSimpleName();


    private String responseStr;
    UserModel userModel;
    VedioSubDataModule vedioSubDataModule;
    VedioDataModule vedioDataModule;

    public HttpPostHandlerVideoUrl() {
    }

    public String makeServicePostVideoUrlCall(String reqPostUrl) {

        userModel = UserModel.getInstance();
        vedioDataModule = VedioDataModule.getInstance();

        vedioSubDataModule = VedioSubDataModule.getInstance();
        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);

            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("uv_child_id", String.valueOf(vedioSubDataModule.getChildID())));
            nameValuePairs.add(new BasicNameValuePair("sub_module_id", String.valueOf(vedioDataModule.getModuleId())));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "All submodule http Response " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }

    public String makeServiceUpdateCompleteStatus(String reqPostUrl) {
        userModel = UserModel.getInstance();
        vedioDataModule = VedioDataModule.getInstance();

        vedioSubDataModule = VedioSubDataModule.getInstance();
        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);
            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("uv_child_id", String.valueOf(vedioSubDataModule.getChildID())));
            nameValuePairs.add(new BasicNameValuePair("module_id", String.valueOf(vedioDataModule.getModuleId())));
            nameValuePairs.add(new BasicNameValuePair("user_id", String.valueOf(userModel.getUserId())));
            nameValuePairs.add(new BasicNameValuePair("uv_watched_status", "1"));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "All submodule http Response " + responseStr);
                Log.v(TAG, "Bala TEs " + vedioSubDataModule.getChildID()+" "+vedioDataModule.getModuleId()+" "+userModel.getUserId());
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }

}
