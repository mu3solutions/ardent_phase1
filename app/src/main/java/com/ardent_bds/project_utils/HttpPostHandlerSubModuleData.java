package com.ardent_bds.project_utils;

import android.util.Log;

import com.ardent_bds.model.McqModuleData;
import com.ardent_bds.model.McqSubModleData;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;

public class HttpPostHandlerSubModuleData {
    private static final String TAG = HttpPostHandlerSubModuleData.class.getSimpleName();


    private String responseStr;
    UserModel userModel;
    VedioDataModule videoDataModule;

    McqSubModleData mcqSubModleData;
    QuestionDataModel questionDataModel;

    public HttpPostHandlerSubModuleData() {
    }

    public String makeServicePostCall(String reqPostUrl) {

        userModel = UserModel.getInstance();

        videoDataModule = VedioDataModule.getInstance();
        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);

            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("module_id", String.valueOf(videoDataModule.getModuleId())));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "All submodule http Response " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }

    public String makeServiceUpdateCall(String reqPostUrl) {

        mcqSubModleData = McqSubModleData.getInstance();
        questionDataModel = QuestionDataModel.getInstance();


        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);

            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("completed_status", "completed"));
            nameValuePairs.add(new BasicNameValuePair("child_module_id", String.valueOf(mcqSubModleData.getChildModuleId())));
            nameValuePairs.add(new BasicNameValuePair("score", String.valueOf(mcqSubModleData.getScore())));
            nameValuePairs.add(new BasicNameValuePair("score_percent", String.valueOf(mcqSubModleData.getScorePercent())));
            //this vale is used only wen calling update
            nameValuePairs.add(new BasicNameValuePair("selected_answers_list", String.valueOf(questionDataModel.getMcqSelectedAnswerList())));
            nameValuePairs.add(new BasicNameValuePair("user_id", String.valueOf(UserModel.getInstance().getUserId())));
            nameValuePairs.add(new BasicNameValuePair("uta_module_id", String.valueOf(McqModuleData.getInstance().getModuleId())));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "Update Grand test status http Response " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }

}
