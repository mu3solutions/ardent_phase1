package com.ardent_bds.tabs;


import android.view.WindowManager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ardent_bds.AllNotesFragment;
import com.ardent_bds.AllSlidesFragment;
import com.ardent_bds.AllVideosFragment;

public class PagerAdapter extends FragmentPagerAdapter {

    int numberOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.numberOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                AllVideosFragment tab1 = new AllVideosFragment();
                return tab1;
            case 1:
                AllSlidesFragment tab2 = new AllSlidesFragment();
                return tab2;
            case 2:
                AllNotesFragment tab3 = new AllNotesFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
