package com.ardent_bds.tabs;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.customviews.LockableViewPager;
import com.ardent_bds.ui.VedioMainActivity;
import com.google.android.material.tabs.TabLayout;

public class AllVideosTabsActivity extends AppCompatActivity {

    LockableViewPager lockableVewPager;

    int value;
    String orientation;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.tb_main_activity);
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Videos Class"));
        tabLayout.addTab(tabLayout.newTab().setText("Slides"));
        tabLayout.addTab(tabLayout.newTab().setText("Notes"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(ColorStateList.valueOf(getColor(R.color.white)));

        // final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        lockableVewPager = (LockableViewPager) findViewById(R.id.pager);
        lockableVewPager.setSwipable(false);

/*
        value = getResources().getConfiguration().orientation;

        if (value == Configuration.ORIENTATION_PORTRAIT) {

            orientation = "Portrait";
        }

        if (value == Configuration.ORIENTATION_LANDSCAPE) {

            orientation = "Landscape";
        }


        if(orientation == "Landscape"){
            tabLayout.setVisibility(View.GONE);
        }
        else{
            tabLayout.setVisibility(View.VISIBLE);
        }


*/



        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        lockableVewPager.setAdapter(adapter);
        lockableVewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                lockableVewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(AllVideosTabsActivity.this, VedioMainActivity.class));
    }
}
