package com.ardent_bds.ui.registeration;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.adapters.StatesRecylerAdapter;
import com.ardent_bds.model.CoutryStateDataModel;

import java.util.ArrayList;

public class RegistrationActivity extends AppCompatActivity {
    ArrayList<CoutryStateDataModel> states;
    CoutryStateDataModel coutryStateDataModel;
    TextView header, conformButton;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_registration);

        states = new ArrayList<>();
        generateAdapterData();

        header = findViewById(R.id.header_text);
        conformButton = findViewById(R.id.conform_button);
        conformButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < states.size(); i++) {
                    if (states.get(i).isSelected()) {
                        //Toast.makeText(RegistrationActivity.this, "Hi " + states.get(i).getName(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), StatesActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
        recyclerView = findViewById(R.id.aca_year_recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new StatesRecylerAdapter(states);
        recyclerView.setAdapter(adapter);
    }

    private void generateAdapterData() {
        coutryStateDataModel = new CoutryStateDataModel();
        coutryStateDataModel.setId(1);
        coutryStateDataModel.setName("B.D.S First Year");
        coutryStateDataModel.setSelected(false);
        states.add(coutryStateDataModel);

        coutryStateDataModel = new CoutryStateDataModel();
        coutryStateDataModel.setId(2);
        coutryStateDataModel.setName("B.D.S Second Year");
        coutryStateDataModel.setSelected(false);
        states.add(coutryStateDataModel);

        coutryStateDataModel = new CoutryStateDataModel();
        coutryStateDataModel.setId(3);
        coutryStateDataModel.setName("B.D.S Third Year");
        coutryStateDataModel.setSelected(false);
        states.add(coutryStateDataModel);

        coutryStateDataModel = new CoutryStateDataModel();
        coutryStateDataModel.setId(4);
        coutryStateDataModel.setName("B.D.S Final Year");
        coutryStateDataModel.setSelected(false);
        states.add(coutryStateDataModel);

        coutryStateDataModel = new CoutryStateDataModel();
        coutryStateDataModel.setId(5);
        coutryStateDataModel.setName("B.D.S Intern");
        coutryStateDataModel.setSelected(false);
        states.add(coutryStateDataModel);
    }
}