package com.ardent_bds.ui.registeration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ardent_bds.R;
import com.ardent_bds.adapters.StatesRecylerAdapter;
import com.ardent_bds.model.CoutryStateDataModel;
import com.ardent_bds.ui.BaseActivity;

import java.util.ArrayList;

public class StatesActivity extends BaseActivity {
    ArrayList<CoutryStateDataModel> states;
    CoutryStateDataModel coutryStateDataModel;
    TextView header, conformButton;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    BaseActivity baseActivity;
    private ArrayList<String> allStateList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_states);

        states = new ArrayList<>();
        generateStateListData();

        header = findViewById(R.id.header_text);
        conformButton = findViewById(R.id.conform_button);
        conformButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i=0; i<states.size(); i++){
                    if (states.get(i).isSelected()){
                       // Toast.makeText(StatesActivity.this, "Hi " + states.get(i).getName(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), CollegeActivity.class);
                        intent.putExtra("stateName",states.get(i).getName());
                        startActivity(intent);
                    }
                }
            }
        });
        recyclerView = findViewById(R.id.states_recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new StatesRecylerAdapter(states);
        recyclerView.setAdapter(adapter);
    }

    private void generateStateListData() {
        allStateList = getStateList();
        for (int i=0; i < allStateList.size(); i++){
            coutryStateDataModel = new CoutryStateDataModel();
            coutryStateDataModel.setId(i+1);
            coutryStateDataModel.setName(allStateList.get(i));
            coutryStateDataModel.setSelected(false);
            states.add(coutryStateDataModel);
        }
    }
}