package com.ardent_bds.ui;


public class UserAccessLevelModel {

    private static UserAccessLevelModel instance = null;

    public static UserAccessLevelModel getInstance() {
        if (instance == null) {
            instance = new UserAccessLevelModel();
        }
        return instance;
    }


    public String getUpdatedUserAccessLevel() {
        return updatedUserAccessLevel;
    }

    public void setUpdatedUserAccessLevel(String updatedUserAccessLevel) {
        this.updatedUserAccessLevel = updatedUserAccessLevel;
    }

    private String updatedUserAccessLevel;

}
