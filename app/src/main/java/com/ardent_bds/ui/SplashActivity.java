package com.ardent_bds.ui;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;

import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.project_utils.Utils;

import static com.ardent_bds.ui.OnBoardingActivity.PREF_USER_FIRST_TIME;


public class SplashActivity extends BaseActivity {
    private boolean isUserFirstTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_splash);

        byte[] sha1 = {
                0x65, 0x5D, 0x66, (byte)0xA1, (byte)0xC9, 0x31, (byte) 0x85, (byte)0xAB, (byte)0x92, (byte)0xC6, (byte)0xA2, 0x60, (byte) 0x87, 0x5B, 0x1A, (byte)0xDA, 0x45, 0x6E, (byte)0x97, (byte)0xEA
        };
        System.out.println("keyhashGooglePlaySignIn:"+ Base64.encodeToString(sha1, Base64.NO_WRAP));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //check if the user is its first launch and set Preferences
                isUserFirstTime = Boolean.valueOf(Utils.readSharedSetting(SplashActivity.this, PREF_USER_FIRST_TIME, "true"));
                Intent introIntent = new Intent(SplashActivity.this, OnBoardingActivity.class);
                introIntent.putExtra(PREF_USER_FIRST_TIME, isUserFirstTime);
                if (isUserFirstTime) {
                    startActivity(introIntent);
                } else {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                }
                finish();
            }
        }, 3000);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }


}