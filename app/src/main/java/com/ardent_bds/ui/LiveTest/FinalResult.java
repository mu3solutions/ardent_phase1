package com.ardent_bds.ui.LiveTest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.GrantTestUserDataModel;
import com.ardent_bds.ui.home.GrantTestDataModel;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FinalResult extends Activity {


    TextView grand_test_name, rank_position, total_rank, total_score,
            max_score_text, percentage, skipped_pgbar1_value,
            correct_pgbar1_value,
            wrong_pgbar1_value;

    CardView cv_rank_user;
    public TextView user_total_score, current_user_score, my_tv_correct, my_tv_wrong, my_tv_skip, u_rank_position, current_userNameTv, grand_test_taken_on;
    String selected_test_name, total_questions;
    int test_id;
    HashMap<Integer, GrantTestUserDataModel> allGrandTestListUserHP;

    //data based on testid and userid(current user)
    String HttpGetURL_Grant_Test_Complete_data = Constants.BaseUrl + "/ArdentDB/individual_user_test_id.php";

    //all user details for that test id
    String HttpGetURL_Grant_Test_Final_Result = Constants.BaseUrl + "/ArdentDB/user_final_result.php";

    List<GrantTestUserDataModel> allGrandTestListCurrentUser;
    List<GrantTestUserDataModel> allGrandTestListUserFullData;


    HashMap<String, String> allUsersMarksDetails;

    String myCurrentUserMarks;

    private RecyclerView recyclerViewRating;

    ShimmerFrameLayout shimmer_layout_rank,shimmer_layout_chart;

    RelativeLayout relativeLayout;
    RecyclerView.Adapter recyclerViewAdapter;
    RecyclerView.LayoutManager recylerViewLayoutManager;


    LinearLayout rank_llp,linearLayout;
    ArrayList<String> allFullDataFilter;
    ArrayList<Integer> allFullDataFilterCurrentUser;

    private ImageView close;
    private ProgressDialog progress_bar_grandTest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.grand_test_result);

        progress_bar_grandTest = new ProgressDialog(FinalResult.this);
        progress_bar_grandTest.setMessage("Please wait...");
        progress_bar_grandTest.setCancelable(false);
        progress_bar_grandTest.setIndeterminate(true);

        current_userNameTv = findViewById(R.id.current_userNameTv);
        shimmer_layout_rank = findViewById(R.id.shimmer_layout_rank);
        shimmer_layout_chart = findViewById(R.id.shimmer_layout_chart);
        shimmer_layout_rank.startShimmerAnimation();
        shimmer_layout_chart.startShimmerAnimation();
        rank_llp = findViewById(R.id.rank_ll);
        linearLayout = findViewById(R.id.linearLayout);
        close = findViewById(R.id.close);
        grand_test_taken_on = findViewById(R.id.grand_test_taken_on);
        grand_test_name = findViewById(R.id.grand_test_name);
        rank_position = findViewById(R.id.rank_position);
        total_rank = findViewById(R.id.total_rank);
        total_score = findViewById(R.id.total_score);
        max_score_text = findViewById(R.id.max_score_text);
        percentage = findViewById(R.id.percentage);
        skipped_pgbar1_value = findViewById(R.id.skipped_pgbar1_value);
        correct_pgbar1_value = findViewById(R.id.correct_pgbar1_value);
        wrong_pgbar1_value = findViewById(R.id.wrong_pgbar1_value);
        cv_rank_user = findViewById(R.id.cv_rank_user);

        allFullDataFilter = new ArrayList<>();
        allFullDataFilterCurrentUser = new ArrayList<>();
        user_total_score = (TextView) findViewById(R.id.user_total_score);
        current_user_score = (TextView) findViewById(R.id.user_total_score);
        my_tv_correct = (TextView) findViewById(R.id.user_tv_correct);
        my_tv_wrong = (TextView) findViewById(R.id.user_tv_wrong);
        my_tv_skip = (TextView) findViewById(R.id.user_tv_skip);
        u_rank_position = (TextView) findViewById(R.id.u_rank_position);

        allUsersMarksDetails = new HashMap<String, String>();
        allGrandTestListUserHP = new HashMap<>();
        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getUserCompletedModulesDetails();
        } else {

            Toast.makeText(FinalResult.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


        Bundle bundle = getIntent().getExtras();
        selected_test_name = bundle.getString("selected_test_name_1");
        total_questions = bundle.getString("total_questions");
        test_id = bundle.getInt("test_id");



        /*GrantTestUserDataModel.getInstance().setGtName(grandTestAraryList.get(position).getTestName());
        GrantTestDataModel.getInstance().setTestName(grandTestAraryList.get(position).getTestName());
*/
        if (GrantTestDataModel.getInstance() != null) {
            if (GrantTestDataModel.getInstance().getTestName() == null) {
                grand_test_name.setText("" + GrantTestUserDataModel.getInstance().getGtName());
            } else {
                grand_test_name.setText("" + GrantTestDataModel.getInstance().getTestName());
            }
        } else {
            grand_test_name.setText("");
        }

        //GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));

        if (test_id == 0) {
            GrantTestUserDataModel.getInstance().setUserTestId(GrantTestUserDataModel.getInstance().getUserTestId());
        } else {
            GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(test_id));
        }


        int selected_test_answersInt = bundle.getInt("selected_test_answers");
        //skipped_answersCount = bundle.getInt("skipped_answers");
        int skipped_answersCount = GrantTestDataModel.getInstance().getSkippedListCompletedGrandTest();


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(FinalResult.this, GrandTestLandingActivity.class));
            }
        });


    }
    //


    private void getUserCompletedModulesDetails() {
        new GetUserCompletedGrantTestData(this).execute();
    }


    public class GetUserCompletedGrantTestData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetUserCompletedGrantTestData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progress_bar_grandTest.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostFinalResultsCurrentUserTestDetails httpPostHandler = new HttpPostFinalResultsCurrentUserTestDetails();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Grant_Test_Complete_data);
            Log.e("Grant Test>>", "Grant Test data responseFromDB: " + responseFromDB);
            if (responseFromDB != null) {
                allGrandTestListCurrentUser = BaseActivity.getGrandTestCompletedBasedOnUser(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected() == true) {
                getAllUsersData();
            } else {

                Toast.makeText(FinalResult.this, "Network Not Available", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void getAllUsersData() {
        new GetAllUserCompletedGrantTestData(this).execute();
    }


    public class GetAllUserCompletedGrantTestData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllUserCompletedGrantTestData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostGetAllUserResultData httpPostHandler = new HttpPostGetAllUserResultData();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Grant_Test_Final_Result);
            Log.e("Grant Test>>", "Grant Test data responseFromDB: " + responseFromDB);
            if (responseFromDB != null) {
                allGrandTestListUserFullData = BaseActivity.getAllUsersGTData(responseFromDB);
            }

          /*  //Collections.sort(allGrandTestListUserFullData,Collections.reverseOrder());

            Collections.sort(allGrandTestListUserFullData, new Comparator<GrantTestUserDataModel>() {
                @Override
                public int compare(GrantTestUserDataModel lhs, GrantTestUserDataModel rhs) {
                    return lhs.getFinalScore().compareTo(rhs.getFinalScore());
                }
            });*/


        /*    for (int i = 0; i < allGrandTestListUserFullData.size(); i++) {
                if(allGrandTestListCurrentUser.get(i).getFinalScore().equals())
                allGrandTestListUserHP.put(Integer.valueOf(allGrandTestListUserFullData.get(i).getUserTestId()) - 1, allGrandTestListUserFullData.get(i));
            }*/


/*
            HashSet hs = new HashSet();
            for (int i = 0; i < allGrandTestListUserFullData.size(); i++) {
                hs.addAll(Collections.singleton(allGrandTestListUserFullData.get(i).getFinalScore())); // demoArrayList= name of arrayList from which u want to remove duplicates
            }
            allFullDataFilter.clear();
            allFullDataFilter.addAll(hs);*/
            // Collections.sort(allFullDataFilter, Collections.reverseOrder());


            for (int i = 0; i < allGrandTestListUserFullData.size(); i++) {
                if (allGrandTestListUserFullData.get(i).getFinalScore().equals(allGrandTestListCurrentUser.get(0).getFinalScore())) {
                    allFullDataFilterCurrentUser.add(Integer.parseInt(String.valueOf(i)) + Integer.parseInt("1"));
                }
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            //progress_bar_grandTest.dismiss();
            if (allGrandTestListCurrentUser != null && allGrandTestListCurrentUser.size() > 0) {


                /*shimmer_layout_rank.stopShimmerAnimation();
                shimmer_layout_rank.setVisibility(View.GONE);
                rank_llp.setVisibility(View.VISIBLE);
*/
                shimmer_layout_chart.stopShimmerAnimation();
                shimmer_layout_chart.setVisibility(View.GONE);
                linearLayout.setVisibility(View.VISIBLE);
                cv_rank_user.setVisibility(View.VISIBLE);
                shimmer_layout_rank.stopShimmerAnimation();
                shimmer_layout_rank.setVisibility(View.GONE);
                rank_llp.setVisibility(View.VISIBLE);

                int finalTotal = allGrandTestListCurrentUser.get(0).getSkipCount() + allGrandTestListCurrentUser.get(0).getCorrectCount() + allGrandTestListCurrentUser.get(0).getWrongCount();
                //grand_test_name.setText("" + selected_test_name);
                current_userNameTv.setText("" + UserModel.getInstance().getUserName());
                total_score.setText("" + allGrandTestListCurrentUser.get(0).getCorrectCount());

                max_score_text.setText("Total Score out of " + finalTotal);
                skipped_pgbar1_value.setText("" + allGrandTestListCurrentUser.get(0).getSkipCount());
                correct_pgbar1_value.setText("" + allGrandTestListCurrentUser.get(0).getCorrectCount());
                wrong_pgbar1_value.setText("" + allGrandTestListCurrentUser.get(0).getWrongCount());

                //double percentile = finalTotal - Integer.parseInt(allGrandTestListCurrentUser.get(0).getFinalScore());

                //percentage.setText("" + String.format("%.2d", percentile));
                percentage.setText("" + allGrandTestListCurrentUser.get(0).getPercentage() + " Percentile");


                grand_test_taken_on.setText("" + allGrandTestListCurrentUser.get(0).getTestTakenOn());
                user_total_score.setText("" + allGrandTestListCurrentUser.get(0).getFinalScore());
                current_user_score.setText("" + allGrandTestListCurrentUser.get(0).getFinalScore());
                my_tv_correct.setText("" + allGrandTestListCurrentUser.get(0).getCorrectCount());
                my_tv_wrong.setText("" + allGrandTestListCurrentUser.get(0).getWrongCount());
                my_tv_skip.setText("" + allGrandTestListCurrentUser.get(0).getSkipCount());

                //u_rank_position.setText("" + allFullDataFilterCurrentUser.get(0) + " st");


                int p = allFullDataFilterCurrentUser.get(0);

                if (p == 1) {
                    cv_rank_user.setVisibility(View.GONE);
                    u_rank_position.setText("" + allFullDataFilterCurrentUser.get(0) + " st");
                } else if (p == 2) {
                    cv_rank_user.setVisibility(View.GONE);
                    u_rank_position.setText("" + allFullDataFilterCurrentUser.get(0) + " nd");
                } else if (p == 3) {
                    cv_rank_user.setVisibility(View.GONE);
                    u_rank_position.setText("" + allFullDataFilterCurrentUser.get(0) + " rd");
                } else {
                    u_rank_position.setText("" + allFullDataFilterCurrentUser.get(0) + " th");
                }



                /*for (int i = 0; i < allGrandTestListUserFullData.size(); i++) {
                    allUsersMarksDetails.put(allGrandTestListUserFullData.get(i).getUserID(), allGrandTestListUserFullData.get(i).getMarksTotal());
                    //allGrandTestListUserFullData.get(0).getMarksTotal()
                }*/

                recyclerViewRating = (RecyclerView) findViewById(R.id.rv_ranking);
                recylerViewLayoutManager = new LinearLayoutManager(context);
                recyclerViewRating.setLayoutManager(recylerViewLayoutManager);
                recyclerViewAdapter = new RecyclerViewRankingAdapter(this, allGrandTestListCurrentUser, allGrandTestListUserFullData, allFullDataFilterCurrentUser.get(0));

                recyclerViewRating.setAdapter(recyclerViewAdapter);
                myCurrentUserMarks = allGrandTestListCurrentUser.get(0).getFinalScore();
                total_rank.setText("" + allGrandTestListUserFullData.size());
                rank_position.setText("" + allFullDataFilterCurrentUser.get(0));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FinalResult.this, GrandTestLandingActivity.class));
    }
}
