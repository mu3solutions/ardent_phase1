package com.ardent_bds.ui.LiveTest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.project_utils.HttpHandler;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.GrantTestUserDataModel;
import com.ardent_bds.ui.MainActivity;
import com.ardent_bds.ui.UserAccessLevelModel;
import com.ardent_bds.ui.gallery.AboutUsActivity;
import com.ardent_bds.ui.gallery.AllVideosSlidesNotesActivity;
import com.ardent_bds.ui.gallery.AllViedosSlidesFragment;
import com.ardent_bds.ui.gallery.ContactUsActivity;
import com.ardent_bds.ui.gallery.PaymentHistory;
import com.ardent_bds.ui.gallery.SubscriptionActivity;
import com.ardent_bds.ui.home.GrantTestDataModel;
import com.ardent_bds.ui.mcq_test_module.MCQTest;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.ardent_bds.ui.BaseActivity.getUserData;

public class GrandTestLandingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView recyclerViewGrandRV;
    private AppBarConfiguration mAppBarConfiguration;

    List<GrantTestDataModel> allGrandTestList;
    List<GrantTestUserDataModel> allGrandTestListUser;
    HashMap<Integer, GrantTestUserDataModel> allGrandTestListUserHP;
    List<UserAccessLevelModel> userAccessLevelList;

    //String HttpGetURL_Grant_Test = Constants.BaseUrl +"/ArdentDB/suggested_test.php";
    String HttpGetURL_Grant_Test = Constants.BaseUrl + "/ArdentDB/suggested_test.php";
    String HttpGetURL_Updated_User_details = Constants.BaseUrl + "/ArdentDB/getUpdatedUserDetails.php";
    String HttpGetURL_Grant_Test_Complete_data = Constants.BaseUrl + "/ArdentDB/individual_user_details.php";


    MeowBottomNavigation bottomNavigation;

    AllViedosSlidesFragment allViedosSlidesFragment;
    private ProgressDialog progress_bar_login;
    ImageView menu;

    DrawerLayout drawer;
    private ImageView menu_slider_right;
    UserModel userModel;
    TextView userName, userEmailId;
    ImageView userImage;
    String TAG = getClass().getSimpleName();

    ArrayList<UserAccessLevelModel> userAccessList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.grandtest_landing_1);
        recyclerViewGrandRV = findViewById(R.id.recyclerViewGrandRV);
        drawer = findViewById(R.id.drawer_layout_grad_test);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        userAccessList = new ArrayList<>();
        allGrandTestListUser = new ArrayList<>();
        allGrandTestListUserHP = new HashMap<>();
        userModel = UserModel.getInstance();

        View nav = navigationView.getHeaderView(0);


        userName = nav.findViewById(R.id.userName);
        userEmailId = nav.findViewById(R.id.userEmailId);
        userImage = nav.findViewById(R.id.userImage);


        userName.setText("" + userModel.getUserName());
        userEmailId.setText("" + userModel.getUserEmailId());


        /*GrantTestDataModel.getInstance().setSelectedTestId(0);
        if (UserModel.getInstance().getUserProfilePic().equalsIgnoreCase("")) {
        } else {
            Glide.with(GrandTestLandingActivity.this)
                    .load(userModel.getUserProfilePic())
                    .into(userImage);
        }*/

       /* if (userModel.getUserProfilePic().equalsIgnoreCase("")) {
        } else {
            userImage.setImageURI(Uri.parse(userModel.getUserProfilePic()));
            //userImage.setImageBitmap(BitmapFactory.decodeFile(userModel.getUserProfilePic()));
            Uri userProfilePic = Uri.parse(userModel.getUserProfilePic());
            if (userProfilePic != null) {
                new BaseActivity.ImageLoadTask(userProfilePic.toString(), userImage).execute();
            }
        }*/


        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getUpdatedUserData();
            getGrantTestData();
        } else {
            Toast.makeText(GrandTestLandingActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }
        menu_slider_right = findViewById(R.id.menu_slider_right);


        menu_slider_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT, true);
            }
        });


        bottomNavigation = findViewById(R.id.bottom_nav);
        bottomViewNavigation();


//        getUpdatedUserData();
    }

    private void getUserCompletedModulesDetails() {
        new GetUserCompletedGrantTestData(this).execute();
    }


    public class GetUserCompletedGrantTestData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetUserCompletedGrantTestData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostGetAllUserData httpPostHandler = new HttpPostGetAllUserData();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Grant_Test_Complete_data);
            Log.e("Grant Test>>5555555", "Grant Test data responseFromDB: " + responseFromDB);
            if (responseFromDB != null) {
                allGrandTestListUser = BaseActivity.getGrandTestCompletedBasedOnUser(responseFromDB);

                for (int i = 0; i < allGrandTestListUser.size(); i++) {
                    allGrandTestListUserHP.put(Integer.valueOf(allGrandTestListUser.get(i).getUserTestId()) - 1, allGrandTestListUser.get(i));
                }
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            recyclerViewGrandRV.setHasFixedSize(true);
            recyclerViewGrandRV.setLayoutManager(new LinearLayoutManager(GrandTestLandingActivity.this));
            GrandTestAdapter adapter = new GrandTestAdapter(GrandTestLandingActivity.this, allGrandTestList, UserAccessLevelModel.getInstance().getUpdatedUserAccessLevel(), allGrandTestListUserHP);
            recyclerViewGrandRV.setAdapter(adapter);

            adapter.notifyDataSetChanged();


        }
    }


    private void getGrantTestData() {

        new GetAllGrantTestData(this).execute();
    }


    public class GetAllGrantTestData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllGrantTestData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler httpPostHandler = new HttpHandler();
            String responseFromDB = httpPostHandler.makeServiceCall(HttpGetURL_Grant_Test);
            Log.e("Grant Test>>", "Grant Test data responseFromDB: " + responseFromDB);
            if (responseFromDB != null) {
                allGrandTestList = new ArrayList<>();

                if (responseFromDB != null) {
                    allGrandTestList = BaseActivity.getDashboardGrantTestData(responseFromDB);
                }
            }
            return null;
        }

        protected void onPostExecute(Void result) {
         /*   recyclerViewGrandRV.setHasFixedSize(true);
            recyclerViewGrandRV.setLayoutManager(new LinearLayoutManager(GrandTestLandingActivity.this));
            GrandTestAdapter adapter = new GrandTestAdapter(GrandTestLandingActivity.this, allGrandTestList, UserAccessLevelModel.getInstance().getUpdatedUserAccessLevel());
            recyclerViewGrandRV.setAdapter(adapter);

            adapter.notifyDataSetChanged();*/


            ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected() == true) {
                getUserCompletedModulesDetails();
            } else {

                Toast.makeText(GrandTestLandingActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
            }


        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
        startActivity(new Intent(GrandTestLandingActivity.this, DashBoardActivity.class));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_payment) {
            overridePendingTransition(0, 0);
            Intent i1 = new Intent(this, PaymentHistory.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i1.putExtras(bundle);
            startActivity(i1);

        } else if (id == R.id.nav_subscribe) {
            //startActivity(new Intent(this, SubscriptionActivity.class));
            overridePendingTransition(0, 0);
            Intent i2 = new Intent(this, SubscriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i2.putExtras(bundle);
            startActivity(i2);

        } else if (id == R.id.nav_bookmark) {

        } else if (id == R.id.nav_share_app) {
            shareApp();

        } else if (id == R.id.nav_help) {
            //startActivity(new Intent(this, ContactUsActivity.class));
            overridePendingTransition(0, 0);
            Intent i3 = new Intent(this, ContactUsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i3.putExtras(bundle);
            startActivity(i3);


        } else if (id == R.id.nav_menu_about_us) {
            //startActivity(new Intent(this, AboutUsActivity.class));
            overridePendingTransition(0, 0);
            Intent i4 = new Intent(this, AboutUsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i4.putExtras(bundle);
            startActivity(i4);


        } else if (id == R.id.nav_menu_settings) {

        } else if (id == R.id.nav_menu_logout) {

            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to Logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(new Intent(GrandTestLandingActivity.this, MainActivity.class));
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }

        drawer.closeDrawer(GravityCompat.END);
        return true;
    }


    public void bottomViewNavigation() {
        bottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.ic_account_balance_black_24dp));
        bottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.ic_baseline_library_add_check_24));
        bottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.ic_assignment_black_24dp));
        bottomNavigation.add(new MeowBottomNavigation.Model(4, R.drawable.ic_live_tv_black_24dp));

        bottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                /* Toast.makeText(NavigationDrawerActivity.this, "clicked item : " + item.getId(), Toast.LENGTH_SHORT).show();*/
                if (item.getId() == 1) {
                    //SlideMenu fragment = new SlideMenu();
                    /*startActivity(new Intent(getActivity(), SlideMenu.class));*/
                    overridePendingTransition(0, 0);
                    Intent i1 = new Intent(GrandTestLandingActivity.this, DashBoardActivity.class);
                    i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i1);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 2) {
                    //startActivity(new Intent(getActivity(), MCQTest.class));
                    overridePendingTransition(0, 0);
                    Intent i2 = new Intent(GrandTestLandingActivity.this, MCQTest.class);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i2);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 3) {
                    //startActivity(new Intent(getActivity(), GrandTestLandingActivity.class));
                    overridePendingTransition(0, 0);
                    Intent i3 = new Intent(GrandTestLandingActivity.this, GrandTestLandingActivity.class);
                    i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i3);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 4) {
                    overridePendingTransition(0, 0);
                    Intent i4 = new Intent(GrandTestLandingActivity.this, AllVideosSlidesNotesActivity.class);
                    i4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i4);
                    overridePendingTransition(0, 0);
                }
            }
        });


        bottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {

                String name;
                switch (item.getId()) {
                    case 1:
                        name = "HOME";
                        break;
                    case 2:
                        name = "EXPLORE";
                        break;
                    case 3:
                        name = "MESSAGE";
                        break;
                    case 4:
                        name = "VIDEOS";
                        break;
                    default:
                        name = "";
                }
            }
        });

        bottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {
                /*  Toast.makeText(NavigationDrawerActivity.this, "reselected item : " + item.getId(), Toast.LENGTH_SHORT).show();*/
            }
        });


        // bottomNavigation.setCount(3, "117");
        bottomNavigation.show(3, true);
    }

    protected void onDestroy() {
        super.onDestroy();
    }


    private void shareApp() {

        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "ArDent BDS Application");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + "com.ardent_bds" + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void getUpdatedUserData() {

        new GetAllUserUpdateData(this).execute();
    }


    public class GetAllUserUpdateData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllUserUpdateData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_bar_login = new ProgressDialog(GrandTestLandingActivity.this);
            progress_bar_login.setMessage("Please wait...");
            progress_bar_login.setCancelable(false);
            progress_bar_login.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostUpdatedUserDetailsHandler httpPostHandler = new HttpPostUpdatedUserDetailsHandler();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Updated_User_details);
            if (responseFromDB != null && !responseFromDB.equals("")) {
                Log.e("Grant Test>>", "Grant Test data responseFromDBrrrrrrrrrrrrrrr: " + responseFromDB);
                userAccessList = getUserData(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            progress_bar_login.dismiss();

            if (userAccessList != null && !userAccessList.isEmpty()) {
                Log.i("hhhhh", "" + userAccessList.get(0).getUpdatedUserAccessLevel());

                UserAccessLevelModel.getInstance().setUpdatedUserAccessLevel(userAccessList.get(0).getUpdatedUserAccessLevel());
                UserModel.getInstance().setUserAccessLevel(Integer.parseInt(userAccessList.get(0).getUpdatedUserAccessLevel()));
            }
        }
    }

}
