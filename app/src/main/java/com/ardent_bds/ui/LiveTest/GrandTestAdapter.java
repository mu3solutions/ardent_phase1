package com.ardent_bds.ui.LiveTest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.project_utils.HttpGrandTestPostHandler;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.GrantTestUserDataModel;
import com.ardent_bds.ui.gallery.SubscriptionActivity;
import com.ardent_bds.ui.home.GrantTestDataModel;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GrandTestAdapter extends RecyclerView.Adapter<GrandTestAdapter.ProductViewHolder> {


    String TAG = getClass().getSimpleName();
    //this context we will use to inflate the layout
    private Context mCtx;

    ArrayList<GrantTestDataModel> grandTestAraryList;
    ArrayList<GrantTestUserDataModel> grandTestAraryListAll;
    ArrayList<GrantTestUserDataModel> userDetailsArrayList;
    HashMap<Integer, GrantTestUserDataModel> userDetailsArrayListHP = new HashMap<>();
    //we are storing all the products in a list
    private List<GrantTestDataModel> productList;
    private String userAccessList;

    GrantTestDataModel grantTestDataModel;

    ArrayList<QuestionDataModel> questionsDataModules;
    String HttpPostUrl = Constants.BaseUrl + "/ArdentDB/getGrandTestWithTestId.php";

    ImageView iv_lock;
    TextView tv_view_results;

    int selectedTestId = 0;

    //List<GrantTestUserDataModel> allGrandTestListUser;
    HashMap<Integer, GrantTestUserDataModel> allGrandTestListUserHP;


    public GrandTestAdapter(Context mCtx, List<GrantTestDataModel> productList, String userAccessList) {
        this.mCtx = mCtx;
        this.productList = productList;
        this.userAccessList = userAccessList;
    }

    public GrandTestAdapter(Context mCtx, List<GrantTestDataModel> productList, String userAccessList, HashMap<Integer, GrantTestUserDataModel> allGrandTestListUser) {
        this.mCtx = mCtx;
        this.productList = productList;
        this.userAccessList = userAccessList;
        this.allGrandTestListUserHP = allGrandTestListUser;

    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.grandrv_custom_layout, null);
        iv_lock = view.findViewById(R.id.iv_lock);
        tv_view_results = view.findViewById(R.id.tv_view_results);


        ConnectivityManager ConnectionManager = (ConnectivityManager) mCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getAllMCQQuestions();
        } else {

            Toast.makeText(mCtx, "Network Not Available", Toast.LENGTH_LONG).show();
        }
        grantTestDataModel = GrantTestDataModel.getInstance();
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position) {
        grandTestAraryList = (ArrayList<GrantTestDataModel>) productList;
        userDetailsArrayListHP = allGrandTestListUserHP;
        //userAccessList = (ArrayList<UserAccessLevelModel>) userAccessList;

        questionsDataModules = new ArrayList<>();
        holder.tv_grant_test_name.setText(productList.get(position).getTestName());


        String date = grandTestAraryList.get(position).getTestexpiryDate();
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
            spf = new SimpleDateFormat("dd-MMM-yyyy");
            date = spf.format(newDate);
            System.out.println(date);
            holder.expires_on.setText("Test Ends On " + date);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.total_questions_time.setText("" + grandTestAraryList.get(position).getTestTotalQuestions() + " Questions | " + grandTestAraryList.get(position).getTestTotalTiming() + " mins");


        if (userDetailsArrayListHP.size() > 0 && userDetailsArrayListHP.containsKey(position)) {
            if (userDetailsArrayListHP.get(position).getCompletedDetails().equalsIgnoreCase("1")) {
                holder.iv_test_status.setVisibility(View.VISIBLE);
                holder.tv_view_results.setVisibility(View.GONE);
            }
        }


        /*holder.tv_view_results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCtx.startActivity(new Intent(view.getContext(), FinalResult.class));
            }
        });*/
        //  holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(product.getImage()));


        if (position == 0 || position == 1) {
            holder.iv_lock.setVisibility(View.GONE);
        } else {

            if (userAccessList != null) {

                if (userAccessList.equalsIgnoreCase("3") || userAccessList.equalsIgnoreCase("32")
                        || userAccessList.equalsIgnoreCase("23") || userAccessList.equalsIgnoreCase("123")
                        || userAccessList.equalsIgnoreCase("13") || userAccessList.equalsIgnoreCase("31")) {
                    holder.iv_lock.setVisibility(View.GONE);
                }
            }

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (position == 0 || position == 1) {
                    GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));
                    grantTestDataModel.setSelectedTestId(productList.get(position).getSelecetdtestId());
                    if (userDetailsArrayListHP.size() > 0 && !userDetailsArrayListHP.containsKey(position)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(mCtx);
                        View view2 = layoutInflaterAndroid.inflate(R.layout.my_custom_dialog, null);
                        builder.setView(view2);
                        builder.setCancelable(false);
                        final AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                        Button ok = alertDialog.findViewById(R.id.buttonOk);
                        TextView mayBeLater = alertDialog.findViewById(R.id.may_be_later);
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();

                                GrantTestUserDataModel.getInstance().setExpiryTest(grandTestAraryList.get(position).getTestexpiryDate());
                                GrantTestDataModel.getInstance().setTestexpiryDate(grandTestAraryList.get(position).getTestexpiryDate());
                                grantTestDataModel.setSelectedTestId(productList.get(position).getSelecetdtestId());
                                GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));
                                GrantTestUserDataModel.getInstance().setGtName(productList.get(position).getTestName());
                                Intent intent = new Intent(view.getContext(), GrandTest.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("from_module_screen", "FMS");
                                bundle.putString("exam_expiry_date", grandTestAraryList.get(position).getTestexpiryDate());
                                bundle.putString("selected_test_name_1", productList.get(position).getTestName());
                                bundle.putInt("test_id", productList.get(position).getSelecetdtestId());
                                bundle.putString("total_questions", grandTestAraryList.get(position).getTestTotalQuestions());
                                bundle.putInt("selected_test_time", Integer.parseInt(grandTestAraryList.get(position).getTestTotalTiming()));
                                intent.putExtras(bundle);
                                view.getContext().startActivity(intent);
                                //((Activity) mCtx).finish();
                            }
                        });
                        mayBeLater.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                alertDialog.dismiss();
                            }
                        });

                    } else if (userDetailsArrayListHP.size() == 0 && !userDetailsArrayListHP.containsKey(position)) {
                        GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));
                        grantTestDataModel.setSelectedTestId(productList.get(position).getSelecetdtestId());
                        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(mCtx);
                        View view2 = layoutInflaterAndroid.inflate(R.layout.my_custom_dialog, null);
                        builder.setView(view2);
                        builder.setCancelable(false);
                        final AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                        Button ok = alertDialog.findViewById(R.id.buttonOk);
                        TextView mayBeLater = alertDialog.findViewById(R.id.may_be_later);
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();
                                GrantTestDataModel.getInstance().setTestexpiryDate(grandTestAraryList.get(position).getTestexpiryDate());
                                GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));
                                GrantTestUserDataModel.getInstance().setExpiryTest(grandTestAraryList.get(position).getTestexpiryDate());
                                grantTestDataModel.setSelectedTestId(productList.get(position).getSelecetdtestId());
                                Intent intent = new Intent(view.getContext(), GrandTest.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("from_module_screen", "FMS");
                                bundle.putString("exam_expiry_date", grandTestAraryList.get(position).getTestexpiryDate());
                                bundle.putString("selected_test_name_1", productList.get(position).getTestName());
                                bundle.putInt("test_id", productList.get(position).getSelecetdtestId());
                                bundle.putString("total_questions", grandTestAraryList.get(position).getTestTotalQuestions());
                                bundle.putInt("selected_test_time", Integer.parseInt(grandTestAraryList.get(position).getTestTotalTiming()));
                                intent.putExtras(bundle);
                                view.getContext().startActivity(intent);
                                // ((Activity) mCtx).finish();
                            }
                        });
                        mayBeLater.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                alertDialog.dismiss();
                            }
                        });

                    } else {
                        GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));
                        grantTestDataModel.setSelectedTestId(productList.get(position).getSelecetdtestId());

                        GrantTestUserDataModel.getInstance().setGtName(grandTestAraryList.get(position).getTestName());
                        GrantTestDataModel.getInstance().setTestName(grandTestAraryList.get(position).getTestName());

                        Intent intent = new Intent(view.getContext(), FinalResult.class);

                        Bundle bundle = new Bundle();
                        bundle.putString("selected_test_name_1", productList.get(position).getTestName());
                        bundle.putInt("test_id", productList.get(position).getSelecetdtestId());
                        bundle.putString("total_questions", grandTestAraryList.get(position).getTestTotalQuestions());
                        bundle.putInt("selected_test_answers", grandTestAraryList.get(position).getTotalCorrectAnswers());
                        bundle.putInt("skipped_test_answers", grandTestAraryList.get(position).getSkippedListCompletedGrandTest());


                        /*if (grandTestAraryList.get(position).getUserSelectedAnswerMap() != null) {
                            String[] skippedCount = allGrandTestListUserHP.get(position).getSelectedAnswerByUser().replace("{", "").replace("}", "").split(",");


                            if (!skippedCount[0].equals("")) {
                                // Then access here your item From list
                                ArrayList<String> skippedList = new ArrayList<>();
                                for (int p = 0; p < skippedCount.length; p++) {
                                    if (skippedCount[p].split("=")[1].equals("0") || skippedCount[p].split("=")[1].equals("-1"))
                                        skippedList.add(skippedCount[p]);
                                }

                                bundle.putInt("skipped_answers", skippedList.size());
                            }
                        } else {
                            bundle.putInt("skipped_answers", 0);
                        }*/

                        //  allGrandTestListUserHP.get(position).getSelectedAnswerByUser();
                        intent.putExtras(bundle);
                        view.getContext().startActivity(intent);
                        //  ((Activity) mCtx).finish();

                    }

                } else {

                    if (userAccessList.equalsIgnoreCase("3") || userAccessList.equalsIgnoreCase("32")
                            || userAccessList.equalsIgnoreCase("23") || userAccessList.equalsIgnoreCase("123")
                            || userAccessList.equalsIgnoreCase("13") || userAccessList.equalsIgnoreCase("31")) {

                        GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));
                        grantTestDataModel.setSelectedTestId(productList.get(position).getSelecetdtestId());
//                        if (userDetailsArrayListHP.get(position).getCompletedDetails().equalsIgnoreCase("0")) {

                        /*if (userDetailsArrayListHP.get(position).getCompletedDetails().equalsIgnoreCase("1")) {*/
                        if (userDetailsArrayListHP.size() > 0 && userDetailsArrayListHP.containsKey(position)) {
                            if (userDetailsArrayListHP.get(position).getCompletedDetails().equalsIgnoreCase("1")) {

                                Intent intent = new Intent(view.getContext(), FinalResult.class);
                                //GrantTestUserDataModel.getInstance().setGtName(productList.get(position).getTestName());\
                                GrantTestUserDataModel.getInstance().setGtName(grandTestAraryList.get(position).getTestName());
                                GrantTestDataModel.getInstance().setTestName(grandTestAraryList.get(position).getTestName());

                                Bundle bundle = new Bundle();
                                bundle.putString("selected_test_name_1", grandTestAraryList.get(position).getTestName());
                                bundle.putInt("test_id", productList.get(position).getSelecetdtestId());
                                bundle.putString("total_questions", grandTestAraryList.get(position).getTestTotalQuestions());
                                bundle.putInt("selected_test_answers", grandTestAraryList.get(position).getTotalCorrectAnswers());
                                bundle.putInt("skipped_test_answers", grandTestAraryList.get(position).getSkippedListCompletedGrandTest());


                                if (grandTestAraryList.get(position).getUserSelectedAnswerMap() != null) {
                                    String[] skippedCount = grandTestAraryList.get(position).getUserSelectedAnswerMap().replace("{", "").replace("}", "").split(",");


                                    if (!skippedCount[0].equals("")) {
                                        // Then access here your item From list
                                        ArrayList<String> skippedList = new ArrayList<>();
                                        for (int p = 0; p < skippedCount.length; p++) {
                                            if (skippedCount[p].split("=")[1].equals("0"))
                                                skippedList.add(skippedCount[p]);
                                        }

                                        bundle.putInt("skipped_answers", skippedList.size());
                                    }
                                } else {
                                    bundle.putInt("skipped_answers", 0);
                                }
                                intent.putExtras(bundle);
                                view.getContext().startActivity(intent);
                                //((Activity) mCtx).finish();

                            }
                        } else {

                            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(mCtx);
                            View view2 = layoutInflaterAndroid.inflate(R.layout.my_custom_dialog, null);
                            builder.setView(view2);
                            builder.setCancelable(false);
                            final AlertDialog alertDialog = builder.create();
                            alertDialog.show();

                            Button ok = alertDialog.findViewById(R.id.buttonOk);
                            TextView mayBeLater = alertDialog.findViewById(R.id.may_be_later);
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alertDialog.dismiss();
                                    if (productList.size() > 0) {
                                        GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));
                                        grantTestDataModel.setSelectedTestId(productList.get(position).getSelecetdtestId());
                                        GrantTestDataModel.getInstance().setTestexpiryDate(grandTestAraryList.get(position).getTestexpiryDate());
                                        GrantTestUserDataModel.getInstance().setExpiryTest(grandTestAraryList.get(position).getTestexpiryDate());
                                        GrantTestUserDataModel.getInstance().setGtName(grandTestAraryList.get(position).getTestName());
                                        GrantTestDataModel.getInstance().setTestName(grandTestAraryList.get(position).getTestName());
                                        Intent intent = new Intent(view.getContext(), GrandTest.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("from_module_screen", "FMS");
                                        bundle.putString("selected_test_name_1", grandTestAraryList.get(position).getTestName());
                                        bundle.putString("exam_expiry_date", grandTestAraryList.get(position).getTestexpiryDate());
                                        bundle.putInt("test_id", productList.get(position).getSelecetdtestId());
                                        bundle.putString("total_questions", grandTestAraryList.get(position).getTestTotalQuestions());
                                        bundle.putInt("selected_test_time", Integer.parseInt(grandTestAraryList.get(position).getTestTotalTiming()));
                                        intent.putExtras(bundle);
                                        view.getContext().startActivity(intent);
                                        //  ((Activity) mCtx).finish();
                                    }

                                }
                            });
                            mayBeLater.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    alertDialog.dismiss();
                                }
                            });
                        }

                    } else {
                        GrantTestUserDataModel.getInstance().setUserTestId(String.valueOf(productList.get(position).getSelecetdtestId()));
                        grantTestDataModel.setSelectedTestId(productList.get(position).getSelecetdtestId());
                        showPaymentAlert();
                    }
                }


            }
        });

    }

    private void showPaymentAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(mCtx);
        View view2 = layoutInflaterAndroid.inflate(R.layout.my_custom_dialog_view_plans, null);
        builder.setView(view2);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button ok = alertDialog.findViewById(R.id.buttonOk);
        TextView mayBeLater = alertDialog.findViewById(R.id.may_be_later);

        ok.setText("View Plans");
        mayBeLater.setText("Cancel");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Intent i2 = new Intent(mCtx, SubscriptionActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("screen_name", "GT");
                i2.putExtras(bundle);
                mCtx.startActivity(i2);
            }
        });
        mayBeLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView tv_grant_test_name, expires_on, total_questions_time, tv_view_results;
        ImageView iv_test_status, iv_lock;

        public ProductViewHolder(View itemView) {
            super(itemView);

            tv_grant_test_name = itemView.findViewById(R.id.tv_grant_test_name);
            expires_on = itemView.findViewById(R.id.expires_on);
            total_questions_time = itemView.findViewById(R.id.total_questions_time);
            tv_view_results = itemView.findViewById(R.id.tv_view_results);
            iv_test_status = itemView.findViewById(R.id.iv_test_status);
            iv_lock = itemView.findViewById(R.id.iv_lock);
            // imageView = itemView.findViewById(R.id.imageView);
        }
    }

    private void getAllMCQQuestions() {
        new GetMCQQuestionsFromServer(mCtx).execute();

    }

    public class GetMCQQuestionsFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetMCQQuestionsFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpGrandTestPostHandler httpGetHandler = new HttpGrandTestPostHandler();
            String responseFromDB = httpGetHandler.makeServicePostCall(HttpPostUrl);
            Log.e("Ques Test Id based dddd", "Question responseFromDB: " + responseFromDB);
            if (responseFromDB != null) {
                questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {


        }

    }


}
