package com.ardent_bds.ui.LiveTest;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.ui.GrantTestUserDataModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecyclerViewRankingAdapter extends RecyclerView.Adapter<RecyclerViewRankingAdapter.ViewHolder> {

    List<GrantTestUserDataModel> SubjectValues;
    FinalResult.GetAllUserCompletedGrantTestData context;
    View view1;
    ViewHolder viewHolder1;
    TextView textView;
    int currUserRank;
    List<GrantTestUserDataModel> allFullData;
    List<GrantTestUserDataModel> allFullDataFilter = new ArrayList<>();
    List<GrantTestUserDataModel> allFullDataFilterFinal = new ArrayList<>();


    public RecyclerViewRankingAdapter(FinalResult.GetAllUserCompletedGrantTestData context1, List<GrantTestUserDataModel> SubjectValues1, List<GrantTestUserDataModel> allGrandTestListUserFullData, Integer rankofCurrentUser) {

        SubjectValues = SubjectValues1;
        context = context1;
        allFullData = allGrandTestListUserFullData;
        currUserRank = rankofCurrentUser;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView userNameTv, tv_correct, tv_wrong, tv_skip, rank_position, top_users_score, current_user_score, my_tv_correct, my_tv_wrong, my_tv_skip, u_rank_position;
        public CardView cardViewRank;

        public ViewHolder(View v) {

            super(v);

            userNameTv = (TextView) v.findViewById(R.id.userNameTv);
            tv_correct = (TextView) v.findViewById(R.id.tv_correct);
            tv_wrong = (TextView) v.findViewById(R.id.tv_wrong);
            tv_skip = (TextView) v.findViewById(R.id.tv_skip);
            rank_position = (TextView) v.findViewById(R.id.rank_position);
            top_users_score = (TextView) v.findViewById(R.id.top_users_score);
            cardViewRank = (CardView) v.findViewById(R.id.card1);


        }
    }

    @Override
    public RecyclerViewRankingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_ranking_layout, parent, false);

        viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        HashMap<Map.Entry, Object> rankHP = new HashMap<>();

/*

        if (allFullDataFilter.size() > 0) {
            for (int i = 0; i < allFullDataFilter.size(); i++) {
                if (!allFullData.get(position).getFinalScore().equals(allFullDataFilter.get(i).getFinalScore())) {
                    allFullDataFilter.add(allFullData.get(position));
                 }
                else{
                    *//*allFullDataFilter.remove(allFullDataFilter.indexOf(i));*//*

                }
            }
            allFullDataFilter.add(allFullData.get(position));
        } else {
            allFullDataFilter.add(allFullData.get(position));
        }*/



       /* if (allFullData.contains(allFullData.get(position).getFinalScore())) {
            allFullData.remove(position);
        }*/


        /*if (allFullData.get(position).getFinalScore().equals(allFullDataFilter.get(position).getFinalScore())) {
            //allFullDataFilterFinal.add(allFullData.get(position));
            allFullData.remove(position);
        }
        else{
            allFullDataFilterFinal.add(allFullData.get(position));
        }*/


        Log.i("allFullDataFilter>>>", "" + allFullDataFilter);
        Log.i("allFullData>>>", "" + allFullData);


            //holder.userNameTv.setText("" + allFullData.get(position).getUserTestId());
            holder.tv_correct.setText("" + allFullData.get(position).getCorrectCount());
            holder.tv_wrong.setText("" + allFullData.get(position).getWrongCount());
            holder.tv_skip.setText("" + allFullData.get(position).getSkipCount());
            holder.top_users_score.setText("" + allFullData.get(position).getFinalScore());


            if (currUserRank - 1 == position) {
                holder.cardViewRank.setCardBackgroundColor(Color.GREEN);
            }

            int p = position + Integer.parseInt("1");
            if (p == 1) {
                holder.rank_position.setText("" + p + "st");
            } else if (p == 2) {
                holder.rank_position.setText("" + p + "nd");
            } else if (p == 3) {
                holder.rank_position.setText("" + p + "rd");
            } else {
         //   holder.rank_position.setText("" + p + "th");
                holder.cardViewRank.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {

        return allFullData.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}