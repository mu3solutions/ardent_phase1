package com.ardent_bds.ui.LiveTest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.adapters.MarkedQuestionAdapter;
import com.ardent_bds.model.DataModelTemp;
import com.ardent_bds.ui.home.GrantTestDataModel;

import java.util.HashMap;

public class MarkedQuestionsActivity extends Activity {

    private HashMap<Integer, Integer> arrayListDataMarked;
    private HashMap<Integer, Integer> arrayListDataSkipped;
    private HashMap<Integer, Integer> getArrayListDataAttempted;
    private String arrayListData1;
    private int totaoArrayListCount;

    GrantTestDataModel grantTestDataModel;
    RecyclerView rViewMarks;
    Context context;


    ImageView closeQuestionMarks;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.marked_questions_activity);

        grantTestDataModel = GrantTestDataModel.getInstance();
        rViewMarks = findViewById(R.id.rViewMarks);
        closeQuestionMarks = findViewById(R.id.closeQuestionMarks);

        closeQuestionMarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        arrayListDataMarked = new HashMap<>();
        getArrayListDataAttempted = new HashMap<>();
        arrayListDataSkipped = new HashMap<>();


        arrayListDataMarked.clear();
        arrayListDataSkipped.clear();
        getArrayListDataAttempted.clear();

        /*arrayListDataMarked = grantTestDataModel.getAllMarked_news();
        arrayListDataSkipped = grantTestDataModel.getAllSkipped_news();
        getArrayListDataAttempted = grantTestDataModel.getAllSelected_news();
        */


        arrayListDataMarked = DataModelTemp.getInstance().getGetTempMarkedAnswers();
        arrayListDataSkipped = DataModelTemp.getInstance().getGetTempSkippedAnswers();
        getArrayListDataAttempted = DataModelTemp.getInstance().getGetTempSelecetdAnswers();


        Bundle bundle = getIntent().getExtras();

//Extract the data…
        totaoArrayListCount = bundle.getInt("total_questions_bundle");

        context = getApplicationContext();

        MarkedQuestionAdapter adapter = new MarkedQuestionAdapter(context, arrayListDataMarked, totaoArrayListCount, arrayListDataSkipped, getArrayListDataAttempted);
        rViewMarks.setHasFixedSize(true);
        int numberOfColumns = 4;
        rViewMarks.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        rViewMarks.setAdapter(adapter);


    }
}
