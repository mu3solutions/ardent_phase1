package com.ardent_bds.ui.LiveTest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.customviews.LockableViewPager;
import com.ardent_bds.project_utils.HttpHandler;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.mcq_of_day.QuestionCustomAdapter;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LiveTestLandingActivity extends BaseActivity implements View.OnClickListener {


    public static final String PREF_USER_FIRST_TIME = "user_first_time";
    public LockableViewPager mViewPager;
    Button mNextBtn;
    Button mSkipBtn, mFinishBtn;
    ImageView zero, one, two;
    ImageView[] indicators;
    int lastLeftValue = 0;
    CoordinatorLayout mCoordinator;
    static final String TAG = "OnBoardingActivity";
    boolean isUserFirstTime;
    int page = 0;
    ArrayList<QuestionDataModel> questionsDataModules;
    QuestionCustomAdapter questionCustomAdapter;
    MeowBottomNavigation bottomNavigation;
    String HttpGetUrl = Constants.BaseUrl +"/ArdentDB/getAllQuestions.php";
    ArrayList<QuestionDataModel> all_folder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.livetest_main_activity);

        mNextBtn = (Button) findViewById(R.id.intro_btn_next);

        mSkipBtn = (Button) findViewById(R.id.intro_btn_skip);
        mFinishBtn = (Button) findViewById(R.id.intro_btn_finish);

        mCoordinator = (CoordinatorLayout) findViewById(R.id.main_content);


        mViewPager = (LockableViewPager) findViewById(R.id.container);

        mViewPager.setSwipable(false);

//        bottomNavigation = findViewById(R.id.bottom_nav);
  //      bottomViewNavigation();

        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getAllMCQQuestions();
        } else {

            Toast.makeText(LiveTestLandingActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }



        mNextBtn.setOnClickListener(this);
        mSkipBtn.setOnClickListener(this);
        mFinishBtn.setOnClickListener(this);


        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page += 1;
                mViewPager.setCurrentItem(page, true);
            }
        });
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page -= 1;
                mViewPager.setCurrentItem(page, true);
            }
        });
        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(LiveTestLandingActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Confirm")
                        .setContentText("Are you sure to submit test!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                overridePendingTransition(0, 0);
                                finish();
                                startActivity(new Intent(LiveTestLandingActivity.this, DashBoardActivity.class));
                            }
                        })
                        .setCancelButton("Cancel", new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }

        });

    }

    private void getAllMCQQuestions() {
        new GetMCQQuestionsFromServer(getApplicationContext()).execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
    }

    public class MCQViewPagerViewAdapter extends PagerAdapter {

        Context context;
        ArrayList<QuestionDataModel> all_folder = new ArrayList<>();
        int int_position;
        ArrayList<Integer> hmp1 = new ArrayList<>();
        LayoutInflater inflater;
        int correctAnswerPosition;
        private int selectedPos = RecyclerView.NO_POSITION;
        TextView option_id, answer_txt1, answer_txt2, answer_txt3, answer_txt4, question_txt, txt_description_answer, proceed_tv;
        ImageView imv_tick_cross1, imv_tick_cross2, imv_tick_cross3, imv_tick_cross4;
        LinearLayout ll1, ll2, ll3, ll4, ll_answer_desc;

        public MCQViewPagerViewAdapter(Context context, ArrayList<QuestionDataModel> all_folder) {
            this.context = context;
            this.all_folder = all_folder;
        }


        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((LinearLayout) object);
        }

        @Override
        public int getCount() {
            return all_folder.size();
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            ImageView imageView;

            QuestionDataModel questionDataModel = QuestionDataModel.getInstance();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View viewLayout = inflater.inflate(R.layout.cards_layout, container, false);

            question_txt = viewLayout.findViewById(R.id.question_txt);
            ll_answer_desc = viewLayout.findViewById(R.id.ll_answer_desc);
            answer_txt1 = viewLayout.findViewById(R.id.answer1);
            answer_txt2 = viewLayout.findViewById(R.id.answer2);
            answer_txt3 = viewLayout.findViewById(R.id.answer3);
            answer_txt4 = viewLayout.findViewById(R.id.answer4);
            txt_description_answer = viewLayout.findViewById(R.id.txt_description_answer);
            proceed_tv = viewLayout.findViewById(R.id.proceed_tv);


            ll1 = viewLayout.findViewById(R.id.ll1);
            ll2 = viewLayout.findViewById(R.id.ll2);
            ll3 = viewLayout.findViewById(R.id.ll3);
            ll4 = viewLayout.findViewById(R.id.ll4);


            ll1.setOnClickListener(LiveTestLandingActivity.this);
            ll2.setOnClickListener(LiveTestLandingActivity.this);
            ll3.setOnClickListener(LiveTestLandingActivity.this);
            ll4.setOnClickListener(LiveTestLandingActivity.this);


            imv_tick_cross1 = viewLayout.findViewById(R.id.imv_tick_cross1);
            imv_tick_cross2 = viewLayout.findViewById(R.id.imv_tick_cross2);
            imv_tick_cross3 = viewLayout.findViewById(R.id.imv_tick_cross3);
            imv_tick_cross4 = viewLayout.findViewById(R.id.imv_tick_cross4);


            String ques = all_folder.get(position).getMcqQuestionDesc();
            String[] queStringArray = ques.split(",");

            question_txt.setText("" + queStringArray[0]);
            answer_txt1.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[0].replace("\r\n", ""));
            answer_txt2.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[1].replace("\r\n", ""));
            answer_txt3.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[2].replace("\r\n", ""));
            answer_txt4.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[3].replace("\r\n", ""));
            correctAnswerPosition = all_folder.get(position).getMcqCorrectAnswer();


            hmp1.add(position, correctAnswerPosition);


            ll1.setTag("l1");
            ll2.setTag("l2");
            ll3.setTag("l3");
            ll4.setTag("l4");

            viewLayout.findViewWithTag("l1").setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPos = 1;

                    if (viewLayout.findViewWithTag("l1").isSelected() || viewLayout.findViewWithTag("l2").isSelected() ||
                            viewLayout.findViewWithTag("l3").isSelected() || viewLayout.findViewWithTag("l4").isSelected()) {
                        viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l1").setSelected(false);
                        viewLayout.findViewWithTag("l2").setSelected(false);
                        viewLayout.findViewWithTag("l3").setSelected(false);
                        viewLayout.findViewWithTag("l4").setSelected(false);
                    }
                    if (selectedPos == hmp1.get(position)) {
                        if (hmp1.get(position) == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (hmp1.get(position) == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (hmp1.get(position) == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (hmp1.get(position) == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                    } else {
                        if (hmp1.get(position) == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (hmp1.get(position) == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (hmp1.get(position) == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (hmp1.get(position) == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }

                        if (selectedPos == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (selectedPos == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (selectedPos == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (selectedPos == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                    }
                }
            });
            viewLayout.findViewWithTag("l2").setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPos = 2;

                    if (viewLayout.findViewWithTag("l1").isSelected() || viewLayout.findViewWithTag("l2").isSelected() ||
                            viewLayout.findViewWithTag("l3").isSelected() || viewLayout.findViewWithTag("l4").isSelected()) {
                        viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l1").setSelected(false);
                        viewLayout.findViewWithTag("l2").setSelected(false);
                        viewLayout.findViewWithTag("l3").setSelected(false);
                        viewLayout.findViewWithTag("l4").setSelected(false);
                    }

                    if (selectedPos == hmp1.get(position)) {
                        if (hmp1.get(position) == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (hmp1.get(position) == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (hmp1.get(position) == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (hmp1.get(position) == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                    } else {
                        if (hmp1.get(position) == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (hmp1.get(position) == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (hmp1.get(position) == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (hmp1.get(position) == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                        if (selectedPos == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (selectedPos == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (selectedPos == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (selectedPos == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                    }
                }
            });
            viewLayout.findViewWithTag("l3").setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPos = 3;
                    if (viewLayout.findViewWithTag("l1").isSelected() || viewLayout.findViewWithTag("l2").isSelected() ||
                            viewLayout.findViewWithTag("l3").isSelected() || viewLayout.findViewWithTag("l4").isSelected()) {
                        viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l1").setSelected(false);
                        viewLayout.findViewWithTag("l2").setSelected(false);
                        viewLayout.findViewWithTag("l3").setSelected(false);
                        viewLayout.findViewWithTag("l4").setSelected(false);
                    }
                    if (selectedPos == hmp1.get(position)) {
                        if (hmp1.get(position) == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (hmp1.get(position) == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (hmp1.get(position) == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (hmp1.get(position) == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                    } else {
                        if (hmp1.get(position) == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (hmp1.get(position) == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (hmp1.get(position) == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (hmp1.get(position) == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }

                        if (selectedPos == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (selectedPos == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (selectedPos == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (selectedPos == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                    }
                }
            });
            viewLayout.findViewWithTag("l4").setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPos = 4;

                    if (viewLayout.findViewWithTag("l1").isSelected() || viewLayout.findViewWithTag("l2").isSelected() ||
                            viewLayout.findViewWithTag("l3").isSelected() || viewLayout.findViewWithTag("l4").isSelected()) {
                        viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);
                        viewLayout.findViewWithTag("l1").setSelected(false);
                        viewLayout.findViewWithTag("l2").setSelected(false);
                        viewLayout.findViewWithTag("l3").setSelected(false);
                        viewLayout.findViewWithTag("l4").setSelected(false);
                    }
                    if (selectedPos == hmp1.get(position)) {
                        if (hmp1.get(position) == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (hmp1.get(position) == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (hmp1.get(position) == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (hmp1.get(position) == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                    } else {
                        if (hmp1.get(position) == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (hmp1.get(position) == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (hmp1.get(position) == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (hmp1.get(position) == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                        if (selectedPos == 1) {
                            viewLayout.findViewWithTag("l1").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l1").setSelected(true);
                        } else if (selectedPos == 2) {
                            viewLayout.findViewWithTag("l2").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l2").setSelected(true);
                        } else if (selectedPos == 3) {
                            viewLayout.findViewWithTag("l3").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l3").setSelected(true);
                        } else if (selectedPos == 4) {
                            viewLayout.findViewWithTag("l4").setBackgroundColor(Color.RED);
                            viewLayout.findViewWithTag("l4").setSelected(true);
                        }
                    }
                }
            });

            mViewPager.setOffscreenPageLimit(all_folder.size());
            container.addView(viewLayout);
            return viewLayout;
        }
    }





    /*public class OnLiveTestPagerAdapter extends FragmentPagerAdapter {

        public OnLiveTestPagerAdapter(FragmentManager supportFragmentManager, ArrayList<QuestionDataModel> questionsDataModules) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }*/

    public class GetMCQQuestionsFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetMCQQuestionsFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler httpPostHandler = new HttpHandler();
            String responseFromDB = httpPostHandler.makeServiceCall(HttpGetUrl);
            if(responseFromDB!=null) {
                Log.e("Questions data ", "Question responseFromDB: " + responseFromDB);

                questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            }

            return null;
        }

        protected void onPostExecute(Void result) {


            if (questionsDataModules.size() > 0) {

                MCQViewPagerViewAdapter photoViewAdapter = new MCQViewPagerViewAdapter(getApplicationContext(), questionsDataModules);
                mViewPager.setAdapter(photoViewAdapter);

                mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        page = position;
                        mNextBtn.setVisibility(position == questionsDataModules.size() - 1 ? View.GONE : View.VISIBLE);
                        mFinishBtn.setVisibility(position == questionsDataModules.size() - 1 ? View.VISIBLE : View.GONE);

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
        startActivity(new Intent(this, DashBoardActivity.class));
    }




   /* private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
*/
}