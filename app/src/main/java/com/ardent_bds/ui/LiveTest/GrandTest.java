package com.ardent_bds.ui.LiveTest;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ardent_bds.CountDownTimerService;
import com.ardent_bds.R;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.customviews.LockableViewPager;
import com.ardent_bds.model.DataModelTemp;
import com.ardent_bds.project_utils.HttpGrandTestPostHandler;
import com.ardent_bds.project_utils.HttpPostHandlerGrandTestUpdate;
import com.ardent_bds.project_utils.HttpPostHandlerGrandTestUpdateUserBased;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.GrantTestUserDataModel;
import com.ardent_bds.ui.home.GrantTestDataModel;
import com.ardent_bds.ui.mcq_of_day.QuestionCustomAdapter;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class GrandTest extends BaseActivity implements View.OnClickListener {


    public static final String PREF_USER_FIRST_TIME = "user_first_time";
    static final String TAG = "OnBoardingActivity";
    public LockableViewPager mViewPager;
    public TextView counter_timer;
    public int counter;
    Button mNextBtn;
    Button mPreviousBtn, mFinishBtn, intro_btn_mark;
    ImageView zero, one, two;
    ImageView[] indicators;
    int lastLeftValue = 0;
    CoordinatorLayout mCoordinator;
    boolean isUserFirstTime;
    int page = 0;
    int markCount;
    ArrayList<Integer> markedCountAnswersMap;
    ArrayList<Integer> skippedCountAnswersMap;
    ArrayList<GrantTestDataModel> questionsDataModules;
    QuestionCustomAdapter questionCustomAdapter;
    String HttpPostUrl = Constants.BaseUrl + "/ArdentDB/getGrandTestWithTestId.php";
    String HttpPostGranTestCompletedUrl = Constants.BaseUrl + "/ArdentDB/grandTestStatusUpdate.php";
    String HttpPostGranTestCompletedUrlAllUsersTest = Constants.BaseUrl + "/ArdentDB/grandtestAllUsersResults.php";
    ArrayList<QuestionDataModel> all_folder;
    int correctAnswerCount = 0;
    TextView question_count_answers;
    SeekBar completedSeekBarCompleted;
    ArrayList<Integer> selectedByUser;
    ArrayList<Integer> correctAnswerList;
    ArrayList<Integer> hmp1;
    HashMap<Integer, Integer> correctAnswerHM;
    HashMap<Integer, Integer> selectedAnswerHM;
    HashMap<Integer, Integer> markedAnswerHM;
    HashMap<Integer, Integer> markedAnswerStatusHM;
    HashMap<Integer, Integer> skippedAnswerStatusHM;
    HashMap<Integer, Integer> allAnswersDetails;
    HashMap<Integer, Boolean> hasUserChosenCorrectAnswer;
    GrantTestDataModel grantTestDataModel;
    String testName;

    HashMap<Integer, Integer> finalWrongAnswerList;

    double finalUserScore;
    String dateTestExpiry;
    int gotoPage = 0;
    int CurrentBackPosition = 0;
    HashSet<Integer> sAllCorrectAnswerCount;
    private ProgressDialog progress_bar_grandTest;
    private ProgressDialog progress_bar_grandTest1;
    private int selectedPos = RecyclerView.NO_POSITION;
    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent); // or whatever method used to update your GUI fields
        }
    };

    Handler seekBarHandler = new Handler();


    HashMap<Integer, Integer> correctData;
    HashMap<Integer, Integer> skippedData;
    HashMap<Integer, Integer> correctDefaultData;
    HashMap<Integer, Integer> wrongData;

    HashMap<Integer, Integer> comparedCorrectData;
    HashMap<Integer, Integer> comparedWrongData;
    HashMap<Integer, Integer> comparedSkipData;


    HashMap<Integer, Integer> SelData;
    HashMap<Integer, Integer> SkipData;


    int SkipDataTotalCount = 0;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.livetest_main_activity_1);

        progress_bar_grandTest = new ProgressDialog(GrandTest.this);
        progress_bar_grandTest.setMessage("Please wait...");
        progress_bar_grandTest.setCancelable(false);
        progress_bar_grandTest.setIndeterminate(true);

        progress_bar_grandTest1 = new ProgressDialog(GrandTest.this);
        progress_bar_grandTest1.setMessage("Getting your Test Ready...");
        progress_bar_grandTest1.setCancelable(false);
        progress_bar_grandTest1.setIndeterminate(true);


        //completedSeekBarCompleted = findViewById(R.id.completedSeekBarQuestions);
        question_count_answers = findViewById(R.id.question_count_answers);
        mViewPager = (LockableViewPager) findViewById(R.id.container);

        mNextBtn = (Button) findViewById(R.id.intro_btn_next);

        mPreviousBtn = (Button) findViewById(R.id.intro_btn_skip);
        mFinishBtn = (Button) findViewById(R.id.intro_btn_finish);
        intro_btn_mark = (Button) findViewById(R.id.intro_btn_mark);

        mCoordinator = (CoordinatorLayout) findViewById(R.id.main_content);


        counter_timer = findViewById(R.id.counter_timer);

        mViewPager.setSwipable(false);

        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getAllMCQQuestions();
        } else {

            Toast.makeText(GrandTest.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


        //updateIndicators(page);
        mNextBtn.setOnClickListener(this);
        mPreviousBtn.setOnClickListener(this);
        mFinishBtn.setOnClickListener(this);
        intro_btn_mark.setOnClickListener(this);

        intro_btn_mark.setSelected(false);
        selectedByUser = new ArrayList<>();
        correctAnswerList = new ArrayList<>();

        sAllCorrectAnswerCount = new HashSet<>();
        correctAnswerHM = new HashMap<>();
        selectedAnswerHM = new HashMap<>();
        markedAnswerHM = new HashMap<>();
        markedAnswerStatusHM = new HashMap<>();
        skippedAnswerStatusHM = new HashMap<>();
        allAnswersDetails = new HashMap<>();
        hasUserChosenCorrectAnswer = new HashMap<>();


        SelData = new HashMap<>();
        SkipData = new HashMap<>();


        markedCountAnswersMap = new ArrayList<>();
        skippedCountAnswersMap = new ArrayList<>();

        hmp1 = new ArrayList<>();

        correctData = new HashMap<>();
        skippedData = new HashMap<>();
        correctDefaultData = new HashMap<>();
        wrongData = new HashMap<>();

        finalWrongAnswerList = new HashMap<>();
        comparedCorrectData = new HashMap<>();
        comparedWrongData = new HashMap<>();
        comparedSkipData = new HashMap<>();


        HashMap<Integer, Integer> dummyClearHMap = new HashMap<>();
        Bundle bundle = getIntent().getExtras();
        grantTestDataModel = GrantTestDataModel.getInstance();

        testName = bundle.getString("selected_test_name");
        final int selected_test_time = bundle.getInt("selected_test_time");
        //String statusOfTest = bundle.getString("from_module_screen");

        String statusOfTest = getIntent().getExtras().getString("from_module_screen");

        if (bundle.getString("exam_expiry_date") != null) {
            dateTestExpiry = bundle.getString("exam_expiry_date");
            GrantTestDataModel.getInstance().setTestexpiryDate(dateTestExpiry);
        } else {
            GrantTestDataModel.getInstance().setTestexpiryDate(GrantTestDataModel.getInstance().getTestexpiryDate());
            dateTestExpiry = GrantTestDataModel.getInstance().getTestexpiryDate();
        }

        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");
        Date newDate = null;
        try {
            newDate = spf.parse(dateTestExpiry);
            spf = new SimpleDateFormat("dd-MMM-yyyy");
            dateTestExpiry = spf.format(newDate);
            System.out.println("Test expiry date " + dateTestExpiry);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (statusOfTest.equalsIgnoreCase("FMS")) {
            grantTestDataModel.setMySelectionList(dummyClearHMap);
            grantTestDataModel.setGetMarkedAnswers(dummyClearHMap);
        } else {

            if (grantTestDataModel.getGetMarkedAnswers() != null) {
                markedAnswerHM = grantTestDataModel.getGetMarkedAnswers();
            }

            if (grantTestDataModel.getMySelectionList() != null) {
                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
            }

            DataModelTemp dataModelTemp = DataModelTemp.getInstance();
            markedAnswerHM = dataModelTemp.getGetTempMarkedAnswers();
            selectedAnswerHM = dataModelTemp.getGetTempSelecetdAnswers();
            skippedAnswerStatusHM = dataModelTemp.getGetTempSkippedAnswers();
            correctAnswerHM = dataModelTemp.getGetTempCorrectAnswers();


            callInSideMethod();

        }


        //startTimer(selected_test_time * 60000);


        int selTimer = selected_test_time * 60000;
        grantTestDataModel.setTestTotalTiming(String.valueOf(selTimer));


/*
        completedSeekBarCompleted.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
*/

/*
        completedSeekBarCompleted.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                progress = page;
                completedSeekBarCompleted.setProgress(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });*/

        /*if(allAnswersDetails.size()>0){

        }*/


        if (allAnswersDetails.size() > 0) {
            selectedPos = allAnswersDetails.get(page);
        }

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               /* if (selectedPos == -1 && !selectedAnswerHM.containsKey(page)) {
                    skippedCountAnswersMap.add(page);
                    skippedAnswerStatusHM.put(page, selectedPos);
                    allAnswersDetails.put(page, -1);
                    selectedAnswerHM.put(page, -1);
                    correctAnswerHM.put(page, hmp1.get(page));
                }*/

                if (selectedPos == -1) {
                    if (selectedAnswerHM.size() > 0) {
                        skippedCountAnswersMap.add(page);
                        skippedAnswerStatusHM.put(page, selectedPos);
                        //selectedAnswerHM.put(page, -1);
                        correctAnswerHM.put(page, hmp1.get(page));
                        allAnswersDetails.put(page, -1);

                    } else {
                        skippedCountAnswersMap.add(mViewPager.getAdapter().getCount());
                        for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {
                            skippedAnswerStatusHM.put(i, mViewPager.getAdapter().getCount());
                        }

                    }
                    SkipData.put(page, -1);

                } else {
                    if (skippedAnswerStatusHM.containsKey(page)) {
                        skippedAnswerStatusHM.remove(page);
                    }
                    if (skippedCountAnswersMap.contains(page)) {
                        //skippedCountAnswersMap.remove(page);
                        skippedCountAnswersMap.remove(Collections.binarySearch(skippedCountAnswersMap, page));
                    }
                    selectedAnswerHM.put(page, selectedPos);
                    correctAnswerHM.put(page, hmp1.get(page));
                    allAnswersDetails.put(page, 1);

                    SelData.put(page, selectedPos);
                }
                if (markedAnswerStatusHM.containsKey(page)) {
                    markedAnswerStatusHM.remove(page);
                    markedAnswerStatusHM.put(page, selectedPos);
                    markedAnswerStatusHM.put(page, selectedPos);
                    /*selectedAnswerHM.put(page, page);*/
                } /*else {
                    markedAnswerStatusHM.put(page, selectedPos);
                    *//*selectedAnswerHM.put(page, page);*//*
                }*/
                page += 1;
                mViewPager.setCurrentItem(page, true);
                selectedPos = -1;
                intro_btn_mark.setBackgroundColor(0);

/*
                correctAnswerHM.put(page, hmp1.get(page));
*/
                //intro_btn_mark.setSelected(false);


                DataModelTemp.getInstance().setGetTempCorrectAnswers(correctAnswerHM);
                grantTestDataModel.setAllMarked_news(markedAnswerHM);
                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                grantTestDataModel.setAllSelected_news(selectedAnswerHM);

                grantTestDataModel.setTotalCorrectAnswers(correctAnswerCount);
                grantTestDataModel.setCorrectAnswerMap(String.valueOf(correctAnswerHM));
                grantTestDataModel.setGetSkippedAnswers(skippedCountAnswersMap);
                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);


                grantTestDataModel.setGetMarkedAnswers(markedAnswerHM);
                grantTestDataModel.setGetMarkedSelectedAnswers(markedAnswerStatusHM);


                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());
                DataModelTemp dataModelTemp = DataModelTemp.getInstance();
                dataModelTemp.setGetTempMarkedAnswers(markedAnswerHM);
                dataModelTemp.setGetTempSelecetdAnswers(selectedAnswerHM);
                dataModelTemp.setGetTempSkippedAnswers(skippedAnswerStatusHM);
                dataModelTemp.setGetTempCorrectAnswers(correctAnswerHM);


                intro_btn_mark.setSelected(false);

                callInSideMethod();
            }
        });
        mPreviousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page -= 1;
                mViewPager.setCurrentItem(page, true);
                callInSideMethod();
            }
        });
        intro_btn_mark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intro_btn_mark.isSelected()) {
                    intro_btn_mark.setBackgroundColor(0);
                    intro_btn_mark.setSelected(false);
                    markedAnswerHM.remove(page);
                    markedAnswerStatusHM.remove(page);
                    markCount = markCount - 1;

                } else {
                    intro_btn_mark.setBackgroundColor(getResources().getColor(R.color.vedantu_orange));
                    intro_btn_mark.setSelected(true);
                    markedAnswerHM.put(page, page);
                    markedAnswerStatusHM.put(page, selectedPos);
                    markCount = markCount + 1;

                }

                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                grantTestDataModel.setAllMarked_news(markedAnswerHM);


                grantTestDataModel.setGetMarkedAnswers(markedAnswerHM);
                grantTestDataModel.setGetMarkedSelectedAnswers(markedAnswerStatusHM);


            }
        });
        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //progress_bar_grandTest.show();


                //showAlertDialog("");


                if (GrantTestDataModel.getInstance().isNothingSelected() || skippedAnswerStatusHM.size() == mViewPager.getAdapter().getCount()) {
                    //progress_bar_grandTest.dismiss();
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(GrandTest.this);
                    alertDialog.setTitle("Do you want to submit the Test.?");
                    alertDialog.setMessage("You have " + mViewPager.getAdapter().getCount() + " unattempted questions");
                    alertDialog.setIcon(R.drawable.logo_05);
                    alertDialog.setPositiveButton("Submit Test", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            HashMap<Integer, Integer> zeroAttempts = new HashMap<>();
                            zeroAttempts.put(0, 0);

                            sAllCorrectAnswerCount.clear();
                            selectedAnswerHM.clear();
                            skippedCountAnswersMap.add(mViewPager.getAdapter().getCount());

                            for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {
                                skippedAnswerStatusHM.put(i, i);
                                skippedCountAnswersMap.add(i);
                            }
                            grantTestDataModel.setSkippedListCompletedGrandTest(skippedAnswerStatusHM.size());
                            grantTestDataModel.setTotalCorrectAnswers(sAllCorrectAnswerCount.size());
                            grantTestDataModel.setCorrectAnswerMap(String.valueOf(zeroAttempts));
                            grantTestDataModel.setTotalCorrectis(sAllCorrectAnswerCount.size());
                            grantTestDataModel.setUserSelectedAnswerMap(zeroAttempts.toString());
                            grantTestDataModel.setGetMarkedAnswers(zeroAttempts);
                            grantTestDataModel.setGetMarkedSelectedAnswers(zeroAttempts);
                            grantTestDataModel.setGetUnMarkedUserSelectedAnswers(zeroAttempts);

                            ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
                            if (networkInfo != null && networkInfo.isConnected() == true) {
                                calculatePercentage();
                                makeAServiceCall();
                                //  makeAServiceCallToUpdateAllUsers();
                            } else {

                                Toast.makeText(GrandTest.this, "Network Not Available", Toast.LENGTH_LONG).show();
                            }


                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = alertDialog.create();
                    alert.setCanceledOnTouchOutside(false);
                    alert.show();
                } else {
                    //progress_bar_grandTest.dismiss();

                    for (int i = 0; i < selectedAnswerHM.size(); i++) {
                        if (selectedAnswerHM.containsKey(i) && correctAnswerHM.containsKey(i)) {
                            if (selectedAnswerHM.get(i).equals(correctAnswerHM.get(i))) {
                                sAllCorrectAnswerCount.add(i);
                            }
                        }
                        int skipC = mViewPager.getAdapter().getCount() - selectedAnswerHM.size();
                        int skipT = skippedCountAnswersMap.size() + skipC;
                        grantTestDataModel.setSkippedListCompletedGrandTest(skipC);
                        grantTestDataModel.setTotalCorrectis(sAllCorrectAnswerCount.size());
                        grantTestDataModel.setTotalCorrectAnswers(sAllCorrectAnswerCount.size());
                    }
                    if (!isFinishing()) {
                        //showAlertDialog("Test Complete. Submit the test" + sAllCorrectAnswerCount.size());
                        showAlertDialog("");
                    }
                }
            }
        });

    }

    private void calculatePercentage() {
        int total_questions = mViewPager.getAdapter().getCount();
        int selected_test_answersInt = selectedAnswerHM.size();
        int skipped_answersCount = grantTestDataModel.getSkippedListCompletedGrandTest();
        int totalWrongAnswers = total_questions - (selected_test_answersInt + skipped_answersCount);
        int getTotalCorrectAnswerInt = GrantTestUserDataModel.getInstance().getCorrectCount();
        int getTotalQuestionsInTestInt = total_questions;

        int dataInt = (int) (((double) getTotalCorrectAnswerInt / (double) getTotalQuestionsInTestInt) * 100);
        /*  String dataIntToString = String.format("%.2d", dataInt);*/
        grantTestDataModel.setPercentageValue(dataInt);
        GrantTestUserDataModel.getInstance().setPercentage(String.valueOf(dataInt));
    }

    private void callInSideMethod() {


        HashMap<Integer, Integer> markedQues = new HashMap<>();
        HashMap<Integer, Integer> markedAnswers = new HashMap<>();

        markedQues = grantTestDataModel.getGetMarkedAnswers();
        markedAnswers = grantTestDataModel.getGetUnMarkedUserSelectedAnswers();


        //set prevcious selected item
        //set prevcious selected item
        if (markedQues != null) {
            if (markedQues.containsValue(mViewPager.getCurrentItem())) {

                if (markedQues.get(mViewPager.getCurrentItem()).equals(page)) {
                    //viewLayout.findViewWithTag("l1").setBackgroundColor(getResources().getColor(R.color.vedantu_orange));
                    intro_btn_mark.setBackgroundColor(getResources().getColor(R.color.vedantu_orange));
                    //viewLayout.findViewWithTag("l1").setSelected(true);
                }

            } else {
                intro_btn_mark.setBackgroundColor(0);
            }

        }

        grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

        /*if (markedAnswers != null) {
            if (markedAnswers.containsValue(mViewPager.getCurrentItem())) {
                if (markedAnswers.get(position) == 1) {
                    viewLayout.findViewWithTag("l1").setBackgroundColor(Color.CYAN);
                    viewLayout.findViewWithTag("l1").setSelected(true);
                } else if (getSelectedAnswersHashMap.get(position) == 2) {
                    viewLayout.findViewWithTag("l2").setBackgroundColor(Color.CYAN);
                    viewLayout.findViewWithTag("l2").setSelected(true);
                } else if (getSelectedAnswersHashMap.get(position) == 3) {
                    viewLayout.findViewWithTag("l3").setBackgroundColor(Color.CYAN);
                    viewLayout.findViewWithTag("l3").setSelected(true);
                } else if (getSelectedAnswersHashMap.get(position) == 4) {
                    viewLayout.findViewWithTag("l4").setBackgroundColor(Color.CYAN);
                    viewLayout.findViewWithTag("l4").setSelected(true);
                }
            }
        }

*/
    }

    public void showAlertDialog(String message) {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        //alertDialog.setTitle("Confirm");
        //alertDialog.setMessage("You have correctly answered " + sAllCorrectAnswerCount.size() + " out of " + mViewPager.getAdapter().getCount());

        alertDialog.setTitle("Test Complete");
        alertDialog.setMessage("Your test is complete. Yor result will be announced on " + dateTestExpiry + ".");
        alertDialog.setIcon(R.drawable.logo_05);
        alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                dialog.dismiss();
                getAllDataCal();
                calculatePercentage();
                ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected() == true) {
                    makeAServiceCall();
                } else {
                    Toast.makeText(GrandTest.this, "Network Not Available", Toast.LENGTH_LONG).show();
                }
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    public void showAlertDialogBackPressed(String message) {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Confirm");
        alertDialog.setMessage("Are you sure you want to leave test?");
        alertDialog.setIcon(R.drawable.logo_05);
        alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                clearAll();
                finish();
                overridePendingTransition(0, 0);
                startActivity(new Intent(GrandTest.this, GrandTestLandingActivity.class));
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    private void makeAServiceCall() {

        new UpdateGrandTestCompleted(getApplicationContext()).execute();

    }

    private void makeAServiceCallToUpdateAllUsers() {

        new UpdateGrandTestCompletedAllUsers(getApplicationContext()).execute();

    }


    private void clearAll() {
        correctAnswerHM.clear();
        selectedAnswerHM.clear();
        markedAnswerHM.clear();
        markedAnswerStatusHM.clear();
        markedCountAnswersMap.clear();
        skippedCountAnswersMap.clear();
        selectedPos = -1;
        markedAnswerStatusHM.clear();
        markCount = 0;
        correctData.clear();
        skippedData.clear();
        correctDefaultData.clear();
        wrongData.clear();
        comparedCorrectData.clear();
    }


    private void getAllMCQQuestions() {
        new GetMCQQuestionsFromServer(getApplicationContext()).execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (!isFinishing()) {
            showAlertDialogBackPressed("Are you sure you want to exit this Test?");
        }
    }

    /*private void startTimer(int noOfMinutes) {

        new CountDownTimer(noOfMinutes, 1000) {

            public void onTick(long duration) {
                long Mmin = (duration / 1000) / 60;
                long Ssec = (duration / 1000) % 60;
                if (Ssec < 10) {
                    counter_timer.setText("" + Mmin + ":0" + Ssec);
                } else counter_timer.setText("" + Mmin + ":" + Ssec);
            }

            public void onFinish() {
                counter_timer.setText("00:00");

                if (!isFinishing()) {
                    //showdialog here

                    //showTestCompletePopup();
                    showAlertDialog("Your time for the test id up. Submit the test");

                }

            }

        }.start();
    }*/

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(br, new IntentFilter(CountDownTimerService.COUNTDOWN_BR));
        Log.i(TAG, "Registered broacast receiver");

        final int pos = 3;

        Bundle bundleQP = getIntent().getExtras();
        if (bundleQP != null) {
            gotoPage = bundleQP.getInt("QP");
            CurrentBackPosition = bundleQP.getInt("QNO");

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(br);
        Log.i(TAG, "Unregistered broacast receiver");
    }

    @Override
    public void onStop() {
        try {
            unregisterReceiver(br);
        } catch (Exception e) {
            // Receiver was probably already stopped in onPause()
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, CountDownTimerService.class));
        Log.i(TAG, "Stopped service");
        super.onDestroy();
    }

    private void updateGUI(Intent intent) {
        if (intent.getExtras() != null) {
            long millisUntilFinished = intent.getLongExtra("countdown", 0);
            int time_in = (int) (millisUntilFinished / 1000);
            if (time_in == 0) {
                if (!isFinishing()) {
                    showAlertDialog("Your time for the test id up. Submit the test");
                }
            } else {

                long Mmin = (millisUntilFinished / 1000) / 60;
                long Ssec = (millisUntilFinished / 1000) % 60;
                counter_timer.setText(" " + Mmin + ":" + Ssec);


                //   counter_timer.setText("Time Remaining  " + millisUntilFinished / 1000);
            }
            Log.i(TAG, "Countdown seconds remaining: " + millisUntilFinished / 1000);
        }
    }

    public class UpdateGrandTestCompleted extends AsyncTask<Void, Void, Void> {
        public Context context;


        public UpdateGrandTestCompleted(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            //  progress_bar_grandTest.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerGrandTestUpdate httpPostHandler = new HttpPostHandlerGrandTestUpdate();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpPostGranTestCompletedUrl);
            Log.e("Status update Completed", "Status update Completed: " + responseFromDB);
            //questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            return null;
        }

        protected void onPostExecute(Void result) {

            makeAServiceCallToUpdateAllUsers();
        }
    }

    public class UpdateGrandTestCompletedAllUsers extends AsyncTask<Void, Void, Void> {
        public Context context;


        public UpdateGrandTestCompletedAllUsers(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progress_bar_grandTest.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerGrandTestUpdateUserBased httpPostHandlerGrandTestUpdateUserBased = new HttpPostHandlerGrandTestUpdateUserBased();
            String responseFromDB = httpPostHandlerGrandTestUpdateUserBased.makeServicePostCall(HttpPostGranTestCompletedUrlAllUsersTest);
            Log.e("Status update Completed", "Status update Completed: " + responseFromDB);
            //questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            return null;
        }

        protected void onPostExecute(Void result) {
            //progress_bar_grandTest.dismiss();
            grantTestDataModel.setTotalCorrectAnswers(sAllCorrectAnswerCount.size());
            grantTestDataModel.setCorrectAnswerMap(String.valueOf(DataModelTemp.getInstance().getGetTempCorrectAnswers()));
            grantTestDataModel.setTotalCorrectis(sAllCorrectAnswerCount.size());
            grantTestDataModel.setUserSelectedAnswerMap(String.valueOf(selectedAnswerHM));
            grantTestDataModel.setGetMarkedAnswers(markedAnswerHM);
            grantTestDataModel.setGetMarkedSelectedAnswers(markedAnswerStatusHM);
            grantTestDataModel.setGetUnMarkedUserSelectedAnswers(skippedAnswerStatusHM);


            overridePendingTransition(0, 0);
            //Intent intent = new Intent(GrandTest.this, FinalResult.class);
            Intent intent = new Intent(GrandTest.this, FinalResult.class);
            Bundle bundle = new Bundle();
            //GrantTestUserDataModel.getInstance().setGtName(grantTestDataModel.getTestName());
            bundle.putString("selected_test_name_1", grantTestDataModel.getTestName());
            bundle.putString("total_questions", String.valueOf(mViewPager.getAdapter().getCount()));
            bundle.putString("test_id", String.valueOf(grantTestDataModel.getTestId()));

            bundle.putInt("selected_test_answers", grantTestDataModel.getTotalCorrectAnswers());
            bundle.putInt("skipped_answers", grantTestDataModel.getSkippedListCompletedGrandTest());

            /*bundle.putInt("selected_test_answers", DataModelTemp.getInstance().getGetTempSelecetdAnswers().size());
            bundle.putInt("skipped_answers", DataModelTemp.getInstance().getGetTempSkippedAnswers().size());
*/
            intent.putExtras(bundle);
            startActivity(intent);


            clearAll();

            finish();

            //startActivity(new Intent(GrandTest.this, GrandTestCompletedActivity.class));

        }
    }

    public class PhotoViewAdapter extends PagerAdapter {

        Context context;
        ArrayList<GrantTestDataModel> all_folder = new ArrayList<>();
        int int_position;

        ArrayList<Integer> correctAnswersFromDB = new ArrayList<>();
        LayoutInflater inflater;
        int correctAnswerPosition;

        TextView option_id, answer_txt1, answer_txt2, answer_txt3, answer_txt4, question_txt, txt_description_answer, proceed_tv;
        ImageView imv_tick_cross1, imv_tick_cross2, imv_tick_cross3, imv_tick_cross4;
        LinearLayout ll1, ll2, ll3, ll4, ll_answer_desc;

        HashMap<Integer, Integer> getMarkedSelectedAnswers = new HashMap<>();
        HashMap<Integer, Integer> getSelectedAnswersHashMap = new HashMap<>();

        HashMap<Integer, Integer> efgh = new HashMap<>();
        int resetViewpagerPosition;
        ImageView imv_marked_list;


        //boolean hasUserChosenCorrectAnswer;


        public PhotoViewAdapter(Context context, HashMap<Integer, Integer> getMarkedSelectedAnswers, ArrayList<GrantTestDataModel> all_folder, ArrayList<Integer> allAnswerListDB, HashMap<Integer, Integer> getSelectedAnswersHashMap, int currentBackPosition) {
            this.context = context;
            this.getMarkedSelectedAnswers = getMarkedSelectedAnswers;
            this.all_folder = all_folder;
            this.correctAnswersFromDB = allAnswerListDB;
            this.getSelectedAnswersHashMap = getSelectedAnswersHashMap;
            this.resetViewpagerPosition = currentBackPosition;
            this.efgh = getSelectedAnswersHashMap;
        }


        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return all_folder.size();
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            ImageView imageView;

            QuestionDataModel questionDataModel = QuestionDataModel.getInstance();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View viewLayout = inflater.inflate(R.layout.cards_layout, container, false);

            imv_marked_list = viewLayout.findViewById(R.id.imv_marked_list);

            question_txt = viewLayout.findViewById(R.id.question_txt);
            ll_answer_desc = viewLayout.findViewById(R.id.ll_answer_desc);
            answer_txt1 = viewLayout.findViewById(R.id.answer1);
            answer_txt2 = viewLayout.findViewById(R.id.answer2);
            answer_txt3 = viewLayout.findViewById(R.id.answer3);
            answer_txt4 = viewLayout.findViewById(R.id.answer4);
            txt_description_answer = viewLayout.findViewById(R.id.txt_description_answer);
            proceed_tv = viewLayout.findViewById(R.id.proceed_tv);


            ll1 = viewLayout.findViewById(R.id.ll1);
            ll2 = viewLayout.findViewById(R.id.ll2);
            ll3 = viewLayout.findViewById(R.id.ll3);
            ll4 = viewLayout.findViewById(R.id.ll4);


            ll1.setOnClickListener(GrandTest.this);
            ll2.setOnClickListener(GrandTest.this);
            ll3.setOnClickListener(GrandTest.this);
            ll4.setOnClickListener(GrandTest.this);


            imv_tick_cross1 = viewLayout.findViewById(R.id.imv_tick_cross1);
            imv_tick_cross2 = viewLayout.findViewById(R.id.imv_tick_cross2);
            imv_tick_cross3 = viewLayout.findViewById(R.id.imv_tick_cross3);
            imv_tick_cross4 = viewLayout.findViewById(R.id.imv_tick_cross4);

            ll1.setTag("l1");
            ll2.setTag("l2");
            ll3.setTag("l3");
            ll4.setTag("l4");

            //set prevcious selected item
            //set prevcious selected item
            if (getMarkedSelectedAnswers != null) {
                if (getMarkedSelectedAnswers.containsValue(resetViewpagerPosition - 1)) {
                    intro_btn_mark.setBackgroundColor(getResources().getColor(R.color.vedantu_orange));
                } else {
                    intro_btn_mark.setBackgroundColor(0);
                }
            }


            getSelectedAnswersHashMap = grantTestDataModel.getMySelectionList();

            //getSelectedAnswersHashMap = DataModelTemp.getInstance().getGetTempSelecetdAnswers();
            if (getSelectedAnswersHashMap != null) {

                if (getSelectedAnswersHashMap.containsKey(position)) {
                    if (getSelectedAnswersHashMap.get(position) == 1) {
                        viewLayout.findViewWithTag("l1").setBackgroundColor(Color.CYAN);
                        viewLayout.findViewWithTag("l1").setSelected(true);
                    } else if (getSelectedAnswersHashMap.get(position) == 2) {
                        viewLayout.findViewWithTag("l2").setBackgroundColor(Color.CYAN);
                        viewLayout.findViewWithTag("l2").setSelected(true);
                    } else if (getSelectedAnswersHashMap.get(position) == 3) {
                        viewLayout.findViewWithTag("l3").setBackgroundColor(Color.CYAN);
                        viewLayout.findViewWithTag("l3").setSelected(true);
                    } else if (getSelectedAnswersHashMap.get(position) == 4) {
                        viewLayout.findViewWithTag("l4").setBackgroundColor(Color.CYAN);
                        viewLayout.findViewWithTag("l4").setSelected(true);
                    }
                }

            }

            imv_marked_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                    grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                    grantTestDataModel.setAllMarked_news(markedAnswerHM);


                    DataModelTemp dataModelTemp = DataModelTemp.getInstance();
                    dataModelTemp.setGetTempMarkedAnswers(markedAnswerHM);
                    dataModelTemp.setGetTempSelecetdAnswers(selectedAnswerHM);
                    dataModelTemp.setGetTempSkippedAnswers(skippedAnswerStatusHM);
                    dataModelTemp.setGetTempCorrectAnswers(correctAnswerHM);

                    Intent markActivity = new Intent(GrandTest.this, MarkedQuestionsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("total_questions_bundle", all_folder.size());
                    markActivity.putExtras(bundle);
                    startActivity(markActivity);
                }
            });


            String ques = all_folder.get(position).getGTQuestionDesc();
            String[] queStringArray = ques.split(",");


            question_txt.setText("" + queStringArray[0]);
            answer_txt1.setText("" + all_folder.get(position).

                    getGTAnswerList().

                    split(",")[0].

                    replace("\r\n", ""));
            answer_txt2.setText("" + all_folder.get(position).

                    getGTAnswerList().

                    split(",")[1].

                    replace("\r\n", ""));
            answer_txt3.setText("" + all_folder.get(position).

                    getGTAnswerList().

                    split(",")[2].

                    replace("\r\n", ""));
            answer_txt4.setText("" + all_folder.get(position).

                    getGTAnswerList().

                    split(",")[3].

                    replace("\r\n", ""));
            correctAnswerPosition = correctAnswersFromDB.get(position);


            viewLayout.findViewWithTag("l1").

                    setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (viewLayout.findViewWithTag("l1").isSelected() || viewLayout.findViewWithTag("l2").isSelected() ||
                                    viewLayout.findViewWithTag("l3").isSelected() || viewLayout.findViewWithTag("l4").isSelected()) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l1").setSelected(false);
                                viewLayout.findViewWithTag("l2").setSelected(false);
                                viewLayout.findViewWithTag("l3").setSelected(false);
                                viewLayout.findViewWithTag("l4").setSelected(false);

                                selectedPos = -1;
                                //GrantTestDataModel.getInstance().setNothingSelected(true);
                                if (selectedAnswerHM.containsKey(page)) {
                                    selectedAnswerHM.remove(page);
                                    correctAnswerHM.remove(page);

                                } else {
                                    selectedAnswerHM.remove(page);
                                    correctAnswerHM.remove(page);
                                }

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

                                if (efgh != null) {
                                    if (efgh.containsKey(page)) {
                                        efgh.remove(page);
                                        //  efgh.put(position, selectedPos);
                                    } else {

                                        //efgh.put(position, selectedPos);
                                    }
                                }

                                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                                grantTestDataModel.setAllMarked_news(markedAnswerHM);

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
                                grantTestDataModel.setMySelectionList(efgh);

                                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
                                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());


                            } else {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.CYAN);
                                viewLayout.findViewWithTag("l1").setSelected(true);

                                selectedPos = 1;

                                //GrantTestDataModel.getInstance().setNothingSelected(false);

                                if (selectedAnswerHM.containsKey(page)) {
                                    selectedAnswerHM.remove(page);
                                    selectedAnswerHM.put(page, selectedPos);
                                    correctAnswerHM.remove(page);
                                    correctAnswerHM.put(page, hmp1.get(page));

                                } else {
                                    selectedAnswerHM.remove(page);
                                    selectedAnswerHM.put(page, selectedPos);
                                    correctAnswerHM.remove(page);
                                    correctAnswerHM.put(page, hmp1.get(page));

                                }

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

                                if (efgh != null) {
                                    if (efgh.containsKey(page)) {
                                        efgh.remove(page);
                                        efgh.put(page, selectedPos);
                                    } else {

                                        efgh.put(page, selectedPos);
                                    }
                                }


                                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                                grantTestDataModel.setAllMarked_news(markedAnswerHM);

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
                                grantTestDataModel.setMySelectionList(efgh);

                                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
                                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());


                            }

                            if (hmp1.get(position) == selectedPos) {
                                correctAnswerCount = correctAnswerCount + 1;

                                if (hasUserChosenCorrectAnswer.containsKey(page)) {
                                    hasUserChosenCorrectAnswer.remove(page);
                                    hasUserChosenCorrectAnswer.put(page, true);
                                } else {
                                    hasUserChosenCorrectAnswer.put(page, true);
                                }
                            } else {
                                hasUserChosenCorrectAnswer.put(page, false);
                            }
                        }
                    });
            viewLayout.findViewWithTag("l2").

                    setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            if (viewLayout.findViewWithTag("l1").isSelected() || viewLayout.findViewWithTag("l2").isSelected() ||
                                    viewLayout.findViewWithTag("l3").isSelected() || viewLayout.findViewWithTag("l4").isSelected()) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l1").setSelected(false);
                                viewLayout.findViewWithTag("l2").setSelected(false);
                                viewLayout.findViewWithTag("l3").setSelected(false);
                                viewLayout.findViewWithTag("l4").setSelected(false);

                                selectedPos = -1;


                                //GrantTestDataModel.getInstance().setNothingSelected(true);

                                if (selectedAnswerHM.containsKey(page)) {
                                    selectedAnswerHM.remove(page);
                                    correctAnswerHM.remove(page);

                                } else {
                                    selectedAnswerHM.remove(page);
                                    correctAnswerHM.remove(page);
                                }

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

                                if (efgh != null) {
                                    if (efgh.containsKey(page)) {
                                        efgh.remove(page);
                                        //  efgh.put(position, selectedPos);
                                    } else {

                                        //efgh.put(position, selectedPos);
                                    }
                                }

                                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                                grantTestDataModel.setAllMarked_news(markedAnswerHM);

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
                                grantTestDataModel.setMySelectionList(efgh);

                                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
                                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());


                            } else {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.CYAN);
                                viewLayout.findViewWithTag("l2").setSelected(true);


                                selectedPos = 2;
                                //GrantTestDataModel.getInstance().setNothingSelected(false);

                                if (selectedAnswerHM.containsKey(page)) {
                                    selectedAnswerHM.remove(page);
                                    selectedAnswerHM.put(page, selectedPos);
                                    correctAnswerHM.remove(page);
                                    correctAnswerHM.put(page, hmp1.get(page));

                                } else {
                                    selectedAnswerHM.remove(page);
                                    selectedAnswerHM.put(page, selectedPos);
                                    correctAnswerHM.remove(page);
                                    correctAnswerHM.put(page, hmp1.get(page));
                                }

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

                                if (efgh != null) {
                                    if (efgh.containsKey(page)) {
                                        efgh.remove(page);
                                        efgh.put(page, selectedPos);
                                    } else {

                                        efgh.put(page, selectedPos);
                                    }
                                }


                                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                                grantTestDataModel.setAllMarked_news(markedAnswerHM);

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
                                grantTestDataModel.setMySelectionList(efgh);

                                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
                                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());


                            }

                            if (hmp1.get(page) == selectedPos) {
                                correctAnswerCount = correctAnswerCount + 1;

                                if (hasUserChosenCorrectAnswer.containsKey(page)) {
                                    hasUserChosenCorrectAnswer.remove(page);
                                    hasUserChosenCorrectAnswer.put(page, true);
                                } else {
                                    hasUserChosenCorrectAnswer.put(page, true);
                                }
                            } else {
                                hasUserChosenCorrectAnswer.put(page, false);
                            }
                        }
                    });
            viewLayout.findViewWithTag("l3").

                    setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            if (viewLayout.findViewWithTag("l1").isSelected() || viewLayout.findViewWithTag("l2").isSelected() ||
                                    viewLayout.findViewWithTag("l3").isSelected() || viewLayout.findViewWithTag("l4").isSelected()) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l1").setSelected(false);
                                viewLayout.findViewWithTag("l2").setSelected(false);
                                viewLayout.findViewWithTag("l3").setSelected(false);
                                viewLayout.findViewWithTag("l4").setSelected(false);

                                selectedPos = -1;


                                // GrantTestDataModel.getInstance().setNothingSelected(true);

                                if (selectedAnswerHM.containsKey(page)) {
                                    selectedAnswerHM.remove(page);
                                    correctAnswerHM.remove(page);


                                } else {
                                    selectedAnswerHM.remove(page);
                                    correctAnswerHM.remove(page);
                                }
                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

                                if (efgh != null) {
                                    if (efgh.containsKey(page)) {
                                        efgh.remove(page);
                                        //  efgh.put(position, selectedPos);
                                    } else {

                                        //efgh.put(position, selectedPos);
                                    }
                                }

                                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                                grantTestDataModel.setAllMarked_news(markedAnswerHM);

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
                                grantTestDataModel.setMySelectionList(efgh);

                                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
                                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());


                            } else {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.CYAN);
                                viewLayout.findViewWithTag("l3").setSelected(true);

                                selectedPos = 3;

                                //GrantTestDataModel.getInstance().setNothingSelected(false);
                                if (selectedAnswerHM.containsKey(page)) {
                                    selectedAnswerHM.remove(page);
                                    selectedAnswerHM.put(page, page);
                                    correctAnswerHM.remove(page);
                                    correctAnswerHM.put(page, hmp1.get(page));

                                } else {
                                    selectedAnswerHM.remove(page);
                                    selectedAnswerHM.put(page, selectedPos);
                                    correctAnswerHM.remove(page);
                                    correctAnswerHM.put(page, hmp1.get(page));
                                }


                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

                                if (efgh != null) {
                                    if (efgh.containsKey(position)) {
                                        efgh.remove(position);
                                        efgh.put(position, selectedPos);
                                    } else {

                                        efgh.put(position, selectedPos);
                                    }
                                }


                                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                                grantTestDataModel.setAllMarked_news(markedAnswerHM);

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
                                grantTestDataModel.setMySelectionList(efgh);

                                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
                                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());


                            }


                            if (hmp1.get(position) == selectedPos) {
                                correctAnswerCount = correctAnswerCount + 1;

                                if (hasUserChosenCorrectAnswer.containsKey(page)) {
                                    hasUserChosenCorrectAnswer.remove(page);
                                    hasUserChosenCorrectAnswer.put(page, true);
                                } else {
                                    hasUserChosenCorrectAnswer.put(page, true);
                                }
                            } else {
                                hasUserChosenCorrectAnswer.put(page, false);
                            }
                        }
                    });
            viewLayout.findViewWithTag("l4").

                    setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            if (viewLayout.findViewWithTag("l1").isSelected() || viewLayout.findViewWithTag("l2").isSelected() ||
                                    viewLayout.findViewWithTag("l3").isSelected() || viewLayout.findViewWithTag("l4").isSelected()) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l1").setSelected(false);
                                viewLayout.findViewWithTag("l2").setSelected(false);
                                viewLayout.findViewWithTag("l3").setSelected(false);
                                viewLayout.findViewWithTag("l4").setSelected(false);


                                selectedPos = -1;


                                // GrantTestDataModel.getInstance().setNothingSelected(true);

                                if (selectedAnswerHM.containsKey(page)) {
                                    selectedAnswerHM.remove(page);
                                    correctAnswerHM.remove(page);


                                } else {
                                    selectedAnswerHM.remove(page);
                                    correctAnswerHM.remove(page);
                                }
                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

                                if (efgh != null) {
                                    if (efgh.containsKey(page)) {
                                        efgh.remove(page);
                                        //  efgh.put(position, selectedPos);
                                    } else {

                                        //efgh.put(position, selectedPos);
                                    }
                                }

                                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                                grantTestDataModel.setAllMarked_news(markedAnswerHM);

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
                                grantTestDataModel.setMySelectionList(efgh);

                                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
                                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());


                            } else {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.CYAN);
                                viewLayout.findViewWithTag("l4").setSelected(true);

                                selectedPos = 4;
                                //GrantTestDataModel.getInstance().setNothingSelected(false);


                                if (selectedAnswerHM.containsKey(page)) {
                                    selectedAnswerHM.remove(page);
                                    selectedAnswerHM.put(page, selectedPos);
                                    correctAnswerHM.remove(page);
                                    correctAnswerHM.put(page, hmp1.get(page));

                                } else {
                                    selectedAnswerHM.put(page, selectedPos);
                                    correctAnswerHM.put(page, hmp1.get(page));

                                }
                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());

                                if (efgh != null) {
                                    if (efgh.containsKey(page)) {
                                        efgh.remove(page);
                                        efgh.put(page, selectedPos);
                                    } else {

                                        efgh.put(page, selectedPos);
                                    }
                                }


                                grantTestDataModel.setAllSkipped_news(skippedAnswerStatusHM);
                                grantTestDataModel.setAllSelected_news(selectedAnswerHM);
                                grantTestDataModel.setAllMarked_news(markedAnswerHM);

                                grantTestDataModel.setMySelectionList(grantTestDataModel.getMySelectionList());
                                grantTestDataModel.setMySelectionList(efgh);

                                grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
                                grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());


                            }
                            if (hmp1.get(position) == selectedPos) {
                                correctAnswerCount = correctAnswerCount + 1;

                                if (hasUserChosenCorrectAnswer.containsKey(page)) {
                                    hasUserChosenCorrectAnswer.remove(page);
                                    hasUserChosenCorrectAnswer.put(page, true);
                                } else {
                                    hasUserChosenCorrectAnswer.put(page, true);
                                }
                            } else {
                                hasUserChosenCorrectAnswer.put(page, false);
                            }
                        }
                    });

//            mViewPager.setOffscreenPageLimit(all_folder.size());
            container.addView(viewLayout);
            return viewLayout;
        }
    }

    public class GetMCQQuestionsFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetMCQQuestionsFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progress_bar_grandTest1.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpGrandTestPostHandler httpGetHandler = new HttpGrandTestPostHandler();
            String responseFromDB = httpGetHandler.makeServicePostCall(HttpPostUrl);
            Log.e("Ques Test Id based ", "Question responseFromDB: " + responseFromDB);
            if (responseFromDB != null) {
                questionsDataModules = BaseActivity.getGrandTestQuestions(responseFromDB);
            }

            return null;
        }

        protected void onPostExecute(Void result) {

            progress_bar_grandTest1.dismiss();
            if (questionsDataModules.size() > 0) {

                question_count_answers.setText("" + (page + 1) + "/" + questionsDataModules.size());

                for (int i = 0; i < questionsDataModules.size(); i++) {
                    hmp1.add(Integer.valueOf(questionsDataModules.get(i).getGTCorrectAmswer()));
                }

                PhotoViewAdapter photoViewAdapter = new PhotoViewAdapter(getApplicationContext(), grantTestDataModel.getGetMarkedAnswers(), questionsDataModules, hmp1, grantTestDataModel.getMySelectionList(), CurrentBackPosition);
                mViewPager.setAdapter(photoViewAdapter);

                photoViewAdapter.notifyDataSetChanged();


                mViewPager.postDelayed(new Runnable() {
                    int gotoSwapPage = gotoPage;

                    @Override
                    public void run() {
                        if (gotoPage != 0) {
                            mViewPager.setCurrentItem(gotoSwapPage, true);

                        }
                    }
                }, 1000);


                startService(new Intent(context, CountDownTimerService.class));
                Log.i(TAG, "Started service");
                mPreviousBtn.setVisibility(page == 0 ? View.GONE : View.VISIBLE);
                mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        page = position;
                        question_count_answers.setText("" + (page + 1) + " / " + questionsDataModules.size());
                        mNextBtn.setVisibility(position == questionsDataModules.size() - 1 ? View.GONE : View.VISIBLE);
                        mFinishBtn.setVisibility(position == questionsDataModules.size() - 1 ? View.VISIBLE : View.GONE);
                        mPreviousBtn.setVisibility(position == 0 ? View.GONE : View.VISIBLE);

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
        }

    }


    public void getAllDataCal() {

        progress_bar_grandTest.show();
        if (selectedPos == -1) {
            if (selectedAnswerHM.size() > 0) {
                skippedCountAnswersMap.add(page);
                skippedAnswerStatusHM.put(page, selectedPos);
                allAnswersDetails.put(page, -1);
                //selectedAnswerHM.put(page, -1);
                correctAnswerHM.put(page, hmp1.get(page));
            } else {
                skippedCountAnswersMap.add(mViewPager.getAdapter().getCount());
                for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {
                    skippedAnswerStatusHM.put(i, mViewPager.getAdapter().getCount());
                }
                correctAnswerHM.put(page, hmp1.get(page));
            }

            SkipData.put(page, -1);

        } else {
            if (skippedAnswerStatusHM.containsKey(page)) {
                skippedAnswerStatusHM.remove(page);

            }
            if (skippedCountAnswersMap.contains(page)) {
                skippedCountAnswersMap.remove(Collections.binarySearch(skippedCountAnswersMap, page));
            }
            selectedAnswerHM.put(page, selectedPos);
            correctAnswerHM.put(page, hmp1.get(page));
            allAnswersDetails.put(page, 1);
            SelData.put(page, selectedPos);
        }


        if (markedAnswerStatusHM.containsKey(page)) {
            markedAnswerStatusHM.remove(page);
            markedAnswerStatusHM.put(page, selectedPos);
            markedAnswerStatusHM.put(page, selectedPos);
            /*selectedAnswerHM.put(page, page);*/
        }


        DataModelTemp.getInstance().

                setGetTempCorrectAnswers(correctAnswerHM);
        grantTestDataModel.setGetSkippedAnswers(skippedCountAnswersMap);
        grantTestDataModel.setSkippedListCompletedGrandTest(skippedCountAnswersMap.size());
        grantTestDataModel.setAllSkipped(skippedAnswerStatusHM);
        sAllCorrectAnswerCount.clear();


        DataModelTemp dataModelTemp = DataModelTemp.getInstance();
        dataModelTemp.setGetTempMarkedAnswers(markedAnswerHM);
        dataModelTemp.setGetTempSelecetdAnswers(selectedAnswerHM);
        dataModelTemp.setGetTempSkippedAnswers(skippedAnswerStatusHM);
        dataModelTemp.setGetTempCorrectAnswers(correctAnswerHM);

        if (selectedAnswerHM.size() > 0) {

            for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {
                if (selectedAnswerHM.containsKey(i)) {
                    if (selectedAnswerHM.get(i).equals(-1)) {
                        correctData.put(i, 0);
                    } else {
                        correctData.put(i, selectedAnswerHM.get(i));
                    }
                } else {
                    correctData.put(i, 0);
                }
            }
        }

        if (skippedAnswerStatusHM.size() > 0) {
            for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {
                if (skippedAnswerStatusHM.containsKey(i)) {
                    skippedData.put(i, skippedAnswerStatusHM.get(i));
                    comparedSkipData.put(i, skippedAnswerStatusHM.get(i));
                } else {
                    skippedData.put(i, 0);
                }
            }
        }


        for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {
            correctDefaultData.put(i, hmp1.get(i));
        }


        if (correctData.size() > 0) {
            for (int i = 0; i < mViewPager.getAdapter().getCount(); i++) {

                if (correctData.get(i).equals(correctDefaultData.get(i))) {
                    comparedCorrectData.put(i, correctData.get(i));
                } else {
                    if (!correctData.get(i).equals(0)) {
                        wrongData.put(i, correctData.get(i));
                        comparedWrongData.put(i, correctData.get(i));
                    }

                }
            }
        }

        SkipDataTotalCount = mViewPager.getAdapter().getCount() - selectedAnswerHM.size();
        Log.i("SelPos", "" + SelData);
        Log.i("SkipPos", "" + SkipData);


        double firstStep = (comparedWrongData.size() * 0.25);

        int totalSkippedScore = comparedSkipData.size();
        int totalCorrectScore = comparedCorrectData.size();

        //double secondStep = firstStep;

        finalUserScore = totalCorrectScore - firstStep;

        grantTestDataModel.setUserFinalScore(String.format("%.2f", finalUserScore));
        GrantTestUserDataModel.getInstance().setFinalScore(String.format("%.2f", finalUserScore));
        //}

        grantTestDataModel.setCorrectString(String.valueOf(correctData));
        grantTestDataModel.setSkippedString(String.valueOf(skippedData));
        grantTestDataModel.setActualCorrectString(String.valueOf(correctDefaultData));
        grantTestDataModel.setUserWrongAnswers(String.valueOf(wrongData));

        grantTestDataModel.setCorrectCount(comparedCorrectData.size());
        grantTestDataModel.setSkipCount(SkipDataTotalCount);
        grantTestDataModel.setWrongCount(comparedWrongData.size());


        GrantTestUserDataModel.getInstance().setCorrectAnswers(String.valueOf(correctData));
        GrantTestUserDataModel.getInstance().setSkippedAnswers(String.valueOf(skippedData));
        GrantTestUserDataModel.getInstance().setWrongAnswers(String.valueOf(wrongData));
        GrantTestUserDataModel.getInstance().setActualCorrectString(String.valueOf(correctDefaultData));

        GrantTestUserDataModel.getInstance().setCorrectCount(comparedCorrectData.size());
        GrantTestUserDataModel.getInstance().setSkipCount(SkipDataTotalCount);
        GrantTestUserDataModel.getInstance().setWrongCount(comparedWrongData.size());


        grantTestDataModel.setTotalCorrectAnswers(sAllCorrectAnswerCount.size());
        grantTestDataModel.setCorrectAnswerMap(String.valueOf(DataModelTemp.getInstance().getGetTempCorrectAnswers()));
        grantTestDataModel.setTotalCorrectis(sAllCorrectAnswerCount.size());
        grantTestDataModel.setUserSelectedAnswerMap(String.valueOf(selectedAnswerHM));
        grantTestDataModel.setGetMarkedAnswers(markedAnswerHM);
        grantTestDataModel.setGetMarkedSelectedAnswers(markedAnswerStatusHM);
        grantTestDataModel.setGetUnMarkedUserSelectedAnswers(skippedAnswerStatusHM);

    }
}
