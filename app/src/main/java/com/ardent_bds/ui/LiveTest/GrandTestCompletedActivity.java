package com.ardent_bds.ui.LiveTest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.ui.home.GrantTestDataModel;
import java.util.ArrayList;

public class GrandTestCompletedActivity extends Activity {


    private TextView startSolveTest, grand_testname, totalCorrectAnswers, tv_totalQuestions, total_correct_out_of, total_questions_asked, total_percentile;

    ArrayList pieEntries;
    ArrayList PieEntryLabels;

    GrantTestDataModel grantTestDataModel;


    ProgressBar correctPro, skippedPro, wrongPro;

    TextView tv_correct_value, tv_skipped_value, tv_wrong_value;


    private float dataInt;
    private String dataIntToString;

    private double percentageOfCompletedTest;


    private ImageView close_report;

    float getTotalCorrectAnswerInt;
    float getTotalQuestionsInTestInt;


    String selected_test_name;
    String total_questions;

    int selected_test_answersInt;
    int skipped_answersCount;
    int totalWrongAnswers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.gt_completed_activity);
        startSolveTest = findViewById(R.id.startSolveTest);


        total_correct_out_of = findViewById(R.id.total_correct_answers);
        total_questions_asked = findViewById(R.id.total_questions_asked);
        total_percentile = findViewById(R.id.total_percentile);


        tv_correct_value = findViewById(R.id.txtProgressCorrect);
        tv_skipped_value = findViewById(R.id.txtProgressSkipped);
        tv_wrong_value = findViewById(R.id.txtProgressWrong);


        correctPro = findViewById(R.id.progressBarCorrect);
        skippedPro = findViewById(R.id.progressBarSkipped);
        wrongPro = findViewById(R.id.progressBarWrong);


        tv_totalQuestions = findViewById(R.id.tv_totalQuestions);
        totalCorrectAnswers = findViewById(R.id.totalCorrectAnswers);
        startSolveTest = findViewById(R.id.startSolveTest);
        grand_testname = findViewById(R.id.grand_testname);
        close_report = findViewById(R.id.close_report);

        grantTestDataModel = GrantTestDataModel.getInstance();


        Bundle bundle = getIntent().getExtras();
        selected_test_name = bundle.getString("selected_test_name");
        total_questions = bundle.getString("total_questions");

        selected_test_answersInt = bundle.getInt("selected_test_answers");
        //skipped_answersCount = bundle.getInt("skipped_answers");
        skipped_answersCount = grantTestDataModel.getSkippedListCompletedGrandTest();

        ///grand_testname.setText("" + selected_test_name);
        total_correct_out_of.setText("" + selected_test_answersInt);
        total_questions_asked.setText("Total Score out of " + total_questions);


        totalWrongAnswers = Integer.parseInt(total_questions) - (selected_test_answersInt + skipped_answersCount);
        getTotalCorrectAnswerInt = selected_test_answersInt;
        getTotalQuestionsInTestInt = Float.parseFloat(total_questions);
        dataInt = getTotalCorrectAnswerInt / getTotalQuestionsInTestInt * 100;
        dataIntToString = String.format("%.2f", dataInt);

        total_percentile.setText("" + dataIntToString + " Percentile");

        // setPieChart();


        tv_correct_value.setText("" + selected_test_answersInt);
        tv_skipped_value.setText("" + skipped_answersCount);
        tv_wrong_value.setText("" + totalWrongAnswers);

        correctPro.setProgress((int) getTotalCorrectAnswerInt);
        wrongPro.setProgress(totalWrongAnswers);
        skippedPro.setProgress(skipped_answersCount);


        close_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(0, 0);
                startActivity(new Intent(GrandTestCompletedActivity.this, GrandTestLandingActivity.class));
            }
        });
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
        startActivity(new Intent(GrandTestCompletedActivity.this, GrandTestLandingActivity.class));
    }
}
