package com.ardent_bds.ui.gallery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.ui.LiveTest.GrandTestLandingActivity;
import com.ardent_bds.ui.mcq_test_module.MCQTest;

public class AboutUsActivity extends Activity {


    ImageView iv_back_about_us;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.about_us);


        iv_back_about_us = findViewById(R.id.iv_back_about_us);

        iv_back_about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = getIntent().getExtras();

//Extract the data…
                String screen_name = bundle.getString("screen_name");

                Log.i("screen_name about us>>", "" + screen_name);

                if (screen_name.equalsIgnoreCase("SlideMenu")) {
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(new Intent(AboutUsActivity.this, DashBoardActivity.class));
                } else if (screen_name.equalsIgnoreCase("MCQTest")) {
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(new Intent(AboutUsActivity.this, MCQTest.class));
                } else if (screen_name.equalsIgnoreCase("GrandTestLandingActivity")) {
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(new Intent(AboutUsActivity.this, GrandTestLandingActivity.class));
                } else if (screen_name.equalsIgnoreCase("AllVideosSlidesNotesActivity")) {
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(new Intent(AboutUsActivity.this, AllVideosSlidesNotesActivity.class));
                }
            }
        });

    }
}
