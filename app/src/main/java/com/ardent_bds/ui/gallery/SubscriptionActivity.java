package com.ardent_bds.ui.gallery;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;

import com.ardent_bds.ApiInterface;
import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.SubscriptionModel;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.ui.GrantTestUserDataModel;
import com.ardent_bds.ui.LiveTest.GrandTestLandingActivity;
import com.ardent_bds.ui.UserAccessLevelModel;
import com.ardent_bds.ui.mcq_test_module.MCQTest;
import com.ardent_bds.ui.mcq_test_module.McqSubModuleActivity;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SubscriptionActivity extends Activity implements PaymentResultListener {

    private static final String TAG = SubscriptionActivity.class.getSimpleName();

    ImageView iv_back_subscription;

    private ApiInterface postsService;

    TextView id1, id2, id3, id4, id5, id6;
    TextView planA, planB, planC, planD, planE, planF, planACost, planBCost, planCCost, planDCost, planECost, planFCost;
    TextView mod1, mod2, mod3, mod4, mod5, mod6;
    TextView mod11, mod12, mod13, mod14, mod15, mod16;
    TextView mod21, mod22, mod23, mod24, mod25, mod26;
    TextView mod31, mod32, mod33, mod34, mod35, mod36;
    TextView mod41, mod42, mod43, mod44, mod45, mod46;
    TextView mod51, mod52, mod53, mod54, mod55, mod56;
    TextView mod61, mod62, mod63, mod64, mod65, mod66;
    CardView cdv1, cdv2, cdv3, cdv4, cdv5, cdv6;

    Checkout checkout;
    ArrayList<UserAccessLevelModel> userAccessList;
    UserModel userModel;
    String screen_name;
    String selectedPlan;
    String HttpPostGranTestCompletedUrl = Constants.BaseUrl + "/ArdentDB/suggested_test_update.php";
    String HttpPostUserAccessUrl = Constants.BaseUrl + "/ArdentDB/userStatusUpdate.php";
    String HttpGetURL_Updated_User_details = Constants.BaseUrl + "/ArdentDB/getUpdatedUserDetails.php";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_subcription_plans);

        Checkout.preload(getApplicationContext());

        userModel = UserModel.getInstance();
        userAccessList = new ArrayList<>();
        iv_back_subscription = findViewById(R.id.iv_back_subscription);

        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {

            getUpdatedUserData();
        } else {
            Toast.makeText(SubscriptionActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


        id1 = findViewById(R.id.id1);
        id2 = findViewById(R.id.id2);
        id3 = findViewById(R.id.id3);
        id4 = findViewById(R.id.id4);
        id5 = findViewById(R.id.id5);
        id6 = findViewById(R.id.id6);

        mod1 = findViewById(R.id.mod1);
        mod2 = findViewById(R.id.mod2);
        mod3 = findViewById(R.id.mod3);
        mod4 = findViewById(R.id.mod4);
        mod5 = findViewById(R.id.mod5);
        mod6 = findViewById(R.id.mod6);

        mod11 = findViewById(R.id.mod11);
        mod12 = findViewById(R.id.mod12);
        mod13 = findViewById(R.id.mod13);
        mod14 = findViewById(R.id.mod14);
        mod15 = findViewById(R.id.mod15);

        mod21 = findViewById(R.id.mod21);
        mod22 = findViewById(R.id.mod22);
        mod23 = findViewById(R.id.mod23);
        mod24 = findViewById(R.id.mod24);
        mod25 = findViewById(R.id.mod25);

        mod31 = findViewById(R.id.mod31);
        mod32 = findViewById(R.id.mod32);
        mod33 = findViewById(R.id.mod33);
        mod34 = findViewById(R.id.mod34);
        mod35 = findViewById(R.id.mod35);

        mod41 = findViewById(R.id.mod41);
        mod42 = findViewById(R.id.mod42);
        mod43 = findViewById(R.id.mod43);
        mod44 = findViewById(R.id.mod44);
        mod45 = findViewById(R.id.mod45);


        mod51 = findViewById(R.id.mod51);
        mod52 = findViewById(R.id.mod52);
        mod53 = findViewById(R.id.mod53);
        mod54 = findViewById(R.id.mod54);
        mod55 = findViewById(R.id.mod55);


        mod61 = findViewById(R.id.mod61);
        mod62 = findViewById(R.id.mod62);
        mod63 = findViewById(R.id.mod63);
        mod64 = findViewById(R.id.mod64);
        mod65 = findViewById(R.id.mod65);


        cdv1 = findViewById(R.id.cdv1);
        cdv2 = findViewById(R.id.cdv2);
        cdv3 = findViewById(R.id.cdv3);
        cdv4 = findViewById(R.id.cdv4);
        cdv5 = findViewById(R.id.cdv5);
        cdv6 = findViewById(R.id.cdv6);


        planA = findViewById(R.id.planA);
        planACost = findViewById(R.id.planACost);
        planB = findViewById(R.id.planB);
        planBCost = findViewById(R.id.planBCost);
        planC = findViewById(R.id.planC);
        planCCost = findViewById(R.id.planCCost);
        planD = findViewById(R.id.planD);
        planDCost = findViewById(R.id.planDCost);
        planE = findViewById(R.id.planE);
        planECost = findViewById(R.id.planECost);
        planF = findViewById(R.id.planF);
        planFCost = findViewById(R.id.planFCost);

        Bundle bundle = getIntent().getExtras();
        screen_name = bundle.getString("screen_name");

        iv_back_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("screenname subscription", "" + screen_name);
                if (screen_name.equalsIgnoreCase("SlideMenu")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(SubscriptionActivity.this, DashBoardActivity.class));
                } else if (screen_name.equalsIgnoreCase("MCQTest")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(SubscriptionActivity.this, MCQTest.class));
                } else if (screen_name.equalsIgnoreCase("GrandTestLandingActivity")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(SubscriptionActivity.this, GrandTestLandingActivity.class));
                } else if (screen_name.equalsIgnoreCase("AllVideosSlidesNotesActivity")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(SubscriptionActivity.this, AllVideosSlidesNotesActivity.class));
                } else if (screen_name.equalsIgnoreCase("GT")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(SubscriptionActivity.this, GrandTestLandingActivity.class));
                } else if (screen_name.equalsIgnoreCase("VC")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(SubscriptionActivity.this, AllVideosSlidesNotesActivity.class));
                } else if (screen_name.equalsIgnoreCase("MCQ")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(new Intent(SubscriptionActivity.this, McqSubModuleActivity.class));
                }
            }
        });


        cdv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPlan = "" + planA.getText().toString();


                String a = mod1.getText().toString() + " " + mod2.getText().toString() + "\n";
                String b = mod3.getText().toString() + " " + mod4.getText().toString() + "\n";
                String c = mod5.getText().toString() + " " + mod6.getText().toString() + "\n";


                customAlertDialog(selectedPlan, String.valueOf(Integer.parseInt(planACost.getText().toString())), a, b, c);
                // startPayment(Integer.parseInt(planACost.getText().toString()) * 100, planA.getText().toString());
            }
        });

        cdv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPlan = "" + planB.getText().toString();
                String a = mod11.getText().toString() + " " + mod21.getText().toString() + "\n";
                String b = mod31.getText().toString() + " " + mod41.getText().toString() + "\n";
                String c = mod51.getText().toString() + " " + mod61.getText().toString() + "\n";
                customAlertDialog(selectedPlan, String.valueOf(Integer.parseInt(planBCost.getText().toString())), a, b, c);
                //startPayment(Integer.parseInt(planBCost.getText().toString()) * 100, planB.getText().toString());
            }
        });

        cdv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPlan = "" + planC.getText().toString();
                String a = mod12.getText().toString() + " " + mod22.getText().toString() + "\n";
                String b = mod32.getText().toString() + " " + mod42.getText().toString() + "\n";
                String c = mod52.getText().toString() + " " + mod62.getText().toString() + "\n";

                customAlertDialog(selectedPlan, String.valueOf(Integer.parseInt(planCCost.getText().toString())), a, b, c);
                //startPayment(Integer.parseInt(planCCost.getText().toString()) * 100, planC.getText().toString());
            }
        });

        cdv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPlan = "" + planD.getText().toString();
                String a = mod13.getText().toString() + " " + mod23.getText().toString() + "\n";
                String b = mod33.getText().toString() + " " + mod43.getText().toString() + "\n";
                String c = mod53.getText().toString() + " " + mod63.getText().toString() + "\n";

                customAlertDialog(selectedPlan, String.valueOf(Integer.parseInt(planDCost.getText().toString())), a, b, c);
                //startPayment(Integer.parseInt(planDCost.getText().toString()) * 100, planD.getText().toString());
            }
        });

        cdv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPlan = "" + planE.getText().toString();
                String a = mod14.getText().toString() + " " + mod24.getText().toString() + "\n";
                String b = mod34.getText().toString() + " " + mod44.getText().toString() + "\n";
                String c = mod54.getText().toString() + " " + mod64.getText().toString() + "\n";

                customAlertDialog(selectedPlan, String.valueOf(Integer.parseInt(planECost.getText().toString())), a, b, c);
                //startPayment(Integer.parseInt(planECost.getText().toString()) * 100, planE.getText().toString());
            }
        });

        cdv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPlan = "" + planF.getText().toString();
                String a = mod15.getText().toString() + " " + mod25.getText().toString() + "\n";
                String b = mod35.getText().toString() + " " + mod45.getText().toString() + "\n";
                String c = mod55.getText().toString() + " " + mod65.getText().toString() + "\n";
                customAlertDialog(selectedPlan, String.valueOf(Integer.parseInt(planFCost.getText().toString())), a, b, c);
                //startPayment(Integer.parseInt(planFCost.getText().toString()) * 100, planF.getText().toString());
            }
        });
    }

    public void startPayment(int cost, String Plan) {
        Checkout checkout = new Checkout();
        checkout.setKeyID("rzp_live_qTKAB6hf56N6yk");
        checkout.setImage(R.drawable.logo);
        final Activity activity = this;
        try {
            JSONObject options = new JSONObject();
            options.put("name", userModel.getUserName());
            options.put("description", Plan);
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
/*
            options.put("order_id", "12345465");
*/
            options.put("currency", "INR");
            //options.put("amount", cost); // in paise eg. 100paise = 1 rupee
            options.put("amount", 100); // in paise eg. 100paise = 1 rupee
            checkout.open(activity, options);
        } catch (Exception e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        showSuccessAlert(selectedPlan, "Success", s, " now ", 2);
    }

    @Override
    public void onPaymentError(int i, String s) {
        showFailureAlert(selectedPlan, "Failed", s, " not ", 1);
    }


    public void showSuccessAlert(String type, String title, String content, String message, int paymentType) {
        new SweetAlertDialog(SubscriptionActivity.this, paymentType)
                .setTitleText(title)
                .setContentText("You are " + message + "Subscribed to " + type + ".\n" + "Transaction ID " + content + ".")
                //.setConfirmText("You are " + message + "to the plan" + type + "." + "Your transacrtion ID is :" + paymentId)
                .setConfirmText("ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();


                        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
                        if (networkInfo != null && networkInfo.isConnected() == true) {

                            makeServiceUpdateAccessLevelCall();
                            makeUserAccessUpdate();

                        } else {
                            Toast.makeText(SubscriptionActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
                        }


                    }
                })
                .show();


    }

    public void showFailureAlert(String type, String title, String content, String message, int paymentType) {
        new SweetAlertDialog(SubscriptionActivity.this, paymentType)
                .setTitleText(title)
                .setContentText("" + message + "You are not Subscribed to the plan" + type + ".\n")
                //.setConfirmText("You are " + message + "to the plan" + type + "." + "Your transacrtion ID is :" + paymentId)
                .setConfirmText("ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();


    }

    private void makeServiceUpdateAccessLevelCall() {

        new UpdateGrandTestCompleted(getApplicationContext()).execute();

    }

    private void makeUserAccessUpdate() {
        new UpdateUserAccess(getApplicationContext()).execute();
    }

    public class UpdateGrandTestCompleted extends AsyncTask<Void, Void, Void> {
        public Context context;


        public UpdateGrandTestCompleted(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerGrandTestAccessUpdate httpPostHandler = new HttpPostHandlerGrandTestAccessUpdate();
            String responseFromDB = httpPostHandler.makeAccessUpdateServicePostCall(HttpPostGranTestCompletedUrl);
            if (responseFromDB != null) {
                Log.e("Status access update", "Status access update Completed: " + responseFromDB);
                //questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {

        }
    }

    public class UpdateUserAccess extends AsyncTask<Void, Void, Void> {
        public Context context;


        public UpdateUserAccess(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerUserAccessUpdateLiveTest httpPostHandler = new HttpPostHandlerUserAccessUpdateLiveTest();
            if (screen_name.equalsIgnoreCase("GT")) {
                String responseFromDB = httpPostHandler.makeAccessUpdateServicePostCall(HttpPostUserAccessUrl);
                if (responseFromDB != null) {
                    Log.e("Status access update", "Status access update Completed: " + responseFromDB);
                }
            } else if (screen_name.equalsIgnoreCase("VC")) {
                String responseFromDB = httpPostHandler.makeAccessUpdateServicePostCallVideo(HttpPostUserAccessUrl);
                if (responseFromDB != null) {
                    Log.e("Status access update", "Status access update Completed: " + responseFromDB);
                }

            } else if (screen_name.equalsIgnoreCase("MCQ")) {
                String responseFromDB = httpPostHandler.makeAccessUpdateServicePostCallMCQ(HttpPostUserAccessUrl);
                if (responseFromDB != null) {
                    Log.e("Status access update", "Status access update Completed: " + responseFromDB);
                }
            }


            //questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            return null;
        }

        protected void onPostExecute(Void result) {

        }
    }


    private void getUpdatedUserData() {

       // new GetAllUserUpdateData(this).execute();
    }



    private void makeServiceCall(String userID) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        postsService = retrofit.create(ApiInterface.class);


        sendPost(userID);


    }

    private void sendPost(String userID) {

        Log.i("userID>>Yes111","" + userID);

        SubscriptionModel.getInstance().setUserid(GrantTestUserDataModel.getInstance().getUserID());



        /*Call<PostModel> call = postsService.savePost("1",PostModel.getInstance().getToken());*/

        Call<SubscriptionModel> call = postsService.getSubscriptionData(userID);
        call.enqueue(new Callback<SubscriptionModel>() {
            @Override
            public void onResponse(Call<SubscriptionModel> call, Response<SubscriptionModel> response) {
                //   lblresponse.setText(response.body().toString());


                Log.i("response??PP","" + response);
            }

            @Override
            public void onFailure(Call<SubscriptionModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
            }

        });
    }






    /*public class GetAllUserUpdateData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllUserUpdateData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostUpdatedUserDetailsHandler httpPostHandler = new HttpPostUpdatedUserDetailsHandler();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Updated_User_details);
            if (responseFromDB != null) {
                Log.e("Grant Test>>", "Grant Test data responseFromDB: " + responseFromDB);
                userAccessList = getUserData(responseFromDB);
                UserAccessLevelModel.getInstance().setUpdatedUserAccessLevel(userAccessList.get(0).getUpdatedUserAccessLevel());
                UserModel.getInstance().setUserAccessLevel(Integer.parseInt(userAccessList.get(0).getUpdatedUserAccessLevel()));
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            //UserAccessLevelModel.getInstance().setUpdatedUserAccessLevel(Us);


        }
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


   /* private void showCustomDialog(final String planname, final String plancost, String plandetails1, String plandetail2, String plandetails3) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup

        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.subcription_pop_up, viewGroup, false);


        TextView tv_click_buy_now = dialogView.findViewById(R.id.tv_click_buy_now);
        TextView plan_name = dialogView.findViewById(R.id.plan_name);
        TextView plan_details = dialogView.findViewById(R.id.plan_details);
        TextView plan_cost = dialogView.findViewById(R.id.plan_cost);
        TextView plan_taken_by = dialogView.findViewById(R.id.plan_taken_by);
        TextView pln_total_cost = dialogView.findViewById(R.id.pln_total_cost);
        TextView plan_sub_total_cost = dialogView.findViewById(R.id.plan_total_cost);


        plan_name.setText("" + planname);
        plan_details.setText("" + plandetails1);
        plan_cost.setText(plandetail2 + " " + plandetails3);
        pln_total_cost.setText("" + plancost);
        plan_sub_total_cost.setText("" + plancost);
        //plan_taken_by.setText("" + plancost);

        tv_click_buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                startPayment(Integer.parseInt(plancost), planname);
            }
        });


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        alertDialog = builder.create();
        alertDialog.show();
    }*/


    private void customAlertDialog(final String planname, final String plancost, String plandetails1, String plandetail2, String plandetails3) {

        // custom dialog
        final Dialog dialogView = new Dialog(SubscriptionActivity.this);
        dialogView.setContentView(R.layout.subcription_pop_up);
        //dialogView.setTitle("Title...");


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogView.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialogView.show();
        dialogView.getWindow().setAttributes(lp);


        TextView tv_click_buy_now = dialogView.findViewById(R.id.tv_click_buy_now);
        TextView plan_name = dialogView.findViewById(R.id.plan_name);
        TextView plan_details = dialogView.findViewById(R.id.plan_details);
        TextView plan_cost = dialogView.findViewById(R.id.plan_cost);
        TextView plan_taken_by = dialogView.findViewById(R.id.plan_taken_by);
        TextView pln_total_cost = dialogView.findViewById(R.id.pln_total_cost);
        TextView plan_sub_total_cost = dialogView.findViewById(R.id.plan_total_cost);


        plan_name.setText("" + planname);
        plan_details.setText("" + plandetails1);
        plan_cost.setText(plandetail2 + " " + plandetails3);
        pln_total_cost.setText("" + plancost);
        plan_sub_total_cost.setText("" + plancost);


        tv_click_buy_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPayment(Integer.parseInt(plancost), planname);
                dialogView.dismiss();
            }
        });

        dialogView.show();
    }


}