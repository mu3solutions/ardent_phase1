package com.ardent_bds.ui.gallery;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.AllSlidesFragment;
import com.ardent_bds.R;
import com.ardent_bds.adapters.VedioRecyclerAdapter;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.project_utils.HttpHandler;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.UserAccessLevelModel;
import com.ardent_bds.ui.mcq_of_day.Mcq;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.util.ArrayList;


public class AllViedosSlidesFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    private ImageView menu_slider_right;
    ArrayList<VedioDataModule> vedioDataModules;
    VedioDataModule dataModule;
    MeowBottomNavigation bottomNavigation;
    private AppBarConfiguration mAppBarConfiguration;
    NavigationView navigationView;
    JSONObject jsonObject = null;
    DrawerLayout drawer;
    String HttpGetURL = Constants.BaseUrl + "/ArdentDB/select_all_modules_details.php";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);


        View root = inflater.inflate(R.layout.fragment_gallery, container, false);

        recyclerView = root.findViewById(R.id.recycler_view_vedio);
        menu_slider_right = root.findViewById(R.id.menu_slider_right);


        menu_slider_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT, true);
            }
        });


        galleryViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });

        vedioDataModules = new ArrayList<>();
        //insertVedioModuleData();



        ConnectivityManager ConnectionManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {

            getAllVideoData();
        }
        else{
            Toast.makeText(getActivity(), "Network Not Available", Toast.LENGTH_LONG).show();
        }





        return root;
    }

    private void getAllVideoData() {
        new GetVideoListDataFromServer(getActivity()).execute();
    }

    private void insertVedioModuleData() {
        dataModule = new VedioDataModule();
        dataModule.setHeading("Heading 1");
        dataModule.setDesc("Desc content of the headin modle one");
        dataModule.setCompletedVedio("5");
        dataModule.setTime("20hrs");
        dataModule.setTotalVedio("18");
        vedioDataModules.add(dataModule);

        dataModule = new VedioDataModule();
        dataModule.setHeading("Heading 2");
        dataModule.setDesc("Desc content of the headin modle one");
        dataModule.setCompletedVedio("5");
        dataModule.setTime("20hrs");
        dataModule.setTotalVedio("18");
        vedioDataModules.add(dataModule);

        dataModule = new VedioDataModule();
        dataModule.setHeading("Heading 3");
        dataModule.setDesc("Desc content of the headin modle one");
        dataModule.setCompletedVedio("5");
        dataModule.setTime("20hrs");
        dataModule.setTotalVedio("18");
        vedioDataModules.add(dataModule);

        dataModule = new VedioDataModule();
        dataModule.setHeading("Heading 4");
        dataModule.setDesc("Desc content of the headin modle one");
        dataModule.setCompletedVedio("5");
        dataModule.setTime("20hrs");
        dataModule.setTotalVedio("18");
        vedioDataModules.add(dataModule);

        dataModule = new VedioDataModule();
        dataModule.setHeading("Heading 5");
        dataModule.setDesc("Desc content of the headin modle one");
        dataModule.setCompletedVedio("5");
        dataModule.setTime("20hrs");
        dataModule.setTotalVedio("18");
        vedioDataModules.add(dataModule);

        dataModule = new VedioDataModule();
        dataModule.setHeading("Heading 6");
        dataModule.setDesc("Desc content of the headin modle one");
        dataModule.setCompletedVedio("5");
        dataModule.setTime("20hrs");
        dataModule.setTotalVedio("18");
        vedioDataModules.add(dataModule);

        dataModule = new VedioDataModule();
        dataModule.setHeading("Heading 7");
        dataModule.setDesc("Desc content of the headin modle one");
        dataModule.setCompletedVedio("5");
        dataModule.setTime("20hrs");
        dataModule.setTotalVedio("18");
        vedioDataModules.add(dataModule);

    }


    public class GetVideoListDataFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetVideoListDataFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            HttpHandler httpHandler = new HttpHandler();
            String responseFromDB = httpHandler.makeServiceCall(HttpGetURL);
            if(responseFromDB!=null) {
                vedioDataModules = BaseActivity.getVideoDetailsList(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            //userModel.setUserOnlineStatus(true);
            //userModel.setUserStatus("Online");
            /*Intent intent = new Intent(getApplicationContext(), NavigationDrawerActivity.class);
            //intent.putExtra("account", acco);
            startActivity(intent);*/


            recyclerView.setHasFixedSize(true);

            layoutManager = new GridLayoutManager(getActivity(), 2);
            recyclerView.setLayoutManager(layoutManager);

            adapter = new VedioRecyclerAdapter(vedioDataModules);
            recyclerView.setAdapter(adapter);

        }
    }

}
