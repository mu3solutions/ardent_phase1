package com.ardent_bds.ui.gallery;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.adapters.VedioRecyclerAdapter;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.model.VedioUserData;
import com.ardent_bds.project_utils.HttpHandler;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.LiveTest.GrandTestLandingActivity;
import com.ardent_bds.ui.MainActivity;
import com.ardent_bds.ui.mcq_test_module.MCQTest;
import com.bumptech.glide.Glide;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.util.ArrayList;

public class AllVideosSlidesNotesActivity extends Activity implements NavigationView.OnNavigationItemSelectedListener {

    private GalleryViewModel galleryViewModel;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    private ImageView menu_slider_right, menu;
    ArrayList<VedioDataModule> vedioDataModules;
    ArrayList<VedioUserData> vedioUserDatas;
    VedioDataModule dataModule;
    MeowBottomNavigation bottomNavigation;
    private AppBarConfiguration mAppBarConfiguration;
    NavigationView navigationView;
    JSONObject jsonObject = null;
    DrawerLayout drawer;
    String HttpGetURL = Constants.BaseUrl + "/ArdentDB/vedioModuleData.php";
    String HttpGetURL1 = Constants.BaseUrl + "/ArdentDB/vedioUserData.php";

    private ProgressDialog progress_bar_login;
    String TAG = getClass().getSimpleName();
    UserModel userModel;
    TextView userName, userEmailId;
    ImageView userImage;

    TextView toolText;
    ImageView toolImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.fragment_gallery_1);


        toolImage = findViewById(R.id.tool_image);
        toolText = findViewById(R.id.tool_text);
        initCollapsingToolbar();

        recyclerView = findViewById(R.id.recycler_view_vedio);
        menu_slider_right = findViewById(R.id.menu_slider_right);
        menu = findViewById(R.id.tool_image);
        drawer = findViewById(R.id.drawer_layout_videos);

        NavigationView navigationView = findViewById(R.id.nav_view_all_videos);
        navigationView.setNavigationItemSelectedListener(this);


        userModel = UserModel.getInstance();

        View nav = navigationView.getHeaderView(0);


        userName = nav.findViewById(R.id.userName);
        userEmailId = nav.findViewById(R.id.userEmailId);
        userImage = nav.findViewById(R.id.userImage);


        userName.setText("" + userModel.getUserName());
        userEmailId.setText("" + userModel.getUserEmailId());

        if (UserModel.getInstance().getUserProfilePic().equalsIgnoreCase("")) {
        } else {
            Glide.with(AllVideosSlidesNotesActivity.this)
                    .load(UserModel.getInstance().getUserProfilePic())
                    .into(userImage);
        }


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT, true);
            }
        });

        menu_slider_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT, true);
            }
        });

        toolImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT, true);
            }
        });


        vedioDataModules = new ArrayList<>();
        //insertVedioModuleData();


        bottomNavigation = findViewById(R.id.bottom_nav);
        bottomViewNavigation();

        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getAllVideoData();
        } else {

            Toast.makeText(AllVideosSlidesNotesActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingToolbar.setTitle(" nn");
        //collapsingToolbar.setPointerIcon(R.drawable.ic_baseline_arrow_back_24);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();

                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Bala");
                    toolImage.setVisibility(View.GONE);
                    toolText.setVisibility(View.VISIBLE);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" Bla");
                    toolImage.setVisibility(View.VISIBLE);
                    toolText.setVisibility(View.GONE);
                    isShow = false;
                }
            }
        });
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_payment) {
            Intent i1 = new Intent(this, PaymentHistory.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i1.putExtras(bundle);
            startActivity(i1);

        } else if (id == R.id.nav_subscribe) {
            //startActivity(new Intent(this, SubscriptionActivity.class));

            Intent i2 = new Intent(this, SubscriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i2.putExtras(bundle);
            startActivity(i2);

        } else if (id == R.id.nav_bookmark) {

        } else if (id == R.id.nav_share_app) {
            shareApp();

        } else if (id == R.id.nav_help) {
            //startActivity(new Intent(this, ContactUsActivity.class));

            Intent i3 = new Intent(this, ContactUsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i3.putExtras(bundle);
            startActivity(i3);


        } else if (id == R.id.nav_menu_about_us) {
            //startActivity(new Intent(this, AboutUsActivity.class));

            Intent i4 = new Intent(this, AboutUsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i4.putExtras(bundle);
            startActivity(i4);


        } else if (id == R.id.nav_menu_settings) {

        } else if (id == R.id.nav_menu_logout) {

            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to Logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(new Intent(AllVideosSlidesNotesActivity.this, MainActivity.class));
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }


    private void getAllVideoData() {
        new GetVideoListDataFromServer(AllVideosSlidesNotesActivity.this).execute();
    }

    public class GetVideoListDataFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetVideoListDataFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_bar_login = new ProgressDialog(AllVideosSlidesNotesActivity.this);
            progress_bar_login.setMessage("Please wait...");
            progress_bar_login.setCancelable(false);
            progress_bar_login.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            HttpHandler httpHandler = new HttpHandler();
            String responseFromDB = httpHandler.makeServiceCall(HttpGetURL);
            String userResponseFromDB = httpHandler.makeServiceCallUserData(HttpGetURL1);
            if (responseFromDB != null) {
                vedioDataModules = BaseActivity.getVideoDetailsList(responseFromDB);
            }
            if (userResponseFromDB != null) {
                vedioUserDatas = BaseActivity.getVideoUserDetailsList(userResponseFromDB);
                vedioDataModules = updateUserDataInModule(vedioDataModules, vedioUserDatas);
            }
            return null;
        }

        private ArrayList<VedioDataModule> updateUserDataInModule(ArrayList<VedioDataModule> vedioDataModules, ArrayList<VedioUserData> vedioUserData) {
            ArrayList<VedioDataModule> data = vedioDataModules;
            ArrayList<VedioUserData> userData = vedioUserData;

            for (int i = 0; i < data.size(); i++) {
                int mid = data.get(i).getModuleId();
                int completedCount = 0;
                for (int j = 0; j < userData.size(); j++) {
                    int umid = userData.get(j).getVedioModuleId();
                    int staus = userData.get(j).getStatus();

                    if (mid == umid && staus == 1) {
                        completedCount += 1;
                    }
                }
                data.get(i).setCompletedVedio("" + completedCount);
            }
            return data;
        }


        protected void onPostExecute(Void result) {
            progress_bar_login.dismiss();
            recyclerView.setHasFixedSize(true);

            layoutManager = new GridLayoutManager(AllVideosSlidesNotesActivity.this, 2);
            recyclerView.setLayoutManager(layoutManager);

            adapter = new VedioRecyclerAdapter(vedioDataModules);
            recyclerView.setAdapter(adapter);

        }
    }


    public void bottomViewNavigation() {
        bottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.ic_account_balance_black_24dp));
        bottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.ic_baseline_library_add_check_24));
        bottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.ic_assignment_black_24dp));
        bottomNavigation.add(new MeowBottomNavigation.Model(4, R.drawable.ic_live_tv_black_24dp));

        bottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                /* Toast.makeText(NavigationDrawerActivity.this, "clicked item : " + item.getId(), Toast.LENGTH_SHORT).show();*/
                if (item.getId() == 1) {
                    //SlideMenu fragment = new SlideMenu();
                    /*startActivity(new Intent(getActivity(), SlideMenu.class));*/
                    overridePendingTransition(0, 0);
                    Intent i1 = new Intent(AllVideosSlidesNotesActivity.this, DashBoardActivity.class);
                    i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i1);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 2) {
                    //startActivity(new Intent(getActivity(), MCQTest.class));
                    overridePendingTransition(0, 0);
                    Intent i2 = new Intent(AllVideosSlidesNotesActivity.this, MCQTest.class);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i2);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 3) {
                    //startActivity(new Intent(getActivity(), GrandTestLandingActivity.class));
                    overridePendingTransition(0, 0);
                    Intent i3 = new Intent(AllVideosSlidesNotesActivity.this, GrandTestLandingActivity.class);
                    i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i3);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 4) {
                    overridePendingTransition(0, 0);
                    Intent i4 = new Intent(AllVideosSlidesNotesActivity.this, AllVideosSlidesNotesActivity.class);
                    i4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i4);
                    overridePendingTransition(0, 0);
                }
            }
        });

        bottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {

                String name;
                switch (item.getId()) {
                    case 1:
                        name = "HOME";
                        break;
                    case 2:
                        name = "EXPLORE";
                        break;
                    case 3:
                        name = "MESSAGE";
                        break;
                    case 4:
                        name = "VIDEOS";
                        break;
                    default:
                        name = "";
                }
            }
        });

        bottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {
                /*  Toast.makeText(NavigationDrawerActivity.this, "reselected item : " + item.getId(), Toast.LENGTH_SHORT).show();*/
            }
        });


        //bottomNavigation.setCount(4, "118");
        bottomNavigation.show(4, true);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void shareApp() {

        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Ardent BDS Application");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + "com.ardent_bds" + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}

