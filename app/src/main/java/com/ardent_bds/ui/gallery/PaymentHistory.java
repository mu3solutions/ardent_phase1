package com.ardent_bds.ui.gallery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.ui.LiveTest.GrandTestLandingActivity;
import com.ardent_bds.ui.mcq_test_module.MCQTest;

public class PaymentHistory extends Activity {


    ImageView iv_back_payment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.payment_details);

        iv_back_payment = findViewById(R.id.iv_back_payment);

        iv_back_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = getIntent().getExtras();

//Extract the data…
                String screen_name = bundle.getString("screen_name");

                Log.i("screen_name payment screen", "" + screen_name);

                if (screen_name.equalsIgnoreCase("SlideMenu")) {
                   finish();
                    overridePendingTransition(0,0);
                    startActivity(new Intent(PaymentHistory.this, DashBoardActivity.class));
                } else if (screen_name.equalsIgnoreCase("MCQTest")) {
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(new Intent(PaymentHistory.this, MCQTest.class));
                } else if (screen_name.equalsIgnoreCase("GrandTestLandingActivity")) {
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(new Intent(PaymentHistory.this, GrandTestLandingActivity.class));
                } else if (screen_name.equalsIgnoreCase("AllVideosSlidesNotesActivity")) {
                    finish();
                    overridePendingTransition(0,0);
                    startActivity(new Intent(PaymentHistory.this, AllVideosSlidesNotesActivity.class));
                }
            }
        });
    }
}
