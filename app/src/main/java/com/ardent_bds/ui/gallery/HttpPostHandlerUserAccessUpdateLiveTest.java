package com.ardent_bds.ui.gallery;

import android.util.Log;

import com.ardent_bds.model.UserModel;
import com.ardent_bds.project_utils.HttpPostHandlerGrandTestData;
import com.ardent_bds.ui.UserAccessLevelModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;

public class HttpPostHandlerUserAccessUpdateLiveTest {

    private static final String TAG = HttpPostHandlerGrandTestData.class.getSimpleName();
    private String responseStr;

    private int allPermissionsList = 0;

    public HttpPostHandlerUserAccessUpdateLiveTest() {
    }

    public String makeAccessUpdateServicePostCall(String reqPostUrl) {

        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);

            UserAccessLevelModel userAccessLevelModel = UserAccessLevelModel.getInstance();

            allPermissionsList = Integer.parseInt(userAccessLevelModel.getUpdatedUserAccessLevel());


            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            if (allPermissionsList == 0) {
                nameValuePairs.add(new BasicNameValuePair("user_access_level", "3"));
            } else {
                nameValuePairs.add(new BasicNameValuePair("user_access_level", allPermissionsList + "3"));
            }
            nameValuePairs.add(new BasicNameValuePair("user_account_id", UserModel.getInstance().getUserId()));

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "All submodule http Response " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }

    public String makeAccessUpdateServicePostCallVideo(String reqPostUrl) {

        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);


            UserAccessLevelModel userAccessLevelModel = UserAccessLevelModel.getInstance();

            allPermissionsList = Integer.parseInt(userAccessLevelModel.getUpdatedUserAccessLevel());


            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            if (allPermissionsList == 0) {
                nameValuePairs.add(new BasicNameValuePair("user_access_level", "2"));
            } else {
                nameValuePairs.add(new BasicNameValuePair("user_access_level", allPermissionsList + "2"));
            }
            nameValuePairs.add(new BasicNameValuePair("user_account_id", UserModel.getInstance().getUserId()));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "All submodule http Response " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }
    public String makeAccessUpdateServicePostCallMCQ(String reqPostUrl) {

        try {
            Log.v(TAG, "postURL: " + reqPostUrl);
            // HttpClient
            HttpClient httpClient = new DefaultHttpClient();
            // post header
            HttpPost httpPost = new HttpPost(reqPostUrl);


            UserAccessLevelModel userAccessLevelModel = UserAccessLevelModel.getInstance();

            allPermissionsList = Integer.parseInt(userAccessLevelModel.getUpdatedUserAccessLevel());


            //NameValuePair Data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            if (allPermissionsList == 0) {
                nameValuePairs.add(new BasicNameValuePair("user_access_level", "1"));
            } else {
                nameValuePairs.add(new BasicNameValuePair("user_access_level", allPermissionsList + "1"));
            }
            nameValuePairs.add(new BasicNameValuePair("user_account_id", UserModel.getInstance().getUserId()));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
// execute HTTP post request
            HttpResponse responses = httpClient.execute(httpPost);
            HttpEntity resEntity = responses.getEntity();
            if (resEntity != null) {
                responseStr = EntityUtils.toString(resEntity).trim();
                Log.v(TAG, "All submodule http Response " + responseStr);
                // you can add an if statement here and do other actions based on the response
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return responseStr;
    }
}
