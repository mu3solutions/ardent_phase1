package com.ardent_bds.ui.mcq_of_day;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.project_utils.HttpHandler;
import com.ardent_bds.ui.BaseActivity;

import java.util.ArrayList;

public class Mcq extends Activity {

    private static QuestionCustomAdapter questionCustomAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView mcq_recycler_view;
    private static ArrayList<QuestionDataModel> data;
    static View.OnClickListener myOnClickListener;
    private static ArrayList<Integer> removedItems;

    ArrayList<QuestionDataModel> questionsDataModules;

    String HttpGetUrl = Constants.BaseUrl + "/ArdentDB/getAllQuestions.php";


    ImageView screen_close;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_question_main);


        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {

            getAllMCQQuestions();
        } else {
            Toast.makeText(Mcq.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


        mcq_recycler_view = (RecyclerView) findViewById(R.id.mcq_recycler_view);
        screen_close = findViewById(R.id.screen_close);
        screen_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(Mcq.this, DashBoardActivity.class));
            }
        });
        /*mcq_recycler_view.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        mcq_recycler_view.setLayoutManager(layoutManager);
        mcq_recycler_view.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<QuestionDataModel>();

        adapter = new QuestionCustomAdapter();
        mcq_recycler_view.setAdapter(adapter);*/

    }

    private void getAllMCQQuestions() {

        new GetMCQQuestionsFromServer(getApplicationContext()).execute();
    }


    public class GetMCQQuestionsFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetMCQQuestionsFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {


            HttpHandler httpPostHandler = new HttpHandler();
            String responseFromDB = httpPostHandler.makeServiceCall(HttpGetUrl);
            if (responseFromDB != null) {
                Log.e("Questions data ", "Question responseFromDB: " + responseFromDB);
                questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {


            if (questionsDataModules.size() > 0) {

                mcq_recycler_view.setHasFixedSize(true);

                layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                mcq_recycler_view.setLayoutManager(layoutManager);

                questionCustomAdapter = new QuestionCustomAdapter(questionsDataModules);
                mcq_recycler_view.setAdapter(questionCustomAdapter);
            } else {
                overridePendingTransition(0, 0);
                onBackPressed();
            }
        }
    }


}
