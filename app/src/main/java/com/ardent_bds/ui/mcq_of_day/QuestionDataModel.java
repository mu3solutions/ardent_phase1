package com.ardent_bds.ui.mcq_of_day;

import java.util.ArrayList;

public class QuestionDataModel {
    private int questionId;
    private int childId;
    private String mcqQuestionDesc;

    private String mcqAnswerList;
    private String mcqAnswerDesc;
    private String mcqSelectedAnswerList;

    public String getMcqAnswerImage() {
        return mcqAnswerImage;
    }

    public void setMcqAnswerImage(String mcqAnswerImage) {
        this.mcqAnswerImage = mcqAnswerImage;
    }

    private String mcqAnswerImage;

    public int getMcqCorrectAnswer() {
        return mcqCorrectAnswer;
    }

    public void setMcqCorrectAnswer(int mcqCorrectAnswer) {
        this.mcqCorrectAnswer = mcqCorrectAnswer;
    }

    private int mcqCorrectAnswer;

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public String getMcqQuestionDesc() {
        return mcqQuestionDesc;
    }

    public void setMcqQuestionDesc(String mcqQuestionDesc) {
        this.mcqQuestionDesc = mcqQuestionDesc;
    }

    public String getMcqAnswerList() {
        return mcqAnswerList;
    }

    public void setMcqAnswerList(String mcqAnswerList) {
        this.mcqAnswerList = mcqAnswerList;
    }

    public String getMcqAnswerDesc() {
        return mcqAnswerDesc;
    }

    public void setMcqAnswerDesc(String mcqAnswerDesc) {
        this.mcqAnswerDesc = mcqAnswerDesc;
    }

    public String getMcqSelectedAnswerList() {
        return mcqSelectedAnswerList;
    }

    public void setMcqSelectedAnswerList(String mcqSelectedAnswerList) {
        this.mcqSelectedAnswerList = mcqSelectedAnswerList;
    }

    private static QuestionDataModel instance = null;
    private static ArrayList<QuestionDataModel> arrayInstance = null;

    public static QuestionDataModel getInstance() {
        if (instance == null) {
            instance = new QuestionDataModel();
        }
        return instance;
    }

    public static ArrayList<QuestionDataModel> getArrayInstance() {
        if (arrayInstance == null) {
            arrayInstance = new ArrayList<QuestionDataModel>();
        }
        return arrayInstance;
    }


}
