package com.ardent_bds.ui.mcq_of_day;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.model.VedioDataModule;

import java.util.ArrayList;

public class QuestionCustomAdapter extends RecyclerView.Adapter<QuestionCustomAdapter.MyViewHolder> {
    ArrayList<QuestionDataModel> vsm;
    ArrayList<VedioDataModule> vedioDataModules;

    String questionListString;
    Context context;

    int correctAnswerPosition;

    private int selectedPos = RecyclerView.NO_POSITION;

    public QuestionCustomAdapter(ArrayList<QuestionDataModel> vsm) {
        this.vsm = vsm;
    }

    public QuestionCustomAdapter(String mcqQuestionDesc) {

        this.questionListString = mcqQuestionDesc;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView option_id, answer_txt1, answer_txt2, answer_txt3, answer_txt4, question_txt, txt_description_answer, proceed_tv;
        ImageView imv_tick_cross1, imv_tick_cross2, imv_tick_cross3, imv_tick_cross4, imv_marked_list;
        LinearLayout ll1, ll2, ll3, ll4, ll_answer_desc;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            //option_id = itemView.findViewById(R.id.option_id);
            question_txt = itemView.findViewById(R.id.question_txt);
            ll_answer_desc = itemView.findViewById(R.id.ll_answer_desc);
            answer_txt1 = itemView.findViewById(R.id.answer1);
            answer_txt2 = itemView.findViewById(R.id.answer2);
            answer_txt3 = itemView.findViewById(R.id.answer3);
            answer_txt4 = itemView.findViewById(R.id.answer4);
            txt_description_answer = itemView.findViewById(R.id.txt_description_answer);
            proceed_tv = itemView.findViewById(R.id.proceed_tv);


            ll1 = itemView.findViewById(R.id.ll1);
            ll2 = itemView.findViewById(R.id.ll2);
            ll3 = itemView.findViewById(R.id.ll3);
            ll4 = itemView.findViewById(R.id.ll4);


            imv_tick_cross1 = itemView.findViewById(R.id.imv_tick_cross1);
            imv_tick_cross2 = itemView.findViewById(R.id.imv_tick_cross2);
            imv_tick_cross3 = itemView.findViewById(R.id.imv_tick_cross3);
            imv_tick_cross4 = itemView.findViewById(R.id.imv_tick_cross4);
            imv_marked_list = itemView.findViewById(R.id.imv_marked_list);

            imv_marked_list.setVisibility(View.GONE);
        }
    }

    @NonNull
    @Override
    public QuestionCustomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mcq_of_day_cards_layout, parent, false);
        context = view.getContext();
        return new QuestionCustomAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final QuestionCustomAdapter.MyViewHolder holder, final int position) {


        // String[] abc = vsm.get(position).getMcqQuestionDesc().split("\\)");

        //holder.option_id.setText(vsm.get(position).getMcqQuestionDesc().split("\\)")[0]);


        String ques = vsm.get(position).getMcqQuestionDesc();


        String[] queStringArray = ques.split(",");

        holder.question_txt.setText("" + queStringArray[0]);
        holder.answer_txt1.setText("" + vsm.get(position).getMcqAnswerList().split(",")[0].split(" ")[1]);
        holder.answer_txt2.setText("" + vsm.get(position).getMcqAnswerList().split(",")[1].split(" ")[1]);
        holder.answer_txt3.setText("" + vsm.get(position).getMcqAnswerList().split(",")[2].split(" ")[1]);
        holder.answer_txt4.setText("" + vsm.get(position).getMcqAnswerList().split(",")[3].split(" ")[1]);


        holder.txt_description_answer.setText("" + vsm.get(position).getMcqAnswerDesc());

        correctAnswerPosition = vsm.get(position).getMcqCorrectAnswer();


        holder.ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = 0;

                holder.ll_answer_desc.setVisibility(View.VISIBLE);
                holder.proceed_tv.setVisibility(View.VISIBLE);

                if (selectedPos == correctAnswerPosition) {
                    if (correctAnswerPosition == 0) {
                        holder.ll1.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 1) {

                        holder.ll2.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 2) {

                        holder.ll3.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 3) {
                        holder.ll4.setBackgroundColor(Color.GREEN);
                    }
                } else {
                    if (correctAnswerPosition == 0) {
                        holder.ll1.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 1) {
                        holder.ll2.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 2) {
                        holder.ll3.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 3) {
                        holder.ll4.setBackgroundColor(Color.GREEN);
                    }

                    if (selectedPos == 0) {
                        holder.ll1.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 1) {
                        holder.ll2.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 2) {
                        holder.ll3.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 3) {
                        holder.ll4.setBackgroundColor(Color.RED);
                    }
                }
            }
        });
        holder.ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = 1;
                holder.ll_answer_desc.setVisibility(View.VISIBLE);
                holder.proceed_tv.setVisibility(View.VISIBLE);
                if (selectedPos == correctAnswerPosition) {
                    if (correctAnswerPosition == 0) {
                        holder.ll1.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 1) {
                        holder.ll2.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 2) {
                        holder.ll3.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 3) {
                        holder.ll4.setBackgroundColor(Color.GREEN);
                    }
                } else {
                    if (correctAnswerPosition == 0) {
                        holder.ll1.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 1) {
                        holder.ll2.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 2) {
                        holder.ll3.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 3) {
                        holder.ll4.setBackgroundColor(Color.GREEN);
                    }
                    if (selectedPos == 0) {
                        holder.ll1.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 1) {
                        holder.ll2.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 2) {
                        holder.ll3.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 3) {
                        holder.ll4.setBackgroundColor(Color.RED);
                    }
                }
            }
        });
        holder.ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = 2;
                holder.ll_answer_desc.setVisibility(View.VISIBLE);
                holder.proceed_tv.setVisibility(View.VISIBLE);
                if (selectedPos == correctAnswerPosition) {
                    if (correctAnswerPosition == 0) {
                        holder.ll1.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 1) {
                        holder.ll2.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 2) {
                        holder.ll3.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 3) {
                        holder.ll4.setBackgroundColor(Color.GREEN);
                    }
                } else {
                    if (correctAnswerPosition == 0) {
                        holder.ll1.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 1) {
                        holder.ll2.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 2) {
                        holder.ll3.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 3) {
                        holder.ll4.setBackgroundColor(Color.GREEN);
                    }

                    if (selectedPos == 0) {
                        holder.ll1.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 1) {
                        holder.ll2.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 2) {
                        holder.ll3.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 3) {
                        holder.ll4.setBackgroundColor(Color.RED);
                    }
                }
            }
        });
        holder.ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = 3;
                holder.ll_answer_desc.setVisibility(View.VISIBLE);
                holder.proceed_tv.setVisibility(View.VISIBLE);
                if (selectedPos == correctAnswerPosition) {
                    if (correctAnswerPosition == 0) {
                        holder.ll1.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 1) {
                        holder.ll2.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 2) {
                        holder.ll3.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 3) {
                        holder.ll4.setBackgroundColor(Color.GREEN);
                    }
                } else {
                    if (correctAnswerPosition == 0) {
                        holder.ll1.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 1) {
                        holder.ll2.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 2) {
                        holder.ll3.setBackgroundColor(Color.GREEN);
                    } else if (correctAnswerPosition == 3) {
                        holder.ll4.setBackgroundColor(Color.GREEN);
                    }
                    if (selectedPos == 0) {
                        holder.ll1.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 1) {
                        holder.ll2.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 2) {
                        holder.ll3.setBackgroundColor(Color.RED);
                    } else if (selectedPos == 3) {
                        holder.ll4.setBackgroundColor(Color.RED);
                    }
                }
            }
        });

        holder.proceed_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.txt_description_answer.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public int getItemCount() {
        return 1;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
