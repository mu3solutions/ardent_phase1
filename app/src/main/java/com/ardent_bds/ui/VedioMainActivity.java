package com.ardent_bds.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.adapters.VedioSubRecyclerAdapter;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.ListItem;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.model.VedioContentItem;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.model.VedioSmHeader;
import com.ardent_bds.model.VedioSubDataModule;
import com.ardent_bds.model.VedioUserData;
import com.ardent_bds.project_utils.HttpHandler;
import com.ardent_bds.project_utils.HttpPostHandlerSubModuleData;
import com.ardent_bds.ui.LiveTest.HttpPostUpdatedUserDetailsHandler;
import com.ardent_bds.ui.gallery.AllVideosSlidesNotesActivity;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.ardent_bds.ui.BaseActivity.getUserData;


public class VedioMainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    JSONObject jsonObject = null;
    VedioDataModule videoDataModule;
    ArrayList<VedioSubDataModule> vedioDataModules;
    ArrayList<VedioUserData> vedioUserDatas;
    VedioSubDataModule dataModule;
    VedioSubDataModule vedioSubDataModule;
    ArrayList<UserAccessLevelModel> userAccessList;
    TextView txt_total_completed;
    private ProgressDialog progress_bar_grandTest;
    SeekBar completedSeekBar;
    String HttpPostURL = Constants.BaseUrl + "/ArdentDB/vedioSubModuleData.php";
    String HttpGetURL1 = Constants.BaseUrl + "/ArdentDB/vedioUserData.php";
    String HttpGetURL_Updated_User_details = Constants.BaseUrl + "/ArdentDB/getUpdatedUserDetails.php";
    TextView toolText, module_name;
    ImageView toolImage, menu_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_vedio_main);

        toolImage = findViewById(R.id.tool_image);
        toolText = findViewById(R.id.tool_text);
        module_name = findViewById(R.id.module_name);
        menu_back = findViewById(R.id.menu_back);
        initCollapsingToolbar();
        userAccessList = new ArrayList<>();
        vedioDataModules = new ArrayList<>();
        // insertVedioModuleData();
        vedioSubDataModule = VedioSubDataModule.getInstance();
        completedSeekBar = findViewById(R.id.completedSeekBar);
        txt_total_completed = findViewById(R.id.txt_total_completed);


        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getUpdatedUserData();
            getAllVideoData();

        } else {

            Toast.makeText(VedioMainActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


        completedSeekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });


        menu_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                onBackPressed();
                finish();
                startActivity(new Intent(VedioMainActivity.this, AllVideosSlidesNotesActivity.class));
            }
        });


        completedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                completedSeekBar.setProgress(progress++);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    private void getUpdatedUserData() {

        new GetAllUserUpdateData(this).execute();
    }


    public class GetAllUserUpdateData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllUserUpdateData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_bar_grandTest = new ProgressDialog(VedioMainActivity.this);
            progress_bar_grandTest.setMessage("Please wait...");
            progress_bar_grandTest.setCancelable(false);
            progress_bar_grandTest.setIndeterminate(true);
            progress_bar_grandTest.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostUpdatedUserDetailsHandler httpPostHandler = new HttpPostUpdatedUserDetailsHandler();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Updated_User_details);
            if (responseFromDB != null) {
                Log.e("Grant Test>>", "Grant Test data responseFromDB: " + responseFromDB);
                userAccessList = getUserData(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {

            UserAccessLevelModel.getInstance().setUpdatedUserAccessLevel(userAccessList.get(0).getUpdatedUserAccessLevel());
            UserModel.getInstance().setUserAccessLevel(Integer.parseInt(userAccessList.get(0).getUpdatedUserAccessLevel()));


        }
    }


    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingToolbar.setTitle(" nn");
        //collapsingToolbar.setPointerIcon(R.drawable.ic_baseline_arrow_back_24);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();

                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Bala");
                    toolImage.setVisibility(View.GONE);
                    toolText.setVisibility(View.VISIBLE);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" Bla");
                    toolImage.setVisibility(View.VISIBLE);
                    toolText.setVisibility(View.GONE);
                    isShow = false;
                }
            }
        });
    }

    private void getAllVideoData() {
        new GetSubModuleDataDataFromServer(getApplicationContext()).execute();

    }


    public class GetSubModuleDataDataFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetSubModuleDataDataFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler httpHandler = new HttpHandler();
            String userResponseFromDB = httpHandler.makeServiceCallUserData(HttpGetURL1);
            HttpPostHandlerSubModuleData httpPostHandler = new HttpPostHandlerSubModuleData();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpPostURL);
            if (userResponseFromDB != null) {
                vedioUserDatas = BaseActivity.getVideoUserDetailsList(userResponseFromDB);
                //vedioDataModules = updateUserDataInModule(vedioDataModules,vedioUserDatas);
            }
            if (responseFromDB != null) {
                Log.e("Sub module data ", "Submodule data responseFromDB: " + responseFromDB);
                ArrayList<VedioUserData> formList = VedioUserData.getInstance();
                vedioDataModules = BaseActivity.getSubModuleDetails(responseFromDB);
                vedioDataModules = updateUserDataInSubModule(vedioDataModules, formList);
            }
            return null;
        }

        private ArrayList<VedioSubDataModule> updateUserDataInSubModule(ArrayList<VedioSubDataModule> vedioDataModules, ArrayList<VedioUserData> vedioUserData) {
            ArrayList<VedioSubDataModule> data = vedioDataModules;
            ArrayList<VedioUserData> userData = vedioUserData;
            if (userData.size() > 0) {
                for (int i = 0; i < data.size(); i++) {
                    int smChid = data.get(i).getChildID();
                    int completedCount = 0;
                    for (int j = 0; j < userData.size(); j++) {
                        int umid = userData.get(j).getVedioChildId();
                        int staus = userData.get(j).getStatus();
                        if (smChid == umid && staus == 1) {
                            completedCount += 1;
                            data.get(i).setStatus(1);
                        }
                    }
                }
            }
            return data;
        }


        private ArrayList<ListItem> getList(ArrayList<VedioSubDataModule> vedioSubModuleDatas) {
            ArrayList<ListItem> arrayList = new ArrayList<>();
            ArrayList<VedioSubDataModule> vedioSMDatas = vedioSubModuleDatas;
            int headId = 0;
            String headName = "";
            for (int i = 0; i < vedioSMDatas.size(); i++) {
                VedioSmHeader header = new VedioSmHeader();
                VedioContentItem contentItem = new VedioContentItem();

                if (headId == 0) {
                    headId = vedioSMDatas.get(i).getSubModuleVid();
                    headName = vedioSMDatas.get(i).getSubModuleName();

                    header.setId(0);
                    header.setName(headName);
                    arrayList.add(header);

                    contentItem.setSmChildId(vedioSMDatas.get(i).getChildID());
                    contentItem.setSmChildName(vedioSMDatas.get(i).getChildName());
                    contentItem.setSmChildDesc(vedioSMDatas.get(i).getChildDesc());
                    contentItem.setSmChildimage(vedioSMDatas.get(i).getChildImage());
                    contentItem.setSmChildRating(vedioSMDatas.get(i).getRating());
                    contentItem.setStatus(vedioSMDatas.get(i).getStatus());
                    contentItem.setAccessLevel(vedioSMDatas.get(i).getAccesslevel());
                    arrayList.add(contentItem);
                } else {
                    if (headId == vedioSMDatas.get(i).getSubModuleVid()) {
                        contentItem.setSmChildId(vedioSMDatas.get(i).getChildID());
                        contentItem.setSmChildName(vedioSMDatas.get(i).getChildName());
                        contentItem.setSmChildDesc(vedioSMDatas.get(i).getChildDesc());
                        contentItem.setSmChildimage(vedioSMDatas.get(i).getChildImage());
                        contentItem.setSmChildRating(vedioSMDatas.get(i).getRating());
                        contentItem.setStatus(vedioSMDatas.get(i).getStatus());
                        contentItem.setAccessLevel(vedioSMDatas.get(i).getAccesslevel());
                        arrayList.add(contentItem);
                    } else {
                        headId = vedioSMDatas.get(i).getSubModuleVid();
                        headName = vedioSMDatas.get(i).getSubModuleName();

                        header.setId(0);
                        header.setName(headName);
                        arrayList.add(header);

                        contentItem.setSmChildId(vedioSMDatas.get(i).getChildID());
                        contentItem.setSmChildName(vedioSMDatas.get(i).getChildName());
                        contentItem.setSmChildDesc(vedioSMDatas.get(i).getChildDesc());
                        contentItem.setSmChildimage(vedioSMDatas.get(i).getChildImage());
                        contentItem.setSmChildRating(vedioSMDatas.get(i).getRating());
                        contentItem.setStatus(vedioSMDatas.get(i).getStatus());
                        contentItem.setAccessLevel(vedioSMDatas.get(i).getAccesslevel());
                        contentItem.setQnCount(vedioSMDatas.get(i).getTotalCount());
                        arrayList.add(contentItem);
                    }
                }
            }
            return arrayList;
        }

        protected void onPostExecute(Void result) {
            progress_bar_grandTest.dismiss();
            vedioDataModules.size();
            if (vedioDataModules.size() > 0) {
                String completed = VedioDataModule.getInstance().getCompletedVedio();
                module_name.setText("" + VedioDataModule.getInstance().getModuleName());
                txt_total_completed.setText(completed + " of " + vedioDataModules.get(0).getTotalCount());
                completedSeekBar.setProgress(Integer.parseInt(completed));
                completedSeekBar.setMax(vedioDataModules.get(0).getTotalCount());
                recyclerView = findViewById(R.id.recycler_view_video);
                recyclerView.setHasFixedSize(true);

                layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);

                vedioSubDataModule.setArrayListAA(vedioDataModules);
                adapter = new VedioSubRecyclerAdapter(VedioMainActivity.this, UserAccessLevelModel.getInstance().getUpdatedUserAccessLevel());
                recyclerView.setAdapter(adapter);
            } else {
                overridePendingTransition(0, 0);
                onBackPressed();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(VedioMainActivity.this, AllVideosSlidesNotesActivity.class));
    }
}