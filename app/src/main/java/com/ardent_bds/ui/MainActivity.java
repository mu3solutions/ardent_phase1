package com.ardent_bds.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.project_utils.HttpPostHandler;
import com.ardent_bds.ui.registeration.RegistrationActivity;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ds";
    CallbackManager callbackManager;
    ImageView fb;
    private static final int PERMISSION_REQUEST_CODE = 100;

    GoogleSignInClient mGoogleSignInClient;
    // ImageView signInButton;
    String id;
    URL profile_pic;
    UserModel userModel;
    ImageView signInButton;
    private ProgressDialog progress_bar_login;

    JSONObject jsonObject = null;
    String HttpPostURL = Constants.BaseUrl + "/ArdentDB/checkuserexist.php";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        progress_bar_login = new ProgressDialog(MainActivity.this);
        progress_bar_login.setMessage("Please wait...");
        progress_bar_login.setCancelable(false);
        progress_bar_login.setIndeterminate(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAllPermission();
        }
        userModel = UserModel.getInstance();
        fb = (ImageView) findViewById(R.id.sign_in_facebook);

        //google start here
        // Set the dimensions of the sign-in button.
        //   signInButton = findViewById(R.id.google);
        // LinearLayout signInButton;
        //findViewById(R.id.google).setOnClickListener(this);
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
      /*  GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("1047804665874-if9b40tuptkbmf9f1fj85kg0nm47knps.apps.googleusercontent.com")
                .requestProfile()
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        //Goggle end here*/


        signInButton = findViewById(R.id.sign_in_gmail);
        //signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);


        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        String accessToken = loginResult.getAccessToken()
                                .getToken();

                        String userID = loginResult.getAccessToken().getUserId();
                        Log.i("accessToken", accessToken);
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object,
                                                            GraphResponse response) {
                                        Log.i("LoginActivity",
                                                response.toString());
                                        try {
                                            id = object.getString("id");
                                            try {
                                                profile_pic = new URL(
                                                        "https://graph.facebook.com/" + id + "/picture?type=large");
                                                Log.i("profile_pic",
                                                        profile_pic + "");

                                            } catch (MalformedURLException e) {
                                                e.printStackTrace();
                                            }
                                            Log.e("UserDate", String.valueOf(object));

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            userModel.setUserName(object.getString("name"));
                                            userModel.setUserId(object.getString("id"));
                                            userModel.setUserEmailId(object.getString("email"));
                                            userModel.setUserProfilePic(String.valueOf(Uri.parse(String.valueOf(profile_pic))));
                                            userModel.setUserLoggedInChannel("Facebook");
                                            //userModel.setUserStatus("Online");
                                            userModel.setUserOnlineStatus(true);
                                            userModel.setUserAccessLevel(0);
                                            userModel.setUserlastLoginTime(String.valueOf(Calendar.getInstance().getTime()));

                                            ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
                                            if (networkInfo != null && networkInfo.isConnected() == true) {
                                                new GetDataFromServer(MainActivity.this).execute();
                                            } else {

                                                Toast.makeText(MainActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                                        intent.putExtra("userData", object.toString());
                                        intent.putExtra("profileLink", profile_pic.toString());
                                        startActivity(intent);
                                        finish();

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }


    public void onClick(View v) {
        if (v == fb) {

            ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected() == true) {
                LoginManager.getInstance().logInWithReadPermissions(
                        this,
                        Arrays.asList("user_photos", "email", "user_birthday", "public_profile"));
                progress_bar_login.show();

            } else {
                Toast.makeText(MainActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();

            }


        } else if (v == signInButton) {
            ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected() == true) {
                progress_bar_login.show();
                signIn();
            } else {
                Toast.makeText(MainActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();

            }
        }

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 1);
    }

   /* private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 1);

    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == 1) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.

            Log.i("Success code", "" + account.getDisplayName());
            //updateUI(account);
            userModel.setUserName(account.getGivenName());
            userModel.setUserEmailId(account.getEmail());
            userModel.setUserId(account.getId());
            userModel.setUserProfilePic(String.valueOf(account.getPhotoUrl()));
            userModel.setUserLoggedInChannel("Gmail");
            userModel.setUserAccessLevel(0);
            userModel.setUserlastLoginTime(String.valueOf(Calendar.getInstance().getTime()));

            ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected() == true) {
                new GetDataFromServer(MainActivity.this).execute();
            } else {

                Toast.makeText(MainActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
            }


        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
// the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    private void updateUI(GoogleSignInAccount acct) {
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personGivenName = acct.getGivenName();
            String personFamilyName = acct.getFamilyName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();


          /*  UserModel userModel = UserModel.getInstance();
            userModel.setUserName(acct.getDisplayName());
            userModel.setUserEmailId(acct.getEmail());
            userModel.setUserId(acct.getId());
            userModel.setUserProfilePic(String.valueOf(acct.getPhotoUrl()));
*/


        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public class GetDataFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetDataFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            HttpPostHandler httpPostHandler = new HttpPostHandler();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpPostURL);
            if (responseFromDB != null) {
                Log.e(TAG, "responseFromDB: " + responseFromDB);
                try {
                    JSONArray jsonArray = new JSONArray(responseFromDB);
                    jsonObject = jsonArray.getJSONObject(0);
                    Log.i("JSON Data from Server", "" + jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            //progress_bar_login.dismiss();
            //userModel.setUserOnlineStatus(true);
            //userModel.setUserStatus("Online");
            finish();
            overridePendingTransition(0, 0);


            Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .getBoolean("isFirstRun", true);

            if (isFirstRun) {
                startActivity(new Intent(MainActivity.this, RegistrationActivity.class));
            } else {
                UserAccessLevelModel.getInstance().setUpdatedUserAccessLevel("" + userModel.getUserAccessLevel());
                startActivity(new Intent(MainActivity.this, DashBoardActivity.class));
            }
            getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                    .putBoolean("isFirstRun", false).commit();

        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkAllPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(MainActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Snackbar.make(MainActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload profile photo",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        PERMISSION_REQUEST_CODE);
                            }
                        }).show();
            } else {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_CODE);
            }
        } else {
            // write your logic code if permission already granted
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean writePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (writePermission && readExternalFile) {
                        // write your logic here
                    } else {
                        Snackbar.make(MainActivity.this.findViewById(android.R.id.content),
                                "Please Grant Permissions to upload profile photo",
                                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                                new View.OnClickListener() {
                                    @RequiresApi(api = Build.VERSION_CODES.M)
                                    @Override
                                    public void onClick(View v) {
                                        requestPermissions(
                                                new String[]{Manifest.permission
                                                        .READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                PERMISSION_REQUEST_CODE);
                                    }
                                }).show();
                    }
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
