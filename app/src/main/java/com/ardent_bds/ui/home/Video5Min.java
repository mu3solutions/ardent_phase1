package com.ardent_bds.ui.home;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.text.HtmlCompat;

import com.ardent_bds.R;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class Video5Min extends AppCompatActivity {

    JCVideoPlayerStandard jcVideoPlayerStandard;
    TextView video_description, tv_header_videos;

    String videoURL;
    String videoName;
    String videoDesc;
    String videoURL1;
    private String lastTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);


        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.video_suggested_videos);

        jcVideoPlayerStandard = (JCVideoPlayerStandard) findViewById(R.id.videoplayer);
        video_description = (TextView) findViewById(R.id.video_description);
        tv_header_videos = findViewById(R.id.tv_header_videos);

        video_description.setMovementMethod(new ScrollingMovementMethod());


        // video_description.setText(Html.fromHtml(getResources().getString(R.string.content_video)), TextView.BufferType.SPANNABLE);


        String htmlText = "<h2>What is Anatomy?</h2>\n" + "<p>About Anatomy In humans and other mammals and in birds, the heart is a four-chambered double pump that is the centre of the circulatory system.In humans it is situated between the two lungs and slightly to the left of centre, behind the breastbone; it rests on the diaphragm, the muscular partition between the chest and the abdominal cavity.\\\"In humans and other mammals and in birds, the heart is a four-chambered double pump that is the centre of the circulatory system. In humans it is situated between the two lungs and slightly to the left of centre, behind the breastbone; it rests on the diaphragm, the muscular partition between the chest and the abdominal cavity.</p>";

        video_description.setText(HtmlCompat.fromHtml(htmlText, 0));


        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            videoURL = null;
        } else {
            videoURL = extras.getString("suggested_all_video");
            videoName = extras.getString("suggested_all_video_txt");
            videoDesc = extras.getString("suggested_all_video_txt_des");
            tv_header_videos.setText("" + videoName);

            video_description.setText("" + videoDesc);

          /*  jcVideoPlayerStandard.setUp(videoURL, JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, "");
            jcVideoPlayerStandard.setAllControlsVisible(0, 0, 0, 0, 0, 0, 0);
            jcVideoPlayerStandard.startVideo();*/

            //Latest code Custom Video Player


            jcVideoPlayerStandard.setUp(videoURL, JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, "");
            jcVideoPlayerStandard.setAllControlsVisible(1, 1, 1, 1, R.drawable.dummy_user, R.drawable.dummy_user, 1);


            jcVideoPlayerStandard.loadingProgressBar.setVisibility(View.GONE);
            jcVideoPlayerStandard.battery_level.setVisibility(View.GONE);
            /*jcVideoPlayerStandard.thumbImageView.setImageURI(Uri.parse("http://images.google.com/images?q=tbn:HhcYCoWTN6y3IM:http://msucares.com/news/print/sgnews/sg04/images/sg041007_200.jpg"));
            jcVideoPlayerStandard.tinyBackImageView.setImageURI(Uri.parse("http://images.google.com/images?q=tbn:HhcYCoWTN6y3IM:http://msucares.com/news/print/sgnews/sg04/images/sg041007_200.jpg"));
*/

            jcVideoPlayerStandard.fullscreenButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    changeConfigurationBasedOnCurrentConfiguration();

                }
            });
            jcVideoPlayerStandard.startButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    jcVideoPlayerStandard.startVideo();
                }
            });
            jcVideoPlayerStandard.currentTimeTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


        }
    }

    private void changeConfigurationBasedOnCurrentConfiguration() {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


            jcVideoPlayerStandard.startWindowFullscreen();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);



            //  updateLayout(true);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
           // updateLayout(false);
        }

    }

    private void updateLayout(boolean isLandscape) {
        if (isLandscape) {
            video_description.setVisibility(View.GONE);
            tv_header_videos.setVisibility(View.GONE);
            //jcVideoPlayerStandard.setPadding(0,5,0,0);
            LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0);
            jcVideoPlayerStandard.setLayoutParams(ll);
        } else {
            video_description.setVisibility(View.VISIBLE);
            tv_header_videos.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, (float) 0.5);
            jcVideoPlayerStandard.setLayoutParams(ll);

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        jcVideoPlayerStandard.onStatePause();
        JCVideoPlayer.releaseAllVideos();
    }

    @Override
    public void onStop() {
        super.onStop();
        JCVideoPlayer.releaseAllVideos();
        JCVideoPlayer.backPress();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateLayout(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
