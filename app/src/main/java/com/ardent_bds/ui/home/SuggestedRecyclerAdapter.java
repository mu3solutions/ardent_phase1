package com.ardent_bds.ui.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.model.StatusDataModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SuggestedRecyclerAdapter extends RecyclerView.Adapter<SuggestedRecyclerAdapter.MyViewHolder> {
    ArrayList<StatusDataModel> sta;
    Context context;

    String[] GrantTestImages;
    String[] GrantTestImagesTxt;
    String[] SuggestedVideosList;

    String min5VideosNuggetsTxt;

    public SuggestedRecyclerAdapter(Context ctx, String[] slideUrlArray1, String[] slideUrlArray2, String[] suggestedVideosArrayNames, String min5VideosNuggetsTxt) {
        this.context = ctx;
        this.GrantTestImages = slideUrlArray1;
        this.GrantTestImagesTxt = slideUrlArray2;
        this.SuggestedVideosList = suggestedVideosArrayNames;
        this.min5VideosNuggetsTxt =min5VideosNuggetsTxt;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout suggested_videos_layout;
        TextView suggested_videos_name, statusDesc;
        ImageView suggested_videos_image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);


            suggested_videos_layout = itemView.findViewById(R.id.suggested_videos_layout);
            suggested_videos_image = itemView.findViewById(R.id.suggested_videos_image);
            suggested_videos_name = itemView.findViewById(R.id.suggested_videos_name);
        }
    }

    @NonNull
    @Override
    public SuggestedRecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.suggested_videos_layout, parent, false);
        context = itemView.getContext();
        return new SuggestedRecyclerAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull SuggestedRecyclerAdapter.MyViewHolder holder, final int position) {

        holder.suggested_videos_name.setText(""+ GrantTestImagesTxt[position]);
        Glide.with(context)
                .load(GrantTestImages[position].replace("\r\n", ""))
                .into(holder.suggested_videos_image);


        holder.suggested_videos_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent videourl = new Intent(context, Video5Min.class);
                videourl.putExtra("suggested_all_video", SuggestedVideosList[position]);
                videourl.putExtra("suggested_all_video_txt", GrantTestImagesTxt[position]);
                videourl.putExtra("suggested_all_video_txt_des", min5VideosNuggetsTxt);
                context.startActivity(videourl);
            }
        });


    }

    @Override
    public int getItemCount() {
        return GrantTestImages.length;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
