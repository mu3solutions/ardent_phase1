package com.ardent_bds.ui.home;

import java.util.ArrayList;
import java.util.HashMap;

public class GrantTestDataModel {

    private static GrantTestDataModel instance = null;


    private int questionGTId;
    private int childGTId;

    public int getQuestionGTId() {
        return questionGTId;
    }

    public void setQuestionGTId(int questionGTId) {
        this.questionGTId = questionGTId;
    }

    public int getChildGTId() {
        return childGTId;
    }

    public void setChildGTId(int childGTId) {
        this.childGTId = childGTId;
    }

    public String getGTQuestionDesc() {
        return GTQuestionDesc;
    }

    public void setGTQuestionDesc(String GTQuestionDesc) {
        this.GTQuestionDesc = GTQuestionDesc;
    }

    public String getGTAnswerList() {
        return GTAnswerList;
    }

    public void setGTAnswerList(String GTAnswerList) {
        this.GTAnswerList = GTAnswerList;
    }

    public String getGTAnswerDesc() {
        return GTAnswerDesc;
    }

    public void setGTAnswerDesc(String GTAnswerDesc) {
        this.GTAnswerDesc = GTAnswerDesc;
    }

    public String getGTSelectedAnswerList() {
        return GTSelectedAnswerList;
    }

    public void setGTSelectedAnswerList(String GTSelectedAnswerList) {
        this.GTSelectedAnswerList = GTSelectedAnswerList;
    }

    private String GTQuestionDesc;

    private String GTAnswerList;
    private String GTAnswerDesc;
    private String GTSelectedAnswerList;

    public String getGTImageList() {
        return GTImageList;
    }

    public void setGTImageList(String GTImageList) {
        this.GTImageList = GTImageList;
    }

    private String GTImageList;

    public String getGTCorrectAmswer() {
        return GTCorrectAmswer;
    }

    public void setGTCorrectAmswer(String GTCorrectAmswer) {
        this.GTCorrectAmswer = GTCorrectAmswer;
    }

    private String GTCorrectAmswer;







    private String testName;

    public int getSelecetdtestId() {
        return selecetdtestId;
    }

    public void setSelecetdtestId(int selecetdtestId) {
        this.selecetdtestId = selecetdtestId;
    }

    private int selecetdtestId;

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    private int accessLevel;

    public int getSelectedTestId() {
        return selectedTestId;
    }

    public void setSelectedTestId(int selectedTestId) {
        this.selectedTestId = selectedTestId;
    }

    private int selectedTestId;

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    private int testId;

    public static GrantTestDataModel getInstance() {
        if (instance == null) {
            instance = new GrantTestDataModel();
        }
        return instance;
    }


    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTesticon() {
        return testicon;
    }

    public void setTesticon(String testicon) {
        this.testicon = testicon;
    }

    public String getTestexpiryDate() {
        return testexpiryDate;
    }

    public void setTestexpiryDate(String testexpiryDate) {
        this.testexpiryDate = testexpiryDate;
    }

    public String getTestTotalQuestions() {
        return testTotalQuestions;
    }

    public void setTestTotalQuestions(String testTotalQuestions) {
        this.testTotalQuestions = testTotalQuestions;
    }

    public String getTestTotalTiming() {
        return testTotalTiming;
    }

    public void setTestTotalTiming(String testTotalTiming) {
        this.testTotalTiming = testTotalTiming;
    }

    private String testicon;
    private String testexpiryDate;
    private String testTotalQuestions;
    private String testTotalTiming;


    private String correctAnswerMap;


    public int getTotalCorrectis() {
        return totalCorrectis;
    }

    public void setTotalCorrectis(int totalCorrrctis) {
        this.totalCorrectis = totalCorrrctis;
    }

    private int totalCorrectis;

    public String getCorrectAnswerMap() {
        return correctAnswerMap;
    }

    public void setCorrectAnswerMap(String correctAnswerMap) {
        this.correctAnswerMap = correctAnswerMap;
    }

    public String getUserSelectedAnswerMap() {
        return userSelectedAnswerMap;
    }

    public void setUserSelectedAnswerMap(String userSelectedAnswerMap) {
        this.userSelectedAnswerMap = userSelectedAnswerMap;
    }

    private String userSelectedAnswerMap;

    public HashMap<Integer, Integer> getIsCompleted(int pos) {
        return isCompleted;
    }

    public void setIsCompleted(HashMap<Integer, Integer> isCompleted) {
        this.isCompleted = isCompleted;
    }

    private HashMap<Integer, Integer> isCompleted = new HashMap<>();


    public int getUserCompletedTestStatus() {
        return userCompletedTestStatus;
    }

    public void setUserCompletedTestStatus(int userCompletedTestStatus) {
        this.userCompletedTestStatus = userCompletedTestStatus;
    }

    private int userCompletedTestStatus;

    public int getTotalCorrectAnswers() {
        return totalCorrectAnswers;
    }

    public void setTotalCorrectAnswers(int totalCorrectAnswers) {
        this.totalCorrectAnswers = totalCorrectAnswers;
    }

    private int totalCorrectAnswers;


    public int getGetWrongAnswers() {
        return getWrongAnswers;
    }

    public void setGetWrongAnswers(int getWrongAnswers) {
        this.getWrongAnswers = getWrongAnswers;
    }

    public ArrayList<Integer> getGetSkippedAnswers() {
        return getSkippedAnswers;
    }

    public void setGetSkippedAnswers(ArrayList<Integer> getSkippedAnswers) {
        this.getSkippedAnswers = getSkippedAnswers;
    }

    private int getWrongAnswers;
    private ArrayList<Integer> getSkippedAnswers;

    public HashMap<Integer, Integer> getGetMarkedAnswers() {
        return getMarkedAnswers;
    }

    public void setGetMarkedAnswers(HashMap<Integer, Integer> getMarkedAnswers) {
        this.getMarkedAnswers = getMarkedAnswers;
    }

    private HashMap<Integer, Integer> getMarkedAnswers;

    public HashMap<Integer, Integer> getGetMarkedSelectedAnswers() {
        return getMarkedSelectedAnswers;
    }

    public void setGetMarkedSelectedAnswers(HashMap<Integer, Integer> getMarkedSelectedAnswers) {
        this.getMarkedSelectedAnswers = getMarkedSelectedAnswers;
    }

    private HashMap<Integer, Integer> getMarkedSelectedAnswers;

    public HashMap<Integer, Integer> getGetUnMarkedUserSelectedAnswers() {
        return getUnMarkedUserSelectedAnswers;
    }

    public void setGetUnMarkedUserSelectedAnswers(HashMap<Integer, Integer> getUnMarkedUserSelectedAnswers) {
        this.getUnMarkedUserSelectedAnswers = getUnMarkedUserSelectedAnswers;
    }

    private HashMap<Integer, Integer> getUnMarkedUserSelectedAnswers;


    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isIsmarked() {
        return ismarked;
    }

    public void setIsmarked(boolean ismarked) {
        this.ismarked = ismarked;
    }

    public int getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(int selectedOption) {
        this.selectedOption = selectedOption;
    }

    private boolean ismarked;
    private int selectedOption;


    private HashMap<Integer, Integer> getMarkLocations;

    public HashMap<Integer, Integer> getGetMarkLocations() {
        return getMarkLocations;
    }

    public void setGetMarkLocations(HashMap<Integer, Integer> getMarkLocations) {
        this.getMarkLocations = getMarkLocations;
    }

    public HashMap<Integer, Integer> getGetSelectedLocations() {
        return getSelectedLocations;
    }

    public void setSelectedLocations(HashMap<Integer, Integer> getSelectedLocations) {
        this.getSelectedLocations = getSelectedLocations;
    }

    private HashMap<Integer, Integer> getSelectedLocations;


    public HashMap<Integer, Integer> getAbcd() {
        return abcd;
    }

    public void setAbcd(HashMap<Integer, Integer> abcd) {
        this.abcd = abcd;
    }

    HashMap<Integer, Integer> abcd = new HashMap<>();


    public HashMap<Integer, Integer> getMySelectionList() {
        return MySelectionList;
    }

    public void setMySelectionList(HashMap<Integer, Integer> mySelectionList) {
        MySelectionList = mySelectionList;
    }

    HashMap<Integer, Integer> MySelectionList = new HashMap<>();


    public String getUserSelectedAnswersForResult() {
        return userSelectedAnswersForResult;
    }

    public void setUserSelectedAnswersForResult(String userSelectedAnswersForResult) {
        this.userSelectedAnswersForResult = userSelectedAnswersForResult;
    }

    private String userSelectedAnswersForResult;


    public int getSkippedListCompletedGrandTest() {
        return SkippedListCompletedGrandTest;
    }

    public void setSkippedListCompletedGrandTest(int skippedListCompletedGrandTest) {
        SkippedListCompletedGrandTest = skippedListCompletedGrandTest;
    }

    public int SkippedListCompletedGrandTest;


    public HashMap<Integer, Integer> getAllSkipped() {
        return allSkipped;
    }

    public void setAllSkipped(HashMap<Integer, Integer> allSkipped) {
        this.allSkipped = allSkipped;
    }

    HashMap<Integer, Integer> allSkipped = new HashMap<>();





    HashMap<Integer, Integer> allSkipped_news = new HashMap<>();

    public HashMap<Integer, Integer> getAllSkipped_news() {
        return allSkipped_news;
    }

    public void setAllSkipped_news(HashMap<Integer, Integer> allSkipped_news) {
        this.allSkipped_news = allSkipped_news;
    }

    public HashMap<Integer, Integer> getAllSelected_news() {
        return allSelected_news;
    }

    public void setAllSelected_news(HashMap<Integer, Integer> allSelected_news) {
        this.allSelected_news = allSelected_news;
    }

    public HashMap<Integer, Integer> getAllMarked_news() {
        return allMarked_news;
    }

    public void setAllMarked_news(HashMap<Integer, Integer> allMarked_news) {
        this.allMarked_news = allMarked_news;
    }

    HashMap<Integer, Integer> allSelected_news = new HashMap<>();
    HashMap<Integer, Integer> allMarked_news = new HashMap<>();


    public boolean isNothingSelected() {
        return isNothingSelected;
    }

    public void setNothingSelected(boolean nothingSelected) {
        isNothingSelected = nothingSelected;
    }

    private boolean isNothingSelected;


    private static ArrayList<GrantTestDataModel> arrayInstance = null;

    public static ArrayList<GrantTestDataModel> getArrayInstance() {
        if (arrayInstance == null) {
            arrayInstance = new ArrayList<GrantTestDataModel>();
        }
        return arrayInstance;
    }


    public int getPercentageValue() {
        return percentageValue;
    }

    public void setPercentageValue(int percentageValue) {
        this.percentageValue = percentageValue;
    }

    private int percentageValue;



    private String correctString;
    private String skippedString;

    public String getCorrectString() {
        return correctString;
    }

    public void setCorrectString(String correctString) {
        this.correctString = correctString;
    }

    public String getSkippedString() {
        return skippedString;
    }

    public void setSkippedString(String skippedString) {
        this.skippedString = skippedString;
    }

    public String getActualCorrectString() {
        return actualCorrectString;
    }

    public void setActualCorrectString(String actualCorrectString) {
        this.actualCorrectString = actualCorrectString;
    }

    private String actualCorrectString;



    private String userFinalScore;

    public String getUserWrongAnswers() {
        return userWrongAnswers;
    }

    public void setUserWrongAnswers(String userWrongAnswers) {
        this.userWrongAnswers = userWrongAnswers;
    }

    private String userWrongAnswers;

    public String getUserFinalScore() {
        return userFinalScore;
    }

    public void setUserFinalScore(String userFinalScore) {
        this.userFinalScore = userFinalScore;
    }

    public int getUserRank() {
        return userRank;
    }

    public void setUserRank(int userRank) {
        this.userRank = userRank;
    }

    private int userRank;

    private int correctCount;

    public int getCorrectCount() {
        return correctCount;
    }

    public void setCorrectCount(int correctCount) {
        this.correctCount = correctCount;
    }

    public int getWrongCount() {
        return wrongCount;
    }

    public void setWrongCount(int wrongCount) {
        this.wrongCount = wrongCount;
    }

    public int getSkipCount() {
        return skipCount;
    }

    public void setSkipCount(int skipCount) {
        this.skipCount = skipCount;
    }

    private int wrongCount;
    private int skipCount;
}
