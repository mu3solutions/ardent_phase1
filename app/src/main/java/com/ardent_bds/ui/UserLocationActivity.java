package com.ardent_bds.ui;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.model.UserModel;

import java.util.ArrayList;

public class UserLocationActivity extends BaseActivity {


    private TextView statecode;
    private TextView statename;
    private ArrayList<String> allStateList = new ArrayList<>();
    UserModel userModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.user_location);
        statename = findViewById(R.id.state_name);
        allStateList = getStateList();

        userModel = UserModel.getInstance();
        getCollageAndStateList(userModel.getUserState());


        for (int i = 0; i < allStateList.size(); i++) {
            statename.append(i + 1 + ". " + allStateList.get(i) + " \n ");
        }


    }
}
