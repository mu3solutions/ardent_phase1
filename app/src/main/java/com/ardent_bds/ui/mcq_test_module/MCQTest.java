package com.ardent_bds.ui.mcq_test_module;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.adapters.McqRecyclerAdapter;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.McqModuleData;
import com.ardent_bds.model.McqUserData;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.LiveTest.GrandTestLandingActivity;
import com.ardent_bds.ui.MainActivity;
import com.ardent_bds.ui.gallery.AboutUsActivity;
import com.ardent_bds.ui.gallery.AllVideosSlidesNotesActivity;
import com.ardent_bds.ui.gallery.ContactUsActivity;
import com.ardent_bds.ui.gallery.PaymentHistory;
import com.ardent_bds.ui.gallery.SubscriptionActivity;
import com.ardent_bds.utils.HttpHandler;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.util.ArrayList;

public class MCQTest extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;

    private ImageView menu_slider_right,menu;
    ArrayList<McqModuleData> mcqDataModules;
    ArrayList<McqUserData> mcqUserData;
    MeowBottomNavigation bottomNavigation;
    private AppBarConfiguration mAppBarConfiguration;
    NavigationView navigationView;
    JSONObject jsonObject = null;
    DrawerLayout drawer;
    ImageView toolbar_ivNavigation;
    private ProgressDialog progress_bar_login;
    UserModel userModel;
    TextView userName, userEmailId;
    ImageView userImage;

    String HttpURL = Constants.BaseUrl + "/ArdentDB/mcqModuleData.php";
    String HttpURL2 = Constants.BaseUrl + "/ArdentDB/userData.php";
    TextView textView;
    String TAG = getClass().getSimpleName();

    TextView toolText;
    ImageView toolImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.mcq_main_acticity_1);
        //toolbar_ivNavigation = findViewById(R.id.toolbar_ivNavigation);

        toolImage= findViewById(R.id.tool_image);
        toolText = findViewById(R.id.tool_text);
        initCollapsingToolbar();

        recyclerView = findViewById(R.id.recycler_view_vedio);
        menu_slider_right = findViewById(R.id.menu_slider_right);
        menu = findViewById(R.id.tool_image);
        drawer = findViewById(R.id.drawer_layout_mcq);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        userModel = UserModel.getInstance();

        View nav = navigationView.getHeaderView(0);


        userName = nav.findViewById(R.id.userName);
        userEmailId = nav.findViewById(R.id.userEmailId);
        userImage = nav.findViewById(R.id.userImage);


        userName.setText("" + userModel.getUserName());
        userEmailId.setText("" + userModel.getUserEmailId());

/*
        if (userModel.getUserProfilePic().equalsIgnoreCase("")) {
        } else {
            Glide.with(MCQTest.this)
                    .load(userModel.getUserProfilePic())
                    .into(userImage);
        }
*/


       /* if (userModel.getUserProfilePic().equalsIgnoreCase("")) {
        } else {
            userImage.setImageURI(Uri.parse(userModel.getUserProfilePic()));
            //userImage.setImageBitmap(BitmapFactory.decodeFile(userModel.getUserProfilePic()));
            Uri userProfilePic = Uri.parse(userModel.getUserProfilePic());
            if (userProfilePic != null) {
                new BaseActivity.ImageLoadTask(userProfilePic.toString(), userImage).execute();
            }
        }*/

/*
        userImage.setImageURI(Uri.parse(userModel.getUserProfilePic()));
        //userImage.setImageBitmap(BitmapFactory.decodeFile(userModel.getUserProfilePic()));

        Uri userProfilePic = Uri.parse(userModel.getUserProfilePic());
        if (userProfilePic != null) {
            new ImageLoadTask(userProfilePic.toString(), userImage).execute();
        }
*/


        Log.i("TAG in MCQ Test screen", "" + TAG);

        menu_slider_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT, true);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT, true);
            }
        });


        bottomNavigation = findViewById(R.id.bottom_nav);
        bottomViewNavigation();


        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getAllMcqModuleData();
        }
        else{
            Toast.makeText(MCQTest.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }




        // Intent intent= new Intent(getApplicationContext(), McqModuleActivity.class);
        //startActivity(intent);
    }
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingToolbar.setTitle(" nn");
        //collapsingToolbar.setPointerIcon(R.drawable.ic_baseline_arrow_back_24);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();

                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Bala");
                    toolImage.setVisibility(View.GONE);
                    toolText.setVisibility(View.VISIBLE);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" Bla");
                    toolImage.setVisibility(View.VISIBLE);
                    toolText.setVisibility(View.GONE);
                    isShow = false;
                }
            }
        });
    }




    private void getAllMcqModuleData() {
        new GetMcqModuleListDataFromServer(getApplicationContext()).execute();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_payment) {
            Intent i1 = new Intent(this, PaymentHistory.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i1.putExtras(bundle);
            startActivity(i1);

        } else if (id == R.id.nav_subscribe) {
            //startActivity(new Intent(this, SubscriptionActivity.class));

            Intent i2 = new Intent(this, SubscriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i2.putExtras(bundle);
            startActivity(i2);

        } else if (id == R.id.nav_bookmark) {

        } else if (id == R.id.nav_share_app) {
            shareApp();

        } else if (id == R.id.nav_help) {
            //startActivity(new Intent(this, ContactUsActivity.class));

            Intent i3 = new Intent(this, ContactUsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i3.putExtras(bundle);
            startActivity(i3);


        } else if (id == R.id.nav_menu_about_us) {
            //startActivity(new Intent(this, AboutUsActivity.class));

            Intent i4 = new Intent(this, AboutUsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i4.putExtras(bundle);
            startActivity(i4);


        } else if (id == R.id.nav_menu_settings) {

        } else if (id == R.id.nav_menu_logout) {

            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to Logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(new Intent(MCQTest.this, MainActivity.class));
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }

        drawer.closeDrawer(GravityCompat.END);
        return true;
    }


    public class GetMcqModuleListDataFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetMcqModuleListDataFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_bar_login = new ProgressDialog(MCQTest.this);
            progress_bar_login.setMessage("Please wait...");
            progress_bar_login.setCancelable(false);
            progress_bar_login.show();

        }
        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler httpHandler = new HttpHandler();
            String responseFromDB = httpHandler.makeServiceCall(HttpURL);
            if(responseFromDB!=null) {
                String userResponseFromDb = httpHandler.makeServiceCallUserData(HttpURL2);
                mcqUserData = BaseActivity.getMcqUserDetailsList(userResponseFromDb);
                mcqDataModules = BaseActivity.getMcqDetailsList(responseFromDB);
                mcqDataModules = updateUserDataInModule(mcqDataModules, mcqUserData);
            }
            return null;
        }

        private ArrayList<McqModuleData> updateUserDataInModule(ArrayList<McqModuleData> mcqDataModules, ArrayList<McqUserData> mcqUserData) {
            ArrayList<McqModuleData> data=mcqDataModules;
            ArrayList<McqUserData> userData = mcqUserData;
            for (int i =0; i<data.size(); i++){
                int mid = data.get(i).getModuleId();
                int completedCount=0;
                for (int j =0; j<userData.size(); j++){
                    int umid = userData.get(j).getModuleId();
                    String staus = userData.get(j).getStatus();

                    if (mid == umid && staus.equals("completed")){
                        completedCount+=1;
                    }
                }
                data.get(i).setCompletedModule(completedCount);
            }
            return data;
        }

        protected void onPostExecute(Void result) {
            //userModel.setUserOnlineStatus(true);
            //userModel.setUserStatus("Online");
            /*Intent intent = new Intent(getApplicationContext(), NavigationDrawerActivity.class);
            //intent.putExtra("account", acco);
            startActivity(intent);*/

            progress_bar_login.dismiss();
            recyclerView.setHasFixedSize(true);

            layoutManager = new GridLayoutManager(getApplicationContext(), 1);
            recyclerView.setLayoutManager(layoutManager);

            adapter = new McqRecyclerAdapter(mcqDataModules);
            recyclerView.setAdapter(adapter);

        }
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        overridePendingTransition(0, 0);
        startActivity(new Intent(MCQTest.this, DashBoardActivity.class));

    }


    public void bottomViewNavigation() {
        bottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.ic_account_balance_black_24dp));
        bottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.ic_baseline_library_add_check_24));
        bottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.ic_assignment_black_24dp));
        bottomNavigation.add(new MeowBottomNavigation.Model(4, R.drawable.ic_live_tv_black_24dp));

        bottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                /* Toast.makeText(NavigationDrawerActivity.this, "clicked item : " + item.getId(), Toast.LENGTH_SHORT).show();*/
                if (item.getId() == 1) {
                    //SlideMenu fragment = new SlideMenu();
                    /*startActivity(new Intent(getActivity(), SlideMenu.class));*/
                    overridePendingTransition(0, 0);
                    Intent i1 = new Intent(MCQTest.this, DashBoardActivity.class);
                    i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i1);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 2) {
                    //startActivity(new Intent(getActivity(), MCQTest.class));

                    overridePendingTransition(0, 0);
                    Intent i2 = new Intent(MCQTest.this, MCQTest.class);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i2);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 3) {
                    //startActivity(new Intent(getActivity(), GrandTestLandingActivity.class));
                    overridePendingTransition(0, 0);
                    Intent i3 = new Intent(MCQTest.this, GrandTestLandingActivity.class);
                    i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i3);
                    overridePendingTransition(0, 0);

                } else if (item.getId() == 4) {
                    overridePendingTransition(0, 0);
                    Intent i4 = new Intent(MCQTest.this, AllVideosSlidesNotesActivity.class);
                    i4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i4);
                    overridePendingTransition(0, 0);
                }
            }
        });
        bottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {

                String name;
                switch (item.getId()) {
                    case 1:
                        name = "HOME";
                        break;
                    case 2:
                        name = "EXPLORE";
                        break;
                    case 3:
                        name = "MESSAGE";
                        break;
                    case 4:
                        name = "VIDEOS";
                        break;
                    default:
                        name = "";
                }
            }
        });

        bottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {
                /*  Toast.makeText(NavigationDrawerActivity.this, "reselected item : " + item.getId(), Toast.LENGTH_SHORT).show();*/
            }
        });


       // bottomNavigation.setCount(2, "116");
        bottomNavigation.show(2, true);
    }


    protected void onDestroy() {
        super.onDestroy();
    }

    private void shareApp() {

        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "ArDent BDS Application");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + "com.ardent_bds" + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }


}