package com.ardent_bds.ui.mcq_test_module;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.ardent_bds.R;
import com.ardent_bds.adapters.ReviewAdapter;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.McqSubModleData;
import com.ardent_bds.model.McqUserData;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ReviewTestActivity extends FragmentActivity {
    static final String TAG = "OnBoardingActivity";
    int page = 0;
    ArrayList<QuestionDataModel> questionsDataModules;

    String HttpGetUrl = Constants.BaseUrl + "/ArdentDb/getReviewQuestions.php";

    String[] count = {"one", "two", "three", "four", "five"};
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static int NUM_PAGES = 5;
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager2 viewPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private ReviewAdapter pagerAdapter;

    McqSubModleData mcqSubModleData;
    ArrayList<McqUserData> mcqUserData = new ArrayList<>();
    ImageView close, next, prev;
    TextView textView;
    int pageNm = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_review_test);

        mcqSubModleData = McqSubModleData.getInstance();
        mcqUserData = McqUserData.getInstance();

        viewPager = findViewById(R.id.pager);
        next = findViewById(R.id.next_arrow);
        prev = findViewById(R.id.prev_arrow);
        close = findViewById(R.id.close_close);
        textView = findViewById(R.id.count_text);
        getAllMCQQuestions();
        pageNm = page + 1;
        textView.setText(pageNm + " / " + questionsDataModules.size());

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page += 1;
                viewPager.setCurrentItem(page, true);
            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page -= 1;
                viewPager.setCurrentItem(page, true);
            }
        });

     close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(ReviewTestActivity.this, McqResultActivity.class));
            }
        });

    }

    private void getAllMCQQuestions() {
        HashMap<Integer, Integer> map = null;
        questionsDataModules = QuestionDataModel.getArrayInstance();
        for (int i = 0; i < mcqUserData.size(); i++) {
            if (mcqUserData.get(i).getChildId() == mcqSubModleData.getChildModuleId()) {
                String hash = mcqUserData.get(i).getQnSelectedAnswer();
                hash = hash.substring(1, hash.length() - 1);           //remove curly brackets

                String[] keyValuePairs = hash.split(",");              //split the string to creat key-value pairs
                map = new HashMap<>();
                for (String pair : keyValuePairs)                        //iterate over the pairs
                {
                    String[] entry = pair.split("=");                   //split the pairs to get key and value
                    map.put(Integer.parseInt(entry[0].trim()), Integer.parseInt(entry[1].trim()));          //add them to the hashmap and trim whitespaces
                }
            }
        }
    //    Toast.makeText(this, "mao: " + map, Toast.LENGTH_SHORT).show();

        HashMap<Integer, Integer> hashMap = new HashMap<>();
        hashMap.put(1, 3);
        hashMap.put(2, 2);
        hashMap.put(3, 2);
        hashMap.put(4, 1);
        hashMap.put(5, 1);
        hashMap.put(6, 2);

     //   Toast.makeText(this, "sdsd" + questionsDataModules.size(), Toast.LENGTH_SHORT).show();
        if (questionsDataModules.size() > 0) {
            pagerAdapter = new ReviewAdapter(this, questionsDataModules, map, viewPager);
            viewPager.setAdapter(pagerAdapter);
            viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    viewPager.setCurrentItem(page, true);
                }

                @Override
                public void onPageSelected(int position) {
                    page = viewPager.getCurrentItem();
                    next.setVisibility(position == questionsDataModules.size() - 1 ? View.GONE : View.VISIBLE);
                    prev.setVisibility(position != 0 ? View.VISIBLE : View.GONE);

                    pageNm = page + 1;
                    textView.setText(pageNm + " / " + questionsDataModules.size());

                    viewPager.setCurrentItem(page, true);

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }

    }


    @Override
    public void onBackPressed() {

    }


}