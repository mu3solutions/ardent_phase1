package com.ardent_bds.ui.mcq_test_module;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.LinkAddress;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.McqModuleData;
import com.ardent_bds.model.McqSubModleData;
import com.ardent_bds.project_utils.HttpPostHandlerMCQTestSubModuleData;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

public class McqTestIntroActivity extends AppCompatActivity {
    TextView moduleName, childName, childDesc, qnCount, startMcq;
    ImageView childImage, close;

    ArrayList<QuestionDataModel> questionsDataModules;
    String HttpGetUrl = Constants.BaseUrl + "/ArdentDB/getReviewQuestions.php";

    ShimmerFrameLayout shimmer_layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_mcq_test_intro);
        McqSubModleData mcqSubModleData = McqSubModleData.getInstance();
        McqModuleData mcqModuleData = McqModuleData.getInstance();


        shimmer_layout = findViewById(R.id.shimmer_layout);


        shimmer_layout.startShimmerAnimation();
        moduleName = findViewById(R.id.mcq_module_name);
        childName = findViewById(R.id.child_name);
        childDesc = findViewById(R.id.child_desc);
        qnCount = findViewById(R.id.total_num_qn);
        childImage = findViewById(R.id.child_image);
        close = findViewById(R.id.close);

        startMcq = findViewById(R.id.start_mcq);
        startMcq.setVisibility(View.GONE);

        startMcq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), McqTestActivity.class);
                startActivity(intent);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        moduleName.setText(mcqModuleData.getModuleName());
        childName.setText(mcqSubModleData.getChildModuleName());
        childDesc.setText(mcqSubModleData.getChildModuleDesc());

        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getAllMCQQuestions();
        } else {
            Toast.makeText(McqTestIntroActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }

    }

    private void getAllMCQQuestions() {
        new GetMCQQuestionsFromServer(getApplicationContext()).execute();

    }


    public class GetMCQQuestionsFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;

        public GetMCQQuestionsFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /*
                @Override
                protected Void doInBackground(Void... arg0) {
                    HttpPostHandlerSubModuleData httpPostHandler = new HttpPostHandlerSubModuleData();
                    String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetUrl);
                    Log.e("Questions data ", "Question responseFromDB: " + responseFromDB);
                    questionsDataModules= QuestionDataModel.getArrayInstance();
                    questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);

                    return null;
                }
                */
        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerMCQTestSubModuleData httpPostHandler = new HttpPostHandlerMCQTestSubModuleData();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetUrl);
            if (responseFromDB != null) {
                Log.e("Questions data ", "Question responseFromDB: " + responseFromDB);
                questionsDataModules = QuestionDataModel.getArrayInstance();
                questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            //   Toast.makeText(context, "sd" + questionsDataModules.size(), Toast.LENGTH_SHORT).show();
            qnCount.setText(String.valueOf(questionsDataModules.size()));
            shimmer_layout.stopShimmerAnimation();
            shimmer_layout.setVisibility(View.GONE);
            qnCount.setVisibility(View.VISIBLE);
            startMcq.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {

        // super.onBackPressed();
        overridePendingTransition(0, 0);

        startActivity(new Intent(McqTestIntroActivity.this, McqSubModuleActivity.class));
    }
}