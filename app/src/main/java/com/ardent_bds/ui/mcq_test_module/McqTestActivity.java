package com.ardent_bds.ui.mcq_test_module;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ardent_bds.R;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.customviews.LockableViewPager;
import com.ardent_bds.model.McqSubModleData;
import com.ardent_bds.project_utils.HttpPostHandlerSubModuleData;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class McqTestActivity extends BaseActivity implements View.OnClickListener {
    LockableViewPager mViewPager;
    Button mNextBtn;
    Button mSkipBtn, mFinishBtn;
    CoordinatorLayout mCoordinator;
    static final String TAG = "OnBoardingActivity";
    int page = 0;
    int Score = 0;
    ArrayList<QuestionDataModel> questionsDataModules;
    HashMap<Integer, Integer> selectedAnswerList;
    QuestionDataModel questionDataModel;
    McqSubModleData mcqSubModleData;
    int score = 0;
    int scorePercent = 0;
    String HttpUpdateUrl = Constants.BaseUrl + "/ArdentDB/updateQuestions.php";
    boolean optionClicked = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_mcq_test);

        questionDataModel = QuestionDataModel.getInstance();
        mcqSubModleData = McqSubModleData.getInstance();

        selectedAnswerList = new HashMap<>();
        mNextBtn = (Button) findViewById(R.id.intro_btn_next);

        mSkipBtn = (Button) findViewById(R.id.intro_btn_skip);
        mFinishBtn = (Button) findViewById(R.id.intro_btn_finish);

        mCoordinator = (CoordinatorLayout) findViewById(R.id.main_content);

        mViewPager = (LockableViewPager) findViewById(R.id.container);

        mViewPager.setSwipable(true);
        getAllMCQQuestions();

        mNextBtn.setOnClickListener(this);
        mSkipBtn.setOnClickListener(this);
        mFinishBtn.setOnClickListener(this);


        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (optionClicked) {
                    page += 1;
                    mViewPager.setCurrentItem(page, true);
                } else {
                    Toast.makeText(McqTestActivity.this, "Please select an option", Toast.LENGTH_SHORT).show();
                }

            }
        });
        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (optionClicked) {
                    new SweetAlertDialog(McqTestActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Confirm")
                            .setContentText("Are you sure to submit test!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                    questionDataModel.setMcqSelectedAnswerList(String.valueOf(selectedAnswerList));
                                    Log.v(TAG, "HashMap" + questionDataModel.getMcqSelectedAnswerList());
                                   // Toast.makeText(getApplicationContext(), "chihd" + questionDataModel.getMcqSelectedAnswerList(), Toast.LENGTH_SHORT).show();
                                    calculateScore();

                                    ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                                    NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
                                    if (networkInfo != null && networkInfo.isConnected() == true) {
                                        updateMCQQuestions();
                                    }
                                    else{
                                        Toast.makeText(McqTestActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
                                    }

                                }

                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();
                } else {
                    Toast.makeText(McqTestActivity.this, "Please select option", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void calculateScore() {
        for (int i = 0; i < questionsDataModules.size(); i++) {
            QuestionDataModel dataModel = questionsDataModules.get(i);
            int qn = dataModel.getQuestionId();
            int crctAns = dataModel.getMcqCorrectAnswer();
            int seletAns = selectedAnswerList.get(qn);
            if (crctAns == seletAns) {
                score += 1;
            }
        }
        scorePercent = score * 100 / questionsDataModules.size();
    }

    private void updateMCQQuestions() {
        new UpdateMCQQuestionsFromServer(getApplicationContext()).execute();

    }


    public class UpdateMCQQuestionsFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;

        public UpdateMCQQuestionsFromServer(Context applicationContext) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            mcqSubModleData.setScore(score);
            mcqSubModleData.setScorePercent(scorePercent);
            HttpPostHandlerSubModuleData httpPostHandler = new HttpPostHandlerSubModuleData();
            String responseFromDB = httpPostHandler.makeServiceUpdateCall(HttpUpdateUrl);
            //Toast.makeText(context, ""+mcqSubModleData.getChildModuleId(), Toast.LENGTH_SHORT).show();
//            Toast.makeText(context, "sd"+responseFromDB, Toast.LENGTH_LONG).show();
            if(responseFromDB!=null) {
                Log.e("Questions data ", "Question responseFromDB: " + responseFromDB);
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            startActivity(new Intent(McqTestActivity.this, McqResultActivity.class));
        }
    }

    private void getAllMCQQuestions() {
        questionsDataModules = QuestionDataModel.getArrayInstance();
        //Toast.makeText(this, "sdsd"+questionsDataModules.size(), Toast.LENGTH_SHORT).show();
        if (questionsDataModules.size() > 0) {
            MCQViewPagerViewAdapter photoViewAdapter = new MCQViewPagerViewAdapter(getApplicationContext(), questionsDataModules);
            mViewPager.setSwipable(false);
            mViewPager.setAdapter(photoViewAdapter);

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    optionClicked = false;
                    page = position;
                    mNextBtn.setVisibility(position == questionsDataModules.size() - 1 ? View.GONE : View.VISIBLE);
                    mFinishBtn.setVisibility(position == questionsDataModules.size() - 1 ? View.VISIBLE : View.GONE);

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }

    }


    @Override
    public void onClick(View view) {

    }


    public class MCQViewPagerViewAdapter extends PagerAdapter {
        Context context;
        ArrayList<QuestionDataModel> all_folder = new ArrayList<>();
        int int_position;
        ArrayList<Integer> hmp1 = new ArrayList<>();
        LayoutInflater inflater;
        int correctAnswerPosition;
        private int selectedPos = RecyclerView.NO_POSITION;
        TextView option_id, answer_txt1, answer_txt2, answer_txt3, answer_txt4, question_txt, txt_description_answer, proceed_tv, qntotalandcount;
        ImageView imv_tick_cross1, imv_tick_cross2, imv_tick_cross3, imv_tick_cross4;
        LinearLayout ll1, ll2, ll3, ll4, ll_answer_desc;
        SeekBar completeSeekBar;

        public MCQViewPagerViewAdapter(Context context, ArrayList<QuestionDataModel> all_folder) {
            this.context = context;
            this.all_folder = all_folder;
        }


        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((LinearLayout) object);
        }

        @Override
        public int getCount() {
            return all_folder.size();
        }


        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            final ImageView answerImageView;
            final boolean[] clicked = {false};
            optionClicked = false;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View viewLayout = inflater.inflate(R.layout.mcq_cards_layout, container, false);

            qntotalandcount=viewLayout.findViewById(R.id.txt_total_completed);
            completeSeekBar = viewLayout.findViewById(R.id.completedSekBar);
            answerImageView = viewLayout.findViewById(R.id.answer_image);
            txt_description_answer = viewLayout.findViewById(R.id.txt_description_answer);
            qntotalandcount.setText((position+1) + " / " + all_folder.size());
            answerImageView.setVisibility(View.GONE);
            txt_description_answer.setVisibility(View.GONE);
            completeSeekBar.setProgress(position);
            completeSeekBar.setMax(all_folder.size() - 1);


            if (all_folder.get(position).getMcqAnswerImage() != null) {
                Glide.with(context)
                        .load(all_folder.get(position).getMcqAnswerImage())
                        .into(answerImageView);
            }


            completeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            question_txt = viewLayout.findViewById(R.id.question_txt);
            ll_answer_desc = viewLayout.findViewById(R.id.ll_answer_desc);
            answer_txt1 = viewLayout.findViewById(R.id.answer1);
            answer_txt2 = viewLayout.findViewById(R.id.answer2);
            answer_txt3 = viewLayout.findViewById(R.id.answer3);
            answer_txt4 = viewLayout.findViewById(R.id.answer4);
            txt_description_answer = viewLayout.findViewById(R.id.txt_description_answer);
            proceed_tv = viewLayout.findViewById(R.id.proceed_tv);


            ll1 = viewLayout.findViewById(R.id.ll1);
            ll2 = viewLayout.findViewById(R.id.ll2);
            ll3 = viewLayout.findViewById(R.id.ll3);
            ll4 = viewLayout.findViewById(R.id.ll4);


            imv_tick_cross1 = viewLayout.findViewById(R.id.imv_tick_cross1);
            imv_tick_cross2 = viewLayout.findViewById(R.id.imv_tick_cross2);
            imv_tick_cross3 = viewLayout.findViewById(R.id.imv_tick_cross3);
            imv_tick_cross4 = viewLayout.findViewById(R.id.imv_tick_cross4);


            String ques = all_folder.get(position).getMcqQuestionDesc();
            String[] queStringArray = ques.split(",");//we shold not use split becoz if we have a comma in qn it will cause error
            final int qnId = all_folder.get(position).getQuestionId();
            question_txt.setText("" + queStringArray[0]);
            answer_txt1.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[0].replace("\r\n", ""));
            answer_txt2.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[1].replace("\r\n", ""));
            answer_txt3.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[2].replace("\r\n", ""));
            answer_txt4.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[3].replace("\r\n", ""));
            correctAnswerPosition = all_folder.get(position).getMcqCorrectAnswer();

            imv_tick_cross1.setVisibility(View.GONE);
            imv_tick_cross2.setVisibility(View.GONE);
            imv_tick_cross3.setVisibility(View.GONE);
            imv_tick_cross4.setVisibility(View.GONE);

            if (correctAnswerPosition == 1) {
                imv_tick_cross1.setImageResource(R.drawable.green_tick);
                imv_tick_cross2.setImageResource(R.drawable.red_cross);
                imv_tick_cross3.setImageResource(R.drawable.red_cross);
                imv_tick_cross4.setImageResource(R.drawable.red_cross);
            } else if (correctAnswerPosition == 2) {
                imv_tick_cross1.setImageResource(R.drawable.red_cross);
                imv_tick_cross2.setImageResource(R.drawable.green_tick);
                imv_tick_cross3.setImageResource(R.drawable.red_cross);
                imv_tick_cross4.setImageResource(R.drawable.red_cross);
            } else if (correctAnswerPosition == 3) {
                imv_tick_cross1.setImageResource(R.drawable.red_cross);
                imv_tick_cross2.setImageResource(R.drawable.red_cross);
                imv_tick_cross3.setImageResource(R.drawable.green_tick);
                imv_tick_cross4.setImageResource(R.drawable.red_cross);
            } else if (correctAnswerPosition == 4) {
                imv_tick_cross1.setImageResource(R.drawable.red_cross);
                imv_tick_cross2.setImageResource(R.drawable.red_cross);
                imv_tick_cross3.setImageResource(R.drawable.red_cross);
                imv_tick_cross4.setImageResource(R.drawable.green_tick);
            }

            hmp1.add(position, correctAnswerPosition);


            ll1.setTag("l1");
            imv_tick_cross1.setTag("im1");
            ll1.setBackgroundColor(Color.WHITE);
            ll1.setSelected(false);

            ll2.setTag("l2");
            imv_tick_cross2.setTag("im2");
            ll2.setBackgroundColor(Color.WHITE);
            ll2.setSelected(false);

            ll3.setTag("l3");
            imv_tick_cross3.setTag("im3");
            ll3.setBackgroundColor(Color.WHITE);
            ll3.setSelected(false);

            ll4.setTag("l4");
            imv_tick_cross4.setTag("im4");
            ll4.setBackgroundColor(Color.WHITE);
            ll4.setSelected(false);
            optionClicked = false;
            //why use Tag we can directly use onclick on ll1
            viewLayout.findViewWithTag("l1").setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!clicked[0]) {
                        selectedPos = 1;
                        optionClicked = true;
                        clicked[0] = true;
                        selectedAnswerList.put(qnId, selectedPos);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);

                                viewLayout.findViewWithTag("im1").setVisibility(View.VISIBLE);
                                if (selectedPos != hmp1.get(position)) {
                                    if (hmp1.get(position) == 1) {
                                        viewLayout.findViewWithTag("im1").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 2) {
                                        viewLayout.findViewWithTag("im2").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 3) {
                                        viewLayout.findViewWithTag("im3").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 4) {
                                        viewLayout.findViewWithTag("im4").setVisibility(View.VISIBLE);
                                    }
                                }
                                answerImageView.setVisibility(View.VISIBLE);
                                txt_description_answer.setVisibility(View.VISIBLE);
                            }
                        }, 1000);
                        if (selectedPos == hmp1.get(position)) {
                            if (hmp1.get(position) == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (hmp1.get(position) == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (hmp1.get(position) == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (hmp1.get(position) == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                        } else {
                            if (hmp1.get(position) == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (hmp1.get(position) == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (hmp1.get(position) == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (hmp1.get(position) == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                            if (selectedPos == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (selectedPos == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (selectedPos == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (selectedPos == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                        }
                    }
                }

            });
            viewLayout.findViewWithTag("l2").setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!clicked[0]) {
                        selectedPos = 2;
                        optionClicked = true;
                        clicked[0] = true;
                        selectedAnswerList.put(qnId, selectedPos);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);

                                viewLayout.findViewWithTag("im2").setVisibility(View.VISIBLE);
                                if (selectedPos != hmp1.get(position)) {
                                    if (hmp1.get(position) == 1) {
                                        viewLayout.findViewWithTag("im1").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 2) {
                                        viewLayout.findViewWithTag("im2").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 3) {
                                        viewLayout.findViewWithTag("im3").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 4) {
                                        viewLayout.findViewWithTag("im4").setVisibility(View.VISIBLE);
                                    }
                                }

                                txt_description_answer.setVisibility(View.VISIBLE);
                                answerImageView.setVisibility(View.VISIBLE);
                            }
                        }, 1000);

                        if (selectedPos == hmp1.get(position)) {
                            if (hmp1.get(position) == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (hmp1.get(position) == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (hmp1.get(position) == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (hmp1.get(position) == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                        } else {
                            if (hmp1.get(position) == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (hmp1.get(position) == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (hmp1.get(position) == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (hmp1.get(position) == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                            if (selectedPos == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (selectedPos == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (selectedPos == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (selectedPos == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                        }
                    }
                }
            });
            viewLayout.findViewWithTag("l3").setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!clicked[0]) {
                        selectedPos = 3;
                        optionClicked = true;
                        clicked[0] = true;
                        selectedAnswerList.put(qnId, selectedPos);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);

                                viewLayout.findViewWithTag("im3").setVisibility(View.VISIBLE);
                                if (selectedPos != hmp1.get(position)) {
                                    if (hmp1.get(position) == 1) {
                                        viewLayout.findViewWithTag("im1").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 2) {
                                        viewLayout.findViewWithTag("im2").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 3) {
                                        viewLayout.findViewWithTag("im3").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 4) {
                                        viewLayout.findViewWithTag("im4").setVisibility(View.VISIBLE);
                                    }
                                }
                                answerImageView.setVisibility(View.VISIBLE);
                                txt_description_answer.setVisibility(View.VISIBLE);
                            }
                        }, 1000);
                        if (selectedPos == hmp1.get(position)) {
                            if (hmp1.get(position) == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (hmp1.get(position) == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (hmp1.get(position) == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (hmp1.get(position) == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                        } else {
                            if (hmp1.get(position) == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (hmp1.get(position) == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (hmp1.get(position) == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (hmp1.get(position) == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                            if (selectedPos == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (selectedPos == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (selectedPos == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (selectedPos == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                        }
                    }
                }
            });
            viewLayout.findViewWithTag("l4").setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!clicked[0]) {
                        selectedPos = 4;
                        optionClicked = true;
                        clicked[0] = true;
                        selectedAnswerList.put(qnId, selectedPos);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.WHITE);
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.WHITE);


                                viewLayout.findViewWithTag("im4").setVisibility(View.VISIBLE);
                                if (selectedPos != hmp1.get(position)) {
                                    if (hmp1.get(position) == 1) {
                                        viewLayout.findViewWithTag("im1").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 2) {
                                        viewLayout.findViewWithTag("im2").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 3) {
                                        viewLayout.findViewWithTag("im3").setVisibility(View.VISIBLE);
                                    } else if (hmp1.get(position) == 4) {
                                        viewLayout.findViewWithTag("im4").setVisibility(View.VISIBLE);
                                    }
                                }
                                answerImageView.setVisibility(View.VISIBLE);
                                txt_description_answer.setVisibility(View.VISIBLE);
                            }
                        }, 1000);
                        if (selectedPos == hmp1.get(position)) {
                            if (hmp1.get(position) == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (hmp1.get(position) == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (hmp1.get(position) == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (hmp1.get(position) == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                        } else {
                            if (hmp1.get(position) == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (hmp1.get(position) == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (hmp1.get(position) == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (hmp1.get(position) == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.GREEN);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                            if (selectedPos == 1) {
                                viewLayout.findViewWithTag("l1").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l1").setSelected(true);
                            } else if (selectedPos == 2) {
                                viewLayout.findViewWithTag("l2").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l2").setSelected(true);
                            } else if (selectedPos == 3) {
                                viewLayout.findViewWithTag("l3").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l3").setSelected(true);
                            } else if (selectedPos == 4) {
                                viewLayout.findViewWithTag("l4").setBackgroundColor(Color.RED);
                                viewLayout.findViewWithTag("l4").setSelected(true);
                            }
                        }
                    }
                }
            });

            mViewPager.setOffscreenPageLimit(all_folder.size());
            container.addView(viewLayout);
            return viewLayout;
        }
    }

    @Override
    public void onBackPressed() {
        new SweetAlertDialog(McqTestActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Confirm")
                .setContentText("Are you sure leave Mcq test!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        finish();
                        startActivity(new Intent(getApplicationContext(), McqSubModuleActivity.class));
                    }

                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .show();
    }
}