package com.ardent_bds.ui.mcq_test_module;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.adapters.McqSubModuleRecyclerAdapter;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.ListItem;
import com.ardent_bds.model.McqModuleData;
import com.ardent_bds.model.McqSMContentItem;
import com.ardent_bds.model.McqSMHeader;
import com.ardent_bds.model.McqSubModleData;
import com.ardent_bds.model.McqUserData;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.LiveTest.HttpPostUpdatedUserDetailsHandler;
import com.ardent_bds.ui.UserAccessLevelModel;
import com.ardent_bds.utils.HttpHandler;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.ardent_bds.ui.BaseActivity.getUserData;

public class McqSubModuleActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    List<List<ListItem>> arraylist;
    JSONObject jsonObject = null;
    McqSubModleData mcqSubModuleData;
    ArrayList<McqSubModleData> mcqSubModuleDatas;

    TextView txt_total_completed;
    McqModuleData dataModule;
    SeekBar completedSeekBar;
    String HttpPostURL = Constants.BaseUrl + "/ArdentDB/mcqSubModuleData.php";
    String HttpGetURL_Updated_User_details = Constants.BaseUrl + "/ArdentDB/getUpdatedUserDetails.php";
    TextView toolText, module_name;
    ImageView toolImage;

    ArrayList<UserAccessLevelModel> userAccessList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_mcq_sub_module);

        recyclerView = findViewById(R.id.recycler_view_vedio);
        toolImage = findViewById(R.id.tool_image);
        toolText = findViewById(R.id.tool_text);
        initCollapsingToolbar();

        dataModule = McqModuleData.getInstance();
        mcqSubModuleDatas = new ArrayList<>();
        // insertVedioModuleData();
        mcqSubModuleData = McqSubModleData.getInstance();
        completedSeekBar = findViewById(R.id.completedSeekBar);
        module_name = findViewById(R.id.module_name);
        txt_total_completed = findViewById(R.id.txt_total_completed);

        toolImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        completedSeekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });


        module_name.setText("" + McqModuleData.getInstance().getModuleName());

        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getUpdatedUserData();
            getAllSmData();

        } else {
            Toast.makeText(McqSubModuleActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


        completedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                completedSeekBar.setProgress(progress++);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingToolbar.setTitle(" nn");
        //collapsingToolbar.setPointerIcon(R.drawable.ic_baseline_arrow_back_24);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        appBarLayout.setExpanded(true);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();

                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Bala");
                    toolImage.setVisibility(View.GONE);
                    toolText.setVisibility(View.VISIBLE);

                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" Bla");
                    toolImage.setVisibility(View.VISIBLE);
                    toolText.setVisibility(View.GONE);
                    isShow = false;
                }
            }
        });
    }

    private void getAllSmData() {
        new GetSubModuleDataDataFromServer(getApplicationContext()).execute();

    }


    public class GetSubModuleDataDataFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;

        public GetSubModuleDataDataFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler httpPostHandler = new HttpHandler();
            String responseFromDB = httpPostHandler.makeServiceCallSmData(HttpPostURL);
            if(responseFromDB!=null) {
                Log.e("Sub module data ", "Submodule data responseFromDB: " + responseFromDB);
                ArrayList<McqUserData> formList = McqUserData.getInstance();
                mcqSubModuleDatas = BaseActivity.getMcqSubModuleDetailsList(responseFromDB);
                mcqSubModuleDatas = updateUserDataInSubModule(mcqSubModuleDatas, formList);
            }
            return null;
        }

        private ArrayList<ListItem> getList(ArrayList<McqSubModleData> mcqSubModuleDatas) {
            ArrayList<ListItem> arrayList = new ArrayList<>();
            ArrayList<McqSubModleData> mcqSMDatas = mcqSubModuleDatas;
            int headId = 0;
            String headName = "";
            for (int i = 0; i < mcqSMDatas.size(); i++) {
                if (dataModule.getModuleId() == mcqSMDatas.get(i).getModuleId()) {
                    McqSMHeader header = new McqSMHeader();
                    McqSMContentItem contentItem = new McqSMContentItem();

                    if (headId == 0) {
                        headId = mcqSMDatas.get(i).getSubModuleId();
                        headName = mcqSMDatas.get(i).getSubModuleName();

                        header.setId(headId);
                        header.setName(headName);
                        arrayList.add(header);
                        contentItem.setSmChildId(mcqSMDatas.get(i).getChildModuleId());
                        contentItem.setSmChildName(mcqSMDatas.get(i).getChildModuleName());
                        contentItem.setSmChildDesc(mcqSMDatas.get(i).getChildModuleDesc());
                        contentItem.setSmChildimage(mcqSMDatas.get(i).getChildModuleImage());
                        contentItem.setSmChildRating(mcqSMDatas.get(i).getChildModuleRating());
                        contentItem.setStatus(mcqSMDatas.get(i).getStatus());
                        contentItem.setAccessLevel(mcqSMDatas.get(i).getAccessLevel());
                        contentItem.setQnConut(mcqSMDatas.get(i).getQnCount());
                        arrayList.add(contentItem);
                    } else {
                        if (headId == mcqSMDatas.get(i).getSubModuleId()) {
                            contentItem.setSmChildId(mcqSMDatas.get(i).getChildModuleId());
                            contentItem.setSmChildName(mcqSMDatas.get(i).getChildModuleName());
                            contentItem.setSmChildDesc(mcqSMDatas.get(i).getChildModuleDesc());
                            contentItem.setSmChildimage(mcqSMDatas.get(i).getChildModuleImage());
                            contentItem.setSmChildRating(mcqSMDatas.get(i).getChildModuleRating());
                            contentItem.setStatus(mcqSMDatas.get(i).getStatus());
                            contentItem.setAccessLevel(mcqSMDatas.get(i).getAccessLevel());
                            contentItem.setQnConut(mcqSMDatas.get(i).getQnCount());
                            arrayList.add(contentItem);
                        } else {
                            headId = mcqSMDatas.get(i).getSubModuleId();
                            headName = mcqSMDatas.get(i).getSubModuleName();
                            header.setId(headId);
                            header.setName(headName);

                            arrayList.add(header);
                            contentItem.setSmChildId(mcqSMDatas.get(i).getChildModuleId());
                            contentItem.setSmChildName(mcqSMDatas.get(i).getChildModuleName());
                            contentItem.setSmChildDesc(mcqSMDatas.get(i).getChildModuleDesc());
                            contentItem.setSmChildimage(mcqSMDatas.get(i).getChildModuleImage());
                            contentItem.setSmChildRating(mcqSMDatas.get(i).getChildModuleRating());
                            contentItem.setStatus(mcqSMDatas.get(i).getStatus());
                            contentItem.setAccessLevel(mcqSMDatas.get(i).getAccessLevel());
                            contentItem.setQnConut(mcqSMDatas.get(i).getQnCount());
                            arrayList.add(contentItem);
                        }
                    }

                }
            }
            /*
            for(int j = 0; j <= 4; j++) {
                Header header = new Header();
                header.setHeader("header"+j);
                arrayList.add(header);
                for (int i = 0; i <= 3; i++) {
                    ContentItem item = new ContentItem();
                    item.setRollnumber(i + "");
                    item.setName("A" + i);
                    arrayList.add(item);
                }
            }*/
            return arrayList;
        }

        private ArrayList<McqSubModleData> updateUserDataInSubModule(ArrayList<McqSubModleData> mcqDataModules, ArrayList<McqUserData> mcqUserData) {
            ArrayList<McqSubModleData> data = mcqDataModules;
            ArrayList<McqUserData> userData = mcqUserData;
            for (int i = 0; i < data.size(); i++) {
                int smChid = data.get(i).getChildModuleId();
                int completedCount = 0;
                for (int j = 0; j < userData.size(); j++) {
                    int umid = userData.get(j).getChildId();
                    String staus = userData.get(j).getStatus();
                    if (smChid == umid && staus.equals("completed")) {
                        completedCount += 1;
                        data.get(i).setStatus("completed");
                    }
                }
            }
            return data;
        }

        protected void onPostExecute(Void result) {
            if (mcqSubModuleDatas.size() > 0) {
                txt_total_completed.setText(dataModule.getCompletedModule() + " of " + dataModule.getTotalModule());
                completedSeekBar.setProgress(dataModule.getCompletedModule());
                completedSeekBar.setMax(dataModule.getTotalModule());
                recyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);

                adapter = new McqSubModuleRecyclerAdapter(getList(mcqSubModuleDatas), userAccessList.get(0).getUpdatedUserAccessLevel());

                recyclerView.setAdapter(adapter);
            } else {
                overridePendingTransition(0, 0);
                onBackPressed();
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MCQTest.class));
    }

    private void getUpdatedUserData() {

        new GetAllUserUpdateData(this).execute();
    }


    public class GetAllUserUpdateData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllUserUpdateData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostUpdatedUserDetailsHandler httpPostHandler = new HttpPostUpdatedUserDetailsHandler();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Updated_User_details);
            if(responseFromDB!=null) {
                Log.e("Grant Test>>", "Grant Test data responseFromDB: " + responseFromDB);
                userAccessList = getUserData(responseFromDB);
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            UserAccessLevelModel.getInstance().setUpdatedUserAccessLevel(userAccessList.get(0).getUpdatedUserAccessLevel());
            UserModel.getInstance().setUserAccessLevel(Integer.parseInt(userAccessList.get(0).getUpdatedUserAccessLevel()));
        }
    }
}