package com.ardent_bds.ui.mcq_test_module;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.McqModuleData;
import com.ardent_bds.model.McqSubModleData;
import com.ardent_bds.model.McqUserData;
import com.ardent_bds.project_utils.HttpPostHandlerMCQTestSubModuleData;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;

public class McqResultActivity extends AppCompatActivity {

    McqSubModleData mcqSubModleData;
    TextView score, scorePercent, review, skip;
    int scoree = 0;
    LinearLayout mcq_result_ll;
    ArrayList<QuestionDataModel> questionsDataModules;
    ArrayList<McqUserData> userData = new ArrayList<>();

    String HttpGetUrl = Constants.BaseUrl + "/ArdentDB/getReviewQuestions.php";
    String HttpURL2 = Constants.BaseUrl + "/ArdentDB/userData.php";

    ImageView circularImageView;
    TextView correct_pgbar1_value;

    TextView moduleName, childName, childDesc, qnCount, startMcq;
    ImageView childImage, close;

    private ProgressBar progressBar1;

    ShimmerFrameLayout shimmer_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_mcq_result);
        mcq_result_ll = findViewById(R.id.mcq_result_ll);
        mcqSubModleData = McqSubModleData.getInstance();
        McqModuleData mcqModuleData = McqModuleData.getInstance();
        shimmer_layout = findViewById(R.id.shimmer_layout);
        shimmer_layout.startShimmerAnimation();
        moduleName = findViewById(R.id.mcq_module_name);
        correct_pgbar1_value = findViewById(R.id.correct_pgbar1_value);
        progressBar1 = findViewById(R.id.progressBar1);
        childName = findViewById(R.id.child_name);
        childDesc = findViewById(R.id.child_desc);
        childImage = findViewById(R.id.child_image);
        close = findViewById(R.id.close);
        circularImageView = findViewById(R.id.score_percentage);


        moduleName.setText(mcqModuleData.getModuleName());
        childName.setText(mcqSubModleData.getChildModuleName());
        childDesc.setText(mcqSubModleData.getChildModuleDesc());

        score = findViewById(R.id.score);
        scorePercent = findViewById(R.id.score_Percent);
        review = findViewById(R.id.review);
        skip = findViewById(R.id.skip);

        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ReviewTestActivity.class);
                startActivity(intent);
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), McqSubModuleActivity.class);
                startActivity(intent);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), McqSubModuleActivity.class);
                startActivity(intent);
            }
        });

        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {

            getAllMCQQuestions();
        } else {
            Toast.makeText(McqResultActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setPieChart() {

        //progressBar1.setSecondaryProgress(Integer.parseInt(userData.get(0).getChildTestScorePercentage()));






      /*  //pieChart = findViewById(R.id.pie_chart);


        //pieChart.setRotationEnabled(false);

        //Log.d("Data Set", "addDataSet: ");
        ArrayList<PieEntry> yEntrys = new ArrayList<>();

        yEntrys.add(new PieEntry(Float.parseFloat(String.valueOf(scoree)), " "));
        yEntrys.add(new PieEntry(Float.parseFloat(String.valueOf(questionsDataModules.size())), " "));
        PieDataSet pieDataSet = new PieDataSet(yEntrys, " ");


        //pieDataSet.setSliceSpace(2);
        //pieDataSet.setValueTextSize(12);
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(getResources().getColor(R.color.green));
        colors.add(getResources().getColor(R.color.vedantu_orange));*/
/*
        ArrayList<Integer> colors1 = new ArrayList<>();
        colors1.add(getResources().getColor(R.color.blue));
        colors1.add(getResources().getColor(R.color.vedantu_orange));

        ArrayList<Integer> colors2 = new ArrayList<>();
        colors2.add(getResources().getColor(R.color.red));
        colors2.add(getResources().getColor(R.color.vedantu_orange));*/

/*

        pieDataSet.setColors(colors);
        PieData pieData = new PieData(pieDataSet);


        pieChart.setData(pieData);
        pieChart.setUsePercentValues(false);
        pieChart.getLegend().setEnabled(false);
        pieChart.setDrawSliceText(false);
        pieChart.setDrawEntryLabels(false);

        pieChart.getLegend().setEnabled(false);
        pieChart.setDrawSliceText(false);
        pieChart.setDrawEntryLabels(false);
        pieChart.setDrawMarkers(false);
        pieChart.setDrawMarkers(false);
        pieChart.setCenterText("");
        pieChart.invalidate();
*/
      /*  pieDataSet.setColors(colors);
        PieData pieData = new PieData(pieDataSet);
        pieChart.setData(pieData);
        pieChart.setUsePercentValues(false);
        pieChart.setCenterText("" + scoree);
        pieChart.setCenterTextSize(16);
        pieChart.setCenterTextColor(Color.RED);
        pieChart.getLegend().setEnabled(false);
        pieChart.setDrawSliceText(false);
        pieChart.setDrawEntryLabels(false);
        pieChart.setDrawMarkers(false);
        pieChart.invalidate();*/
    }


    private void getAllMCQQuestions() {
        new GetMCQQuestionsFromServer(getApplicationContext()).execute();

    }


    public class GetMCQQuestionsFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;

        public GetMCQQuestionsFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandlerMCQResult httpHandlerMCQResult = new HttpHandlerMCQResult();
            String userResponseFromDB = httpHandlerMCQResult.makeReviewMCQQuestionsServicePostCall(HttpURL2);

            if (userResponseFromDB != null) {
                HttpPostHandlerMCQTestSubModuleData httpPostHandler = new HttpPostHandlerMCQTestSubModuleData();
                String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetUrl);
                Log.e("Questions data ", "Question responseFromDB: " + responseFromDB);


                userData = BaseActivity.getMcqUserDetailsList(userResponseFromDB);
                /*            userData = McqUserData.getInstance();*/
                questionsDataModules = QuestionDataModel.getArrayInstance();
                questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            }
            return null;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        protected void onPostExecute(Void result) {
            //  Toast.makeText(context, "sd" + questionsDataModules.size(), Toast.LENGTH_SHORT).show();
            for (int i = 0; i < userData.size(); i++) {
                if (userData.get(i).getChildId() == mcqSubModleData.getChildModuleId()) {
                    scoree = Integer.parseInt(userData.get(i).getChildTestScore());
                    score.setText(userData.get(i).getChildTestScore() + " / " + questionsDataModules.size());
                    scorePercent.setText(userData.get(i).getChildTestScorePercentage());
                    //setPieChart();

                    correct_pgbar1_value.setText("" + scoree);
                    circularImageBar(circularImageView, Integer.parseInt(userData.get(i).getChildTestScorePercentage()), scoree);
                    shimmer_layout.stopShimmerAnimation();
                    shimmer_layout.setVisibility(View.GONE);
                    mcq_result_ll.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        //    overridePendingTransition(0, 0);

//        startActivity(new Intent(McqResultActivity.this, MCQTest.class));
    }

    private void circularImageBar(ImageView iv2, int i, int score) {

        Bitmap b = Bitmap.createBitmap(300, 300, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
        Paint paint = new Paint();

        paint.setColor(getResources().getColor(R.color.vedantu_orange));
        paint.setStrokeWidth(20);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(150, 150, 140, paint);
        paint.setColor(getResources().getColor(R.color.green));
        paint.setStrokeWidth(20);
        paint.setStyle(Paint.Style.FILL);
        final RectF oval = new RectF();
        paint.setStyle(Paint.Style.STROKE);
        oval.set(10, 10, 290, 290);


        canvas.drawArc(oval, 270, ((i * 360) / 100), false, paint);


        paint.setStrokeWidth(20);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(getResources().getColor(R.color.vedantu_orange));
        paint.setTextSize(140);
        //canvas.drawText(String.valueOf(i), 150, 150 + (paint.getTextSize()), paint);
        iv2.setImageBitmap(b);

    }


}