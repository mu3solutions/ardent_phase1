package com.ardent_bds.ui;

public class GrantTestUserDataModel {

    private String userID;
    private String userTestId;

    public String getExpiryTest() {
        return expiryTest;
    }

    public void setExpiryTest(String expiryTest) {
        this.expiryTest = expiryTest;
    }

    private String expiryTest;

    private static GrantTestUserDataModel instance = null;

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    private String percentage;

    public String getSelectedAnswerByUser() {
        return selectedAnswerByUser;
    }

    public void setSelectedAnswerByUser(String selectedAnswerByUser) {
        this.selectedAnswerByUser = selectedAnswerByUser;
    }

    private String selectedAnswerByUser;

    public String getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(String finalScore) {
        this.finalScore = finalScore;
    }

    String finalScore;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserTestId() {
        return userTestId;
    }

    public void setUserTestId(String userTestId) {
        this.userTestId = userTestId;
    }

    public String getCompletedDetails() {
        return completedDetails;
    }

    public void setCompletedDetails(String completedDetails) {
        this.completedDetails = completedDetails;
    }

    public String getMarksTotal() {
        return marksTotal;
    }

    public void setMarksTotal(String marksTotal) {
        this.marksTotal = marksTotal;
    }

    public String getCurrentRank() {
        return currentRank;
    }

    public void setCurrentRank(String currentRank) {
        this.currentRank = currentRank;
    }

    public String getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(String correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public String getWrongAnswers() {
        return wrongAnswers;
    }

    public void setWrongAnswers(String wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }

    public String getSkippedAnswers() {
        return skippedAnswers;
    }

    public void setSkippedAnswers(String skippedAnswers) {
        this.skippedAnswers = skippedAnswers;
    }

    String completedDetails;
    String marksTotal;
    String currentRank;
    String correctAnswers;
    String wrongAnswers;
    String skippedAnswers;

    public String getTestTakenOn() {
        return testTakenOn;
    }

    public void setTestTakenOn(String testTakenOn) {
        this.testTakenOn = testTakenOn;
    }

    String testTakenOn;



    private int correctCount;

    public int getCorrectCount() {
        return correctCount;
    }

    public void setCorrectCount(int correctCount) {
        this.correctCount = correctCount;
    }

    public int getWrongCount() {
        return wrongCount;
    }

    public void setWrongCount(int wrongCount) {
        this.wrongCount = wrongCount;
    }

    public int getSkipCount() {
        return skipCount;
    }

    public void setSkipCount(int skipCount) {
        this.skipCount = skipCount;
    }

    private int wrongCount;
    private int skipCount;

    public String getGtName() {
        return gtName;
    }

    public void setGtName(String gtName) {
        this.gtName = gtName;
    }

    private String gtName;

    public String getActualCorrectString() {
        return actualCorrectString;
    }

    public void setActualCorrectString(String actualCorrectString) {
        this.actualCorrectString = actualCorrectString;
    }

    private String actualCorrectString;


    public static GrantTestUserDataModel getInstance() {
        if (instance == null) {
            instance = new GrantTestUserDataModel();
        }
        return instance;
    }
}
