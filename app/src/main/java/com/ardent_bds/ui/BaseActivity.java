package com.ardent_bds.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.McqModuleData;
import com.ardent_bds.model.McqSubModleData;
import com.ardent_bds.model.McqUserData;
import com.ardent_bds.model.StatesAndCollegeModule;
import com.ardent_bds.model.StatusDataModel;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.model.VedioSubDataModule;
import com.ardent_bds.model.VedioUserData;
import com.ardent_bds.ui.LiveTest.HttpPostUpdatedUserDetailsHandler;
import com.ardent_bds.ui.home.GrantTestDataModel;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class BaseActivity extends AppCompatActivity {

    static McqModuleData mcqDataModule = McqModuleData.getInstance();
    static ArrayList<McqModuleData> mcqDataModules = new ArrayList<>();

    static VedioDataModule vedioDataModule = VedioDataModule.getInstance();


    static VedioUrlModule VedioUrlModules = VedioUrlModule.getInstance();
    static StatusDataModel statusDataModel = StatusDataModel.getInstance();
    static GrantTestDataModel grantTestDataModel = GrantTestDataModel.getInstance();
    static GrantTestUserDataModel grantTestUserDataModel = GrantTestUserDataModel.getInstance();
    static UserAccessLevelModel accessLevelModel = UserAccessLevelModel.getInstance();


    static QuestionDataModel questionDataModel = QuestionDataModel.getInstance();
    static GrantTestDataModel grantTestDataModelGrand = GrantTestDataModel.getInstance();

    static VedioSubDataModule vedioSubDataModule = VedioSubDataModule.getInstance();

    static ArrayList<VedioDataModule> vedioDataModules = new ArrayList<>();
    static ArrayList<VedioSubDataModule> vedioDataSubModules = new ArrayList<>();

    static ArrayList<VedioUrlModule> VedioUrlModuleArrayList = new ArrayList<>();
    static ArrayList<StatusDataModel> DashboardModuleArrayList = new ArrayList<>();
    static ArrayList<GrantTestDataModel> GrantTestModuleArrayList = new ArrayList<>();
    static ArrayList<GrantTestUserDataModel> GrantTestUserArrayList = new ArrayList<>();
    static ArrayList<GrantTestUserDataModel> GrantTestUserArrayListAllUsersList = new ArrayList<>();
    static ArrayList<GrantTestUserDataModel> GrantTestUserArrayIndividualList = new ArrayList<>();


    static ArrayList<UserAccessLevelModel> UserAccessLevelModuleArrayList = new ArrayList<>();


    static ArrayList<QuestionDataModel> MCQQuestionsModuleArrayList = new ArrayList<>();
    static ArrayList<GrantTestDataModel> GranrTestQuestionsModuleArrayList = new ArrayList<>();

    ArrayList<UserAccessLevelModel> userAccessList = new ArrayList<>();

    String HttpGetURL_Updated_User_details = Constants.BaseUrl + "/ArdentDB/getUpdatedUserDetails.php";

    public void hideSystemUI() {
        View viewDecor = getWindow().getDecorView();
        viewDecor.setSystemUiVisibility(
                viewDecor.SYSTEM_UI_FLAG_IMMERSIVE |
                        viewDecor.SYSTEM_UI_FLAG_FULLSCREEN |
                        viewDecor.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        viewDecor.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        viewDecor.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        viewDecor.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public void showSystemUI() {
        View viewDecors = getWindow().getDecorView();
        viewDecors.setSystemUiVisibility(
                viewDecors.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                        viewDecors.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                        viewDecors.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
    }


    public String loadStateListFromAssets() {
        String json = null;
        try {
            InputStream is = getAssets().open("state_list.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public ArrayList<String> getStateList() {
        ArrayList<String> formList = new ArrayList<String>();
        //get All State list
        try {
            JSONObject obj = new JSONObject(loadStateListFromAssets());
            JSONArray m_jArry = obj.getJSONArray("allstate");
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Log.d("get All  State ", jo_inside.getString("name"));
                String state_code = jo_inside.getString("code");
                String state_name = jo_inside.getString("name");
                formList.add(state_name);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return formList;
    }

    public ArrayList<StatesAndCollegeModule> getCollageAndStateList(String stateName) {
        ArrayList<StatesAndCollegeModule> collegeList = new ArrayList<>();
        HashMap<String, String> allCollegeStateList = new HashMap<>();
        try {
            //filer College based on State or State Code
            JSONObject json = new JSONObject(loadStateListFromAssets());
            JSONArray jData = json.getJSONArray("allstate");
            for (int i = 0; i < jData.length(); i++) {
                JSONObject jo = jData.getJSONObject(i);
                if ((jo.getString("name")).startsWith(stateName, 0)) {
                    Log.i("Name is ", jo.getString("name"));
                    JSONArray array = jo.getJSONArray("c_name");
                    for (int j = 0; j < array.length(); j++) {
                        JSONObject obj = array.getJSONObject(j);
                        StatesAndCollegeModule module = new StatesAndCollegeModule();
                        module.setId(j + 1);
                        module.setStatecode(jo.getString("code"));
                        module.setStateName(jo.getString("name"));
                        module.setCollegeName(obj.getString("colle_id"));
                        collegeList.add(module);
                        //allCollegeStateList.put(obj.getString("colle_id"), jo.getString("name"));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return collegeList;
    }


    public static ArrayList<VedioDataModule> getVideoDetailsList(String jsonstring) {
        ArrayList<VedioDataModule> formList = new ArrayList<VedioDataModule>();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("My App", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("video_module");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    //Log.d("get All  State ", jo_inside.getString("name"));
                    String moduleId = jo_inside.getString("id");
                    String video_module_name = jo_inside.getString("name");
                    String image_icon = jo_inside.getString("icons");
                    String desc = jo_inside.getString("desc");
                    String total_modules = jo_inside.getString("totalsubmodules");
                    String mvtotalcompletedmodules = jo_inside.getString("mvtotalcompletedmodules");
                    String mv_video_total_time = jo_inside.getString("module_video_time");

                    vedioDataModule = new VedioDataModule();
                    vedioDataModule.setModuleId(Integer.parseInt(moduleId));
                    vedioDataModule.setModuleName(video_module_name);
                    vedioDataModule.setModuleIcons(image_icon);
                    vedioDataModule.setHeading("Heading");
                    vedioDataModule.setDesc(desc);
                    vedioDataModule.setCompletedVedio(mvtotalcompletedmodules);
                    vedioDataModule.setTime(mv_video_total_time + "Hrs");
                    vedioDataModule.setTotalVedio(total_modules);
                    vedioDataModules.add(vedioDataModule);
                    formList.add(vedioDataModules.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return formList;
    }

    public static ArrayList<VedioUserData> getVideoUserDetailsList(String jsonstring) {
        ArrayList<VedioUserData> formList = VedioUserData.getInstance();
        formList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("My App", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("video_user_data");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    //Log.d("get All  State ", jo_inside.getString("user_id"));
                    String vedioModuleId = jo_inside.getString("vediomoduleid");
                    String vedioSubModuleId = jo_inside.getString("vediosubmoduleid");
                    String vedioReturnTime = jo_inside.getString("vedioreturntime");
                    String status = jo_inside.getString("vediostatus");

                    VedioUserData vedioUserData = new VedioUserData();
                    vedioUserData.setVedioModuleId(Integer.parseInt(vedioModuleId));
                    vedioUserData.setVedioChildId(Integer.parseInt(vedioSubModuleId));
                    vedioUserData.setReturnTime(vedioReturnTime);
                    vedioUserData.setStatus(Integer.parseInt(status));
                    formList.add(vedioUserData);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return formList;
    }

    public static ArrayList<VedioSubDataModule> getSubModuleDetails(String jsonstring) {
        ArrayList<VedioSubDataModule> formLists = new ArrayList<VedioSubDataModule>();
        //vedioDataSubModules.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("My App", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("video_sub_module");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    //Log.d("get All  State ", jo_inside.getString("smv_child_id"));
                    String mvid = jo_inside.getString("vediomoduleid");
                    String subModuleId = jo_inside.getString("vediosubmoduleid");
                    String subModuleName = jo_inside.getString("vediosubmodulename");
                    String childId = jo_inside.getString("vediosubmoduleChildid");
                    String childName = jo_inside.getString("vediosubmoduleChildname");
                    String childImage = jo_inside.getString("vediothumbimage");
                    String childDescription = jo_inside.getString("vediodesc");
                    String rating = jo_inside.getString("rating");
                    String accesslevel = jo_inside.getString("accesslevel");


                    String smv_video_id = jo_inside.getString("smvvedioid");
                    String smv_slide_id = jo_inside.getString("smvslideid");
                    String smv_note_id = jo_inside.getString("smvnoteid");
                    String video_length = jo_inside.getString("vediolength");
                    String status = "not defined";

                    vedioSubDataModule = new VedioSubDataModule();
                    vedioSubDataModule.setModuleVid(Integer.parseInt(mvid));
                    vedioSubDataModule.setSubModuleVid(Integer.parseInt(subModuleId));
                    vedioSubDataModule.setSubModuleName(subModuleName);
                    vedioSubDataModule.setChildID(Integer.parseInt(childId));
                    vedioSubDataModule.setChildName(""+childName);
                    vedioSubDataModule.setChildImage(""+childImage);
                    vedioSubDataModule.setChildDesc(""+childDescription);
                    vedioSubDataModule.setTime("" + video_length + "Mins");
                    vedioSubDataModule.setRating(""+rating);
                    vedioSubDataModule.setAccesslevel(Integer.parseInt(accesslevel));
                    vedioSubDataModule.setSmvVedioId(""+smv_video_id);
                    vedioSubDataModule.setSmvSlideId(""+smv_slide_id);
                    vedioSubDataModule.setSmvNoteId(""+smv_note_id);
                    vedioSubDataModule.setStatus(0);
                    vedioSubDataModule.setTotalCount(m_jArry.length());
                    formLists.add(vedioSubDataModule);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return formLists;
    }


    public static ArrayList<VedioUrlModule> getVideoListFromChildID(String jsonstring) {
        ArrayList<VedioUrlModule> formListsVideoList = new ArrayList<VedioUrlModule>();

        VedioUrlModuleArrayList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Videourl from child id", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("video_details_list");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("get All  State ", jo_inside.getString("uv_id"));
                    String uv_id_child_video_url = jo_inside.getString("uv_id");
                    String videourl = jo_inside.getString("videourl");
                    String slideurl = jo_inside.getString("slide_url");
                    String module_name_title = jo_inside.getString("module_name");
                    String video_description = jo_inside.getString("video_description");
                    String pdf_url = jo_inside.getString("pdf_url");

                    // String noteurl = jo_inside.getString("note_url");

                    VedioUrlModules = new VedioUrlModule();
                    VedioUrlModules.setVideoId("" + uv_id_child_video_url);
                    VedioUrlModules.setVideoUrl("" + videourl);
                    VedioUrlModules.setNoteId("");
                    VedioUrlModules.setNoteUrl("");
                    VedioUrlModules.setSlideId("");
                    VedioUrlModules.setSlideUrl(slideurl);
                    VedioUrlModules.setModuleName(module_name_title);
                    VedioUrlModules.setVideoDescription("" + video_description);
                    VedioUrlModules.setPdfUrl("" + pdf_url);
                    VedioUrlModuleArrayList.add(VedioUrlModules);
                    formListsVideoList.add(VedioUrlModuleArrayList.get(i));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return formListsVideoList;
    }


    public static ArrayList<StatusDataModel> getDashboardImagesData(String jsonstring) {
        ArrayList<StatusDataModel> dashboardImagesList = new ArrayList<StatusDataModel>();

        DashboardModuleArrayList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Dashboard Data", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("dashboard_details_list");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("dashBoard images", jo_inside.getString("db_top_slider_images"));
                    String db_top_slider_images = jo_inside.getString("db_top_slider_images");
                    String db_nuggets = jo_inside.getString("db_nuggets");
                    String db_suggested_test = jo_inside.getString("db_suggested_test");
                    String db_suggested_videos = jo_inside.getString("db_suggested_videos");
                    String db_nugets_txt = jo_inside.getString("db_nugets_txt");
                    String db_sug_videos_txt = jo_inside.getString("db_sug_videos_txt");
                    String nug_5_mins_video = jo_inside.getString("nug_5_mins_video");
                    String nuggets_text_content = jo_inside.getString("nuggets_text_content");
                    String suggested_concept_videos = jo_inside.getString("nug_concept_videos");
                    String nuggets_inner_slider = jo_inside.getString("nuggets_inner_slider");

                    // String noteurl = jo_inside.getString("note_url");

                    statusDataModel = new StatusDataModel();
                    statusDataModel.setSliderImageList("" + db_top_slider_images);
                    statusDataModel.setImage("" + db_nuggets);
                    statusDataModel.setName("" + db_nugets_txt);
                    statusDataModel.setNug_5_mins_video("" + nug_5_mins_video);
                    statusDataModel.setNug_concepts_video("" + suggested_concept_videos);
                    statusDataModel.setNug_5_mins_txt("" + nuggets_text_content);


                    statusDataModel.setNuggetsSliderImageList(nuggets_inner_slider);
                    statusDataModel.setDb_suggested_videos("" + db_suggested_videos);
                    statusDataModel.setDb_suggested_videos_txt("" + db_sug_videos_txt);
                    /*statusDataModel.setDb_suggested_test("" + db_suggested_test);

                    statusDataModel.setDb_nuggets_txt("" + db_nugets_txt);*/


                    DashboardModuleArrayList.add(statusDataModel);
                    dashboardImagesList.add(DashboardModuleArrayList.get(i));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return dashboardImagesList;
    }

    public static ArrayList<GrantTestDataModel> getDashboardGrantTestData(String jsonstring) {
        ArrayList<GrantTestDataModel> dashboardGrantTestList = new ArrayList<GrantTestDataModel>();

        GrantTestModuleArrayList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Dashboard Data", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("dashboard_grand_test");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("gt_test_name>>", jo_inside.getString("gt_test_name"));

                    int gt_test_id = jo_inside.getInt("testid");
                    String gt_test_name = jo_inside.getString("gt_test_name");
                    String gt_icon = jo_inside.getString("gt_icon");
                    String gt_expiry_date = jo_inside.getString("gt_expiry_date");
                    String gt_total_questions = jo_inside.getString("gt_total_questions");
                    String gt_total_time = jo_inside.getString("gt_total_time");
                    int gt_completed_status = jo_inside.getInt("completed_status");
                    int total_correct_answers = jo_inside.getInt("total_correct_answers");
                    String completed_answer_list = jo_inside.getString("completed_answer_list");
                    String completed_selected_answer_list = jo_inside.getString("completed_selected_answer_list");
                    String completed_skipped_list = jo_inside.getString("total_skipped_data");
                    //int access_level = jo_inside.getInt("access_level");

                    grantTestDataModel = new GrantTestDataModel();

                    grantTestDataModel.setTestId(gt_test_id);
                    grantTestDataModel.setSelecetdtestId(gt_test_id);
                    grantTestDataModel.setTestName("" + gt_test_name);
                    grantTestDataModel.setTesticon("" + gt_icon);
                    grantTestDataModel.setTestexpiryDate("" + gt_expiry_date);
                    grantTestDataModel.setTestTotalQuestions("" + gt_total_questions);
                    grantTestDataModel.setTestTotalTiming("" + gt_total_time);
                    grantTestDataModel.setUserCompletedTestStatus(gt_completed_status);
                    //grantTestDataModel.setAccessLevel(access_level);
                    grantTestDataModel.setSkippedListCompletedGrandTest(Integer.parseInt(completed_skipped_list));


                    grantTestDataModel.setTotalCorrectAnswers(total_correct_answers);
                    grantTestDataModel.setCorrectAnswerMap(completed_answer_list);
                    grantTestDataModel.setUserSelectedAnswerMap(completed_selected_answer_list);

                    GrantTestModuleArrayList.add(grantTestDataModel);
                    dashboardGrantTestList.add(GrantTestModuleArrayList.get(i));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return dashboardGrantTestList;
    }


    public static ArrayList<GrantTestUserDataModel> getGrandTestCompletedBasedOnUser(String jsonstring) {
        ArrayList<GrantTestUserDataModel> grantTestUserBasedList = new ArrayList<GrantTestUserDataModel>();

        GrantTestUserArrayIndividualList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Dashboard Data", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("user_grand_test");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("user_id>>", jo_inside.getString("user_id"));


                    int user_id = jo_inside.getInt("user_id");
                    int gt_test_id = jo_inside.getInt("test_id");
                    String uta_score_percentage = jo_inside.getString("uta_score_percentage");
                    String uta_time_stamp = jo_inside.getString("uta_time_stamp");
                    String grand_test_status = jo_inside.getString("grand_test_status");
                    String qn_selected_answer = jo_inside.getString("qn_selected_answer");
                    String gt_skipped_data = jo_inside.getString("gt_skipped_data");

                    String gt_wrong_answers = jo_inside.getString("gt_wrong_answers");
                    String gt_correct_answer = jo_inside.getString("gt_correct_answer");
                    String user_final_score = jo_inside.getString("user_final_score");
                    String user_rank = jo_inside.getString("user_rank");


                    String user_correct_count = jo_inside.getString("total_correct_count");
                    String user_wrong_count = jo_inside.getString("toital_wrong_count");
                    String user_skipped_count = jo_inside.getString("total_skip_count");

                    grantTestUserDataModel = new GrantTestUserDataModel();

                    grantTestUserDataModel.setUserID("" + user_id);
                    grantTestUserDataModel.setUserTestId("" + gt_test_id);
                    grantTestUserDataModel.setCompletedDetails("" + grand_test_status);
                    grantTestUserDataModel.setCorrectAnswers("" + gt_correct_answer);
                    grantTestUserDataModel.setTestTakenOn(uta_time_stamp);
                    grantTestUserDataModel.setCurrentRank("" + user_rank);
                    grantTestUserDataModel.setTestTakenOn("" + uta_time_stamp);
                    grantTestUserDataModel.setMarksTotal("" + user_final_score);
                    grantTestUserDataModel.setFinalScore("" + user_final_score);
                    grantTestUserDataModel.setSkippedAnswers(gt_skipped_data);
                    grantTestUserDataModel.setPercentage(uta_score_percentage);
                    grantTestUserDataModel.setSelectedAnswerByUser(qn_selected_answer);
                    grantTestUserDataModel.setWrongAnswers(gt_wrong_answers);

                    grantTestUserDataModel.setCorrectCount(Integer.parseInt(user_correct_count));
                    grantTestUserDataModel.setSkipCount(Integer.parseInt(user_skipped_count));
                    grantTestUserDataModel.setWrongCount(Integer.parseInt(user_wrong_count));
                    GrantTestUserArrayIndividualList.add(grantTestUserDataModel);
                    //grantTestUserBasedList.add(GrantTestUserArrayList.get(i));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return GrantTestUserArrayIndividualList;
    }


    public static ArrayList<GrantTestUserDataModel> getAllUsersGTData(String jsonstring) {
        ArrayList<GrantTestUserDataModel> grantTestAllUserList = new ArrayList<GrantTestUserDataModel>();

        GrantTestUserArrayListAllUsersList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Dashboard Data", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("user_grand_test");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("user_id>>", jo_inside.getString("user_id"));


                    int user_id = jo_inside.getInt("user_id");
                    int gt_test_id = jo_inside.getInt("test_id");
                    String uta_score_percentage = jo_inside.getString("uta_score_percentage");
                    String uta_time_stamp = jo_inside.getString("uta_time_stamp");
                    String grand_test_status = jo_inside.getString("grand_test_status");
                    String qn_selected_answer = jo_inside.getString("qn_selected_answer");
                    String gt_skipped_data = jo_inside.getString("gt_skipped_data");

                    String gt_wrong_answers = jo_inside.getString("gt_wrong_answers");
                    String gt_correct_answer = jo_inside.getString("gt_correct_answer");
                    String user_final_score = jo_inside.getString("user_final_score");
                    String user_rank = jo_inside.getString("user_rank");


                    String user_correct_count = jo_inside.getString("total_correct_count");
                    String user_wrong_count = jo_inside.getString("toital_wrong_count");
                    String user_skipped_count = jo_inside.getString("total_skip_count");

                    grantTestUserDataModel = new GrantTestUserDataModel();

                    grantTestUserDataModel.setUserID("" + user_id);
                    grantTestUserDataModel.setUserTestId("" + gt_test_id);
                    grantTestUserDataModel.setCompletedDetails("" + grand_test_status);
                    grantTestUserDataModel.setCorrectAnswers("" + gt_correct_answer);
                    grantTestUserDataModel.setCurrentRank("" + user_rank);
                    grantTestUserDataModel.setTestTakenOn(uta_time_stamp);
                    grantTestUserDataModel.setMarksTotal("" + user_final_score);
                    grantTestUserDataModel.setFinalScore("" + user_final_score);
                    grantTestUserDataModel.setSkippedAnswers("" + gt_skipped_data);
                    grantTestUserDataModel.setPercentage("" + uta_score_percentage);
                    grantTestUserDataModel.setSelectedAnswerByUser(qn_selected_answer);
                    grantTestUserDataModel.setWrongAnswers(gt_wrong_answers);

                    grantTestUserDataModel.setCorrectCount(Integer.parseInt(user_correct_count));
                    grantTestUserDataModel.setSkipCount(Integer.parseInt(user_skipped_count));
                    grantTestUserDataModel.setWrongCount(Integer.parseInt(user_wrong_count));
                    GrantTestUserArrayListAllUsersList.add(grantTestUserDataModel);
                    // grantTestAllUserList.add(GrantTestUserArrayListAllUsersList.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return GrantTestUserArrayListAllUsersList;
    }


    public static ArrayList<UserAccessLevelModel> getUserData(String jsonstring) {
        ArrayList<UserAccessLevelModel> userlevelAccessArrayList = new ArrayList<UserAccessLevelModel>();

        userlevelAccessArrayList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Dashboard Data", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("updated_user_details");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("user_access_level>>", jo_inside.getString("user_access_level"));
                    String gt_access_level = jo_inside.getString("user_access_level");
                    accessLevelModel.setUpdatedUserAccessLevel(gt_access_level);
                    UserAccessLevelModuleArrayList.add(accessLevelModel);
                    userlevelAccessArrayList.add(UserAccessLevelModuleArrayList.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userlevelAccessArrayList;
    }


    public static ArrayList<QuestionDataModel> getMCQQuestions(String jsonstring) {
        ArrayList<QuestionDataModel> questionDataList = QuestionDataModel.getArrayInstance();
        questionDataList.clear();
        MCQQuestionsModuleArrayList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Dashboard Data", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("mcq_questions_list");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("mcq_qn_id>>", jo_inside.getString("mcq_qn_id"));

                    int mcq_qn_id = jo_inside.getInt("mcq_qn_id");
                    int mcq_child_id = jo_inside.getInt("mcq_child_id");
                    String mcq_qn_desc = jo_inside.getString("mcq_qn_desc");
                    String mcq_answers = jo_inside.getString("mcq_answers");
                    int mcq_qn_correct_answer_num = jo_inside.getInt("mcq_qn_correct_answer_num");
                    String mcq_answer_desc = jo_inside.getString("mcq_answer_desc");
                    String mcq_answer_image = jo_inside.getString("mcq_answer_desc_images");

                    questionDataModel = new QuestionDataModel();
                    questionDataModel.setQuestionId(mcq_qn_id);
                    questionDataModel.setChildId(mcq_child_id);
                    questionDataModel.setMcqQuestionDesc("" + mcq_qn_desc);
                    questionDataModel.setMcqAnswerList("" + mcq_answers);
                    questionDataModel.setMcqCorrectAnswer(mcq_qn_correct_answer_num);
                    questionDataModel.setMcqAnswerDesc("" + mcq_answer_desc);
                    questionDataModel.setMcqAnswerImage("" + mcq_answer_image);

                    MCQQuestionsModuleArrayList.add(questionDataModel);
                    questionDataList.add(MCQQuestionsModuleArrayList.get(i));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return questionDataList;
    }


    public static ArrayList<GrantTestDataModel> getGrandTestQuestions(String jsonstring) {
        ArrayList<GrantTestDataModel> questionDataList = GrantTestDataModel.getArrayInstance();
        questionDataList.clear();
        GranrTestQuestionsModuleArrayList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Dashboard Data", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("mcq_questions_list");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("mcq_qn_id>>", jo_inside.getString("mcq_qn_id"));

                    int mcq_qn_id = jo_inside.getInt("mcq_qn_id");
                    int mcq_child_id = jo_inside.getInt("mcq_child_id");
                    String mcq_qn_desc = jo_inside.getString("mcq_qn_desc");
                    String mcq_answers = jo_inside.getString("mcq_answers");
                    int mcq_qn_correct_answer_num = jo_inside.getInt("mcq_qn_correct_answer_num");
                    String mcq_answer_desc = jo_inside.getString("mcq_answer_desc");
                    //String mcq_answer_image = jo_inside.getString("mcq_answer_desc_images");

                    grantTestDataModelGrand = new GrantTestDataModel();
                    grantTestDataModelGrand.setQuestionGTId(mcq_qn_id);
                    grantTestDataModelGrand.setChildGTId(mcq_child_id);
                    grantTestDataModelGrand.setGTQuestionDesc("" + mcq_qn_desc);
                    grantTestDataModelGrand.setGTAnswerList("" + mcq_answers);
                    grantTestDataModelGrand.setGTCorrectAmswer("" + mcq_qn_correct_answer_num);
                    grantTestDataModelGrand.setGTAnswerDesc("" + mcq_answer_desc);
                    //grantTestDataModelGrand.setGTImageList("" + mcq_answer_image);

                    GranrTestQuestionsModuleArrayList.add(grantTestDataModelGrand);
                    questionDataList.add(GranrTestQuestionsModuleArrayList.get(i));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return questionDataList;
    }


    //bala code

    public static ArrayList<McqModuleData> getMcqDetailsList(String jsonstring) {
        ArrayList<McqModuleData> formList = new ArrayList<McqModuleData>();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("My App", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("mcqModule");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("get All  State ", jo_inside.getString("mcq_mname"));
                    String moduleId = jo_inside.getString("mcq_mid");
                    String module_icon = jo_inside.getString("mcq_micon");
                    String module_name = jo_inside.getString("mcq_mname");
                    String total_modules = jo_inside.getString("mcq_total_submodule");
                    String completedmodules = jo_inside.getString("mcq_completed_submodule");


                    mcqDataModule = new McqModuleData();
                    mcqDataModule.setModuleId(Integer.parseInt(moduleId));
                    mcqDataModule.setModuleName(module_name);
                    mcqDataModule.setModuleIcon(module_icon);
                    mcqDataModule.setTotalModule(Integer.parseInt(total_modules));
                    mcqDataModule.setCompletedModule(Integer.parseInt(completedmodules));

                    mcqDataModules.add(mcqDataModule);
                    //formlist and mcqDataModules are same why declare 2 variable

                    formList.add(mcqDataModules.get(i));

                   /* HashMap<String, String> contact = new HashMap<>();


                    // adding each child node to HashMap key => value
                    contact.put("image_icon", image_icon);
                    contact.put("video_module_name", video_module_name);
                    contact.put("total_modules", total_modules);
                    contact.put("mvtotalcompletedmodules", mvtotalcompletedmodules);

                    // adding contact to contact list
                    contactList.add(contact);
*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return formList;
    }

    public static ArrayList<McqUserData> getMcqUserDetailsList(String jsonstring) {
        ArrayList<McqUserData> formList = McqUserData.getInstance();
        formList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("My App", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("all_mcq_result");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("get All  State ", jo_inside.getString("user_id"));
                    String userId = jo_inside.getString("user_id");
                    String moduleId = jo_inside.getString("uta_module_id");
                    String smChildId = jo_inside.getString("uta_sm_child_id");
                    String testScore = jo_inside.getString("uta_child_test_score");
                    String testPercentage = jo_inside.getString("uta_score_percentage");
                    String time = jo_inside.getString("uta_time_stamp");
                    String status = jo_inside.getString("uta_module_status");
                    String qnSelectedAnswer = jo_inside.getString("qn_selected_answer");

                    McqUserData mcqUserData = new McqUserData();
                    mcqUserData.setModuleId(Integer.parseInt(moduleId));
                    mcqUserData.setChildId(Integer.parseInt(smChildId));
                    mcqUserData.setChildTestScore(testScore);
                    mcqUserData.setChildTestScorePercentage(testPercentage);
                    mcqUserData.setStatus(status);
                    mcqUserData.setTime(time);
                    mcqUserData.setQnSelectedAnswer(qnSelectedAnswer);
                    formList.add(mcqUserData);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return formList;
    }


    public static ArrayList<McqSubModleData> getMcqSubModuleDetailsList(String jsonstring) {
        ArrayList<McqSubModleData> formList = new ArrayList<>();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);
            Log.d("My App", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("mcqSubModule");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("get All  State ", jo_inside.getString("mcq_module_id_ref"));
                    String moduleId = jo_inside.getString("mcq_module_id_ref");
                    String subModuleId = jo_inside.getString("mcq_sm_id");
                    String subModuleName = jo_inside.getString("mcq_sm_name");
                    String smChildId = jo_inside.getString("mcq_sm_child_id");
                    String smChildName = jo_inside.getString("mcq_sm_child_name");
                    String smChildImage = jo_inside.getString("mcq_sm_child_image");
                    String smChildDesc = jo_inside.getString("mcq_sm_child_desc");
                    String smChildRating = jo_inside.getString("mcq_sm_child_rating");
                    String smChildAccessLevel = jo_inside.getString("mcq_sm_access_level");
                    String smChildQnArray = jo_inside.getString("mcq_sm_child_qn_array");
                    String smQnCount = jo_inside.getString("mcq_sm_qn_count");
                    String status = "not defined";

                    McqSubModleData mcqSubModleData = new McqSubModleData();
                    mcqSubModleData.setModuleId(Integer.parseInt(moduleId));
                    mcqSubModleData.setSubModuleId(Integer.parseInt(subModuleId));
                    mcqSubModleData.setSubModuleName(subModuleName);
                    mcqSubModleData.setChildModuleId(Integer.parseInt(smChildId));
                    mcqSubModleData.setChildModuleName(smChildName);
                    mcqSubModleData.setChildModuleImage(smChildImage);
                    mcqSubModleData.setChildModuleDesc(smChildDesc);
                    mcqSubModleData.setChildModuleRating(smChildRating);
                    mcqSubModleData.setAccessLevel(Integer.parseInt(smChildAccessLevel));
                    mcqSubModleData.setQnArray(smChildQnArray);
                    mcqSubModleData.setQnCount(Integer.parseInt(smQnCount));
                    mcqSubModleData.setStatus(status);
                    formList.add(mcqSubModleData);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return formList;
    }


/*
    public static ArrayList<QuestionDataModel> getMCQQuestions(String jsonstring) {
        ArrayList<QuestionDataModel> questionDataList = QuestionDataModel.getArrayInstance();
        MCQQuestionsModuleArrayList.clear();
        JSONObject obj = null;
        try {
            obj = new JSONObject(jsonstring);

            Log.d("Dashboard Data", obj.toString());
            //get All State list
            try {
                JSONArray m_jArry = obj.getJSONArray("mcq_questions_list");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    Log.d("mcq_qn_id>>", jo_inside.getString("mcq_qn_id"));

                    int mcq_qn_id = jo_inside.getInt("mcq_qn_id");
                    int mcq_child_id = jo_inside.getInt("mcq_child_id");
                    String mcq_qn_desc = jo_inside.getString("mcq_qn_desc");
                    String mcq_answers = jo_inside.getString("mcq_answers");
                    int mcq_qn_correct_answer_num = jo_inside.getInt("mcq_qn_correct_answer_num");
                    String mcq_answer_desc = jo_inside.getString("mcq_answer_desc");

                    questionDataModel = new QuestionDataModel();
                    questionDataModel.setQuestionId(mcq_qn_id);
                    questionDataModel.setChildId(mcq_child_id);
                    questionDataModel.setMcqQuestionDesc("" + mcq_qn_desc);
                    questionDataModel.setMcqAnswerList("" + mcq_answers);
                    questionDataModel.setMcqCorrectAnswer(mcq_qn_correct_answer_num);
                    questionDataModel.setMcqAnswerDesc("" + mcq_answer_desc);

                    MCQQuestionsModuleArrayList.add(questionDataModel);
                    questionDataList.add(MCQQuestionsModuleArrayList.get(i));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return questionDataList;
    }
*/


    public static class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {
        private String url;
        private ImageView imageView;

        public ImageLoadTask(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
/*
            Toast.makeText(HomeActivity.this, " " + result, Toast.LENGTH_SHORT).show();
*/
            imageView.setImageBitmap(result);
        }

    }


    public void initCollapsingToolbar(final TextView toolText, final ImageView toolImage, final CollapsingToolbarLayout collapsingToolbar, AppBarLayout appBarLayout) {
        collapsingToolbar.setTitle(" nn");
        //collapsingToolbar.setPointerIcon(R.drawable.ic_baseline_arrow_back_24);
        appBarLayout.setExpanded(true);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                //Toast.makeText(AllVideosSlidesNotesActivity.this, "verti"+verticalOffset, Toast.LENGTH_SHORT).show();
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Bala");
                    toolImage.setVisibility(View.GONE);
                    toolText.setVisibility(View.VISIBLE);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle("Bla");
                    toolImage.setVisibility(View.VISIBLE);
                    toolText.setVisibility(View.GONE);
                    isShow = false;
                }
            }
        });
    }


    //custom Alert dialog


    public class GetAllUserUpdateData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllUserUpdateData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostUpdatedUserDetailsHandler httpPostHandler = new HttpPostUpdatedUserDetailsHandler();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Updated_User_details);
            Log.e("Grant Test>>", "Grant Test data responseFromDB1222222222: " + responseFromDB);
            if (responseFromDB != null) {
                userAccessList = getUserData(responseFromDB);
                UserAccessLevelModel.getInstance().setUpdatedUserAccessLevel(userAccessList.get(0).getUpdatedUserAccessLevel());
                UserModel.getInstance().setUserAccessLevel(Integer.parseInt(userAccessList.get(0).getUpdatedUserAccessLevel()));

            }

            return null;
        }

        protected void onPostExecute(Void result) {


        }
    }


}
