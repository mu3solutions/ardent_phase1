package com.ardent_bds;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;

class SlideCustomPagerAdapter extends PagerAdapter {
    Context context;
    String images[];
    LayoutInflater layoutInflater;


    public SlideCustomPagerAdapter(Context context, String images[]) {
        this.context = context;
        this.images = images;
        //layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        //View itemView = LayoutInflater.from(context.inflate(R.layout.suggested_videos_layout, parent, false);
        View itemView = LayoutInflater.from(context).inflate(R.layout.slide_custom_layout, container, false);
        //View itemView = layoutInflater.inflate(R.layout.slide_custom_layout, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.slide_custom_iv);

        //if (images.length > 0) {

            Glide.with(context)
                    .load(images[position])
                    .into(imageView);
            //imageView.setImageResource(Integer.parseInt(images[position]));

            container.addView(itemView);
        //} else {

          //  Toast.makeText(context, "NO images availabe", Toast.LENGTH_SHORT).show();
            //overridePendingTransition(0, 0);
            //onBackPressed();
        //}
        //listening to image click
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();*/
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
