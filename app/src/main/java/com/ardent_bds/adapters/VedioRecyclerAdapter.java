package com.ardent_bds.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.ui.VedioMainActivity;
import com.ardent_bds.ui.gallery.PaymentHistory;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class VedioRecyclerAdapter extends RecyclerView.Adapter<VedioRecyclerAdapter.MyviewHolder> {
    ArrayList<VedioDataModule> vda;
    Context context;

    public VedioRecyclerAdapter(ArrayList<VedioDataModule> vedioDataModules) {
        this.vda = vedioDataModules;
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView heading, desc, bookValue, totalTime, totalModule;
        ImageView image_icon;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);
            heading = itemView.findViewById(R.id.video_heading);
            desc = itemView.findViewById(R.id.video_desc);
            bookValue = itemView.findViewById(R.id.book_val);
            totalTime = itemView.findViewById(R.id.total_time_val);
            image_icon = itemView.findViewById(R.id.image_icon);

        }
    }

    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vedio_module_layout, parent, false);
        context = view.getContext();
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyviewHolder holder, final int position) {
        holder.desc.setText(vda.get(position).getDesc());
        holder.heading.setText(vda.get(position).getModuleName());
        holder.totalTime.setText(vda.get(position).getTime());
        holder.bookValue.setText(vda.get(position).getCompletedVedio() + " / " + vda.get(position).getTotalVedio());

        Glide.with(context)
                .load(vda.get(position).getModuleIcons())
                .into(holder.image_icon);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VedioDataModule vedioDataModule = VedioDataModule.getInstance();
                vedioDataModule.setModuleId(vda.get(position).getModuleId());
                vedioDataModule.setModuleName("" + vda.get(position).getModuleName());
                vedioDataModule.setCompletedVedio("" + vda.get(position).getCompletedVedio());
                vedioDataModule.setTotalVedio("" + vda.get(position).getTotalVedio());
                Intent intent = new Intent(v.getContext(), VedioMainActivity.class);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return vda.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
