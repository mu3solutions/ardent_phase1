package com.ardent_bds.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.model.CoutryStateDataModel;

import java.util.ArrayList;

public class StatesRecylerAdapter extends RecyclerView.Adapter<StatesRecylerAdapter.MyViewHolder> {
    ArrayList<CoutryStateDataModel> states;
    Context context;
    private int selectedPosition = -1;
    public StatesRecylerAdapter(ArrayList<CoutryStateDataModel> states) {
        this.states = states;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView checkbox, checkboxText;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            checkbox = itemView.findViewById(R  .id.checkbox);
            checkboxText = itemView.findViewById(R.id.checkbox_text);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.states_view,parent,false);
        context = view.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        if(selectedPosition == position){
            holder.checkbox.setBackgroundResource(R.color.btton_orange);
            holder.checkboxText.setText(states.get(position).getName());
            states.get(position).setSelected(true);
        }
        else{
            holder.checkbox.setBackgroundResource(R.drawable.check_box);
            holder.checkboxText.setText(states.get(position).getName());
            states.get(position).setSelected(false);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return states.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
