package com.ardent_bds.adapters;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.model.ListItem;
import com.ardent_bds.model.McqSMContentItem;
import com.ardent_bds.model.McqSMHeader;
import com.ardent_bds.model.McqSubModleData;
import com.ardent_bds.ui.gallery.SubscriptionActivity;
import com.ardent_bds.ui.mcq_test_module.McqResultActivity;
import com.ardent_bds.ui.mcq_test_module.McqTestIntroActivity;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class McqSubModuleRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_POS = 2;
    private static final String SAVED_LAYOUT_MANAGER = "5";

    //Header header;
    Toast toast = null;
    List<ListItem> list;

    String user_access_level;

    public McqSubModuleRecyclerAdapter(List<ListItem> headerItems, String user_access_level) {
        this.list = headerItems;
        this.user_access_level = user_access_level;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            View v = inflater.inflate(R.layout.sub_module_header, parent, false);
            return new VHHeader(v);
        } else {
            View v = inflater.inflate(R.layout.sub_module_content_item, parent, false);
            return new VHItem(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            // VHHeader VHheader = (VHHeader)holder;
            McqSMHeader currentItem = (McqSMHeader) list.get(position);
            VHHeader VHheader = (VHHeader) holder;
            VHheader.txtTitle.setText(currentItem.getName());
        } else if (holder instanceof VHItem) {
            final McqSMContentItem currentItem = (McqSMContentItem) list.get(position);
            final VHItem VHitem = (VHItem) holder;
            VHitem.txtName.setText(currentItem.getSmChildName());
            VHitem.mcqCount.setText(currentItem.getQnConut() + " | MCQ");
            if (currentItem.getAccessLevel() == 0) {
                VHitem.lock.setVisibility(View.GONE);
            }
            if (currentItem.getStatus().equals("completed")) {
                VHitem.completed.setVisibility(View.VISIBLE);
            }

            if (user_access_level.equalsIgnoreCase("1") || user_access_level.equalsIgnoreCase("12")
                    || user_access_level.equalsIgnoreCase("21")
                    || user_access_level.equalsIgnoreCase("23") || user_access_level.equalsIgnoreCase("123")
                    || user_access_level.equalsIgnoreCase("13") || user_access_level.equalsIgnoreCase("31")) {
                VHitem.lock.setVisibility(View.GONE);
            }


            VHitem.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    McqSubModleData mcqSubModleData = McqSubModleData.getInstance();
                    mcqSubModleData.setChildModuleDesc(currentItem.getSmChildDesc());
                    mcqSubModleData.setChildModuleId(currentItem.getSmChildId());
                    mcqSubModleData.setChildModuleName(currentItem.getSmChildName());
                    mcqSubModleData.setChildModuleImage(currentItem.getSmChildimage());
                    mcqSubModleData.setQnCount(currentItem.getQnConut());
                    mcqSubModleData.setStatus(currentItem.getStatus());
                    mcqSubModleData.setAccessLevel(currentItem.getAccessLevel());
                    if (mcqSubModleData.getAccessLevel() == 0) {
                        if (mcqSubModleData.getStatus().equals("completed")) {
                            Intent intent = new Intent(v.getContext(), McqResultActivity.class);
                            v.getContext().startActivity(intent);
                        } else {
                            Intent intent = new Intent(v.getContext(), McqTestIntroActivity.class);
                            v.getContext().startActivity(intent);
                        }
                    } else {
                        if (user_access_level.equalsIgnoreCase("1") || user_access_level.equalsIgnoreCase("12")
                                || user_access_level.equalsIgnoreCase("21")
                                || user_access_level.equalsIgnoreCase("23") || user_access_level.equalsIgnoreCase("123")
                                || user_access_level.equalsIgnoreCase("13") || user_access_level.equalsIgnoreCase("31")) {

                            if (mcqSubModleData.getStatus().equals("completed")) {
                                Intent intent = new Intent(v.getContext(), McqResultActivity.class);
                                v.getContext().startActivity(intent);
                            } else {
                                if (mcqSubModleData.getQnCount() != 0) {
                                    Intent intent = new Intent(v.getContext(), McqTestIntroActivity.class);
                                    v.getContext().startActivity(intent);
                                } else {

                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            toast.makeText(v.getContext(), "Test has no Questions. Try again later", Toast.LENGTH_LONG).show();
                                        }
                                    }, 1000);


                                }
                            }
                        } else {
                            new SweetAlertDialog(v.getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText("View Plans")
                                    .setContentText(v.getContext().getResources().getString(R.string.alert_msg_view_plans))
                                    .setCustomImage(R.drawable.blob)
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                            Intent i2 = new Intent(v.getContext(), SubscriptionActivity.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("screen_name", "MCQ");
                                            i2.putExtras(bundle);
                                            v.getContext().startActivity(i2);
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();
                        }
                    }

                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return position;
    }

    private boolean isPositionHeader(int position) {
        return list.get(position) instanceof McqSMHeader;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    class VHHeader extends RecyclerView.ViewHolder {
        TextView txtTitle;

        public VHHeader(View itemView) {
            super(itemView);
            this.txtTitle = (TextView) itemView.findViewById(R.id.header_text);
        }
    }

    class VHItem extends RecyclerView.ViewHolder {
        TextView txtName, mcqCount;
        ImageView completed, lock;

        public VHItem(View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.vedio_sub_heading);
            this.mcqCount = (TextView) itemView.findViewById(R.id.mcq_count);
            this.lock = itemView.findViewById(R.id.lock);
            this.completed = (ImageView) itemView.findViewById(R.id.completed);
        }
    }
}
