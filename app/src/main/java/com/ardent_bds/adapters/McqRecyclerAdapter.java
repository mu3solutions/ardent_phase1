package com.ardent_bds.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.model.McqModuleData;
import com.ardent_bds.ui.mcq_test_module.McqSubModuleActivity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class McqRecyclerAdapter extends RecyclerView.Adapter<McqRecyclerAdapter.MyViewHolder> {
    ArrayList<McqModuleData> mData;
    Context context;
    public McqRecyclerAdapter(ArrayList<McqModuleData> mcqModuleData) {
        this.mData = mcqModuleData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mcq_module_layout, parent, false);
        context = view.getContext();
        return new MyViewHolder(view);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView heading,  bookValue, totalTime, totalModule,progressText;
        ProgressBar progressBar;
        ImageView iconImage;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            heading = itemView.findViewById(R.id.video_heading);
            bookValue = itemView.findViewById(R.id.book_val);
            totalTime = itemView.findViewById(R.id.total_time_val);

            iconImage = itemView.findViewById(R.id.image_icon);

            progressText=itemView.findViewById(R.id.progress_text);
            progressBar=itemView.findViewById(R.id.progressBar);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.heading.setText(mData.get(position).getModuleName());
        holder.totalTime.setText(String.valueOf(mData.get(position).getTotalModule()));
        holder.bookValue.setText("out of \n"+String.valueOf(mData.get(position).getTotalModule())+" modules");
        holder.progressText.setText(String.valueOf(mData.get(position).getCompletedModule()));
        holder.progressBar.setMax(mData.get(position).getTotalModule());
        holder.progressBar.setProgress(mData.get(position).getCompletedModule());
        String ur=mData.get(position).getModuleIcon();
        Glide.with(context).load(ur).into(holder.iconImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                McqModuleData dataModule = McqModuleData.getInstance();
                dataModule.setModuleId(mData.get(position).getModuleId());
                dataModule.setModuleName("" + mData.get(position).getModuleName());
                dataModule.setCompletedModule(mData.get(position).getCompletedModule());
                dataModule.setTotalModule(mData.get(position).getTotalModule());

                Intent intent = new Intent(v.getContext(), McqSubModuleActivity.class);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
