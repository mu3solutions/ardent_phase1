package com.ardent_bds.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.CustomSliderWhatsappStyle.NuggetsSliders;
import com.ardent_bds.R;
import com.ardent_bds.model.StatusDataModel;
import com.ardent_bds.ui.home.Video5MinNuggets;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class StatusRecyclerAdapter extends RecyclerView.Adapter<StatusRecyclerAdapter.MyViewHolder> {
    ArrayList<StatusDataModel> sta;
    Context context;


    String[] img;
    String[] imgNames;
    String[] videos_5_mins_nuggets;
    String video5MinsUrl;
    String video5MinsTxt;

    StatusDataModel statusDataModel = StatusDataModel.getInstance();

    public StatusRecyclerAdapter(Context ctx, String[] slideUrlArray1, String[] slideUrlArray2, ArrayList<StatusDataModel> statusDataModel, String min5VideosNuggets, String min5VideosNuggetsTxt) {

        this.context = ctx;
        this.img = slideUrlArray1;
        this.imgNames = slideUrlArray2;
        this.sta = statusDataModel;
        this.video5MinsUrl = min5VideosNuggets;
        this.video5MinsTxt = min5VideosNuggetsTxt;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        TextView statusName, statusDesc;
        ImageView statusImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.status_layout);
            statusImage = itemView.findViewById(R.id.status_image);
            statusName = itemView.findViewById(R.id.status_name);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_layout, parent, false);
        context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {


        if (position % 2 == 0) {
            holder.layout.setBackgroundResource(R.color.golden_yellow);
//            holder.statusDesc.setTextColor(ContextCompat.getColor(context, R.color.white));
            //          holder.statusName.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
          holder.statusName.setText("" + imgNames[position]);
        Glide.with(context)
                .load(img[position].replace("\r\n", ""))
                .into(holder.statusImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == 1) {
                    Intent videourl = new Intent(context, Video5MinNuggets.class);
                    videourl.putExtra("nug_5_video", video5MinsUrl);
                    videourl.putExtra("video5MinsTxt", video5MinsTxt);
                    context.startActivity(videourl);

                } else {
                    Intent intent = new Intent(view.getContext(), NuggetsSliders.class);
                    view.getContext().startActivity(intent);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return img.length;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
