package com.ardent_bds.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.model.ListItem;
import com.ardent_bds.model.VedioSubDataModule;
import com.ardent_bds.tabs.AllVideosTabsActivity;
import com.ardent_bds.ui.UserAccessLevelModel;
import com.ardent_bds.ui.gallery.SubscriptionActivity;

import java.util.ArrayList;
import java.util.List;

public class VedioSubRecyclerAdapter extends RecyclerView.Adapter<VedioSubRecyclerAdapter.MyViewHolder> {
    ArrayList<ListItem> vsm;
    Context context;
    ArrayList<VedioSubDataModule> vedioDataModulesA;

    private String userAccessList;

    public VedioSubRecyclerAdapter(Context context, String userAccessList) {
        this.context =context;
        this.userAccessList = userAccessList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView heading, time, ratingText;
        RatingBar ratingBar;
        ImageView iv_lock_videos, iv_completed_videos;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            heading = itemView.findViewById(R.id.vedio_sub_heading);
            time = itemView.findViewById(R.id.vedio_time);
            ratingText = itemView.findViewById(R.id.vedio_rating_number);
            ratingBar = itemView.findViewById(R.id.rating);
            iv_lock_videos = itemView.findViewById(R.id.iv_lock_videos);
            iv_completed_videos = itemView.findViewById(R.id.iv_completed_videos);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vedio_sub_layout, parent, false);
        context = view.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        /*userAccessList = new ArrayList<>();*/
        holder.heading.setText("" + VedioSubDataModule.getInstance().getArrayListAA().get(position).getChildName());
        holder.time.setText("" + VedioSubDataModule.getInstance().getArrayListAA().get(position).getTime());
        holder.ratingText.setText("" + VedioSubDataModule.getInstance().getArrayListAA().get(position).getRating());
        holder.ratingBar.setRating(Float.parseFloat(VedioSubDataModule.getInstance().getArrayListAA().get(position).getRating()));

        if (position == 0) {
            holder.iv_lock_videos.setVisibility(View.GONE);
        } else {
            if (userAccessList.equalsIgnoreCase("2") || userAccessList.equalsIgnoreCase("12")
                    || userAccessList.equalsIgnoreCase("21") || userAccessList.equalsIgnoreCase("123")
                    || userAccessList.equalsIgnoreCase("23") || userAccessList.equalsIgnoreCase("32")) {
                holder.iv_lock_videos.setVisibility(View.GONE);
            }
        }

        if ( VedioSubDataModule.getInstance().getArrayListAA().get(position).getStatus() == 1) {
            holder.iv_completed_videos.setVisibility(View.VISIBLE);
            holder.iv_lock_videos.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (position == 0) {

                    VedioSubDataModule videoSubDataModule = VedioSubDataModule.getInstance();
                    videoSubDataModule.setChildID(VedioSubDataModule.getInstance().getArrayListAA().get(position).getChildID());
                    videoSubDataModule.setStatus(VedioSubDataModule.getInstance().getArrayListAA().get(position).getStatus());
                    videoSubDataModule.setChildDesc(VedioSubDataModule.getInstance().getArrayListAA().get(position).getChildDesc());
                    videoSubDataModule.setSelectedModuleName(VedioSubDataModule.getInstance().getArrayListAA().get(position).getChildName());

                    Intent intent = new Intent(v.getContext(), AllVideosTabsActivity.class);
                    v.getContext().startActivity(intent);
                } else {
                    if (userAccessList.equalsIgnoreCase("2") || userAccessList.equalsIgnoreCase("12")
                            || userAccessList.equalsIgnoreCase("21") || userAccessList.equalsIgnoreCase("123")
                            || userAccessList.equalsIgnoreCase("23") || userAccessList.equalsIgnoreCase("32")) {
                        VedioSubDataModule videoSubDataModule = VedioSubDataModule.getInstance();
                        videoSubDataModule.setChildID(VedioSubDataModule.getInstance().getArrayListAA().get(position).getChildID());
                        videoSubDataModule.setStatus(VedioSubDataModule.getInstance().getArrayListAA().get(position).getStatus());
                        videoSubDataModule.setChildDesc(VedioSubDataModule.getInstance().getArrayListAA().get(position).getChildDesc());
                        videoSubDataModule.setSelectedModuleName(VedioSubDataModule.getInstance().getArrayListAA().get(position).getChildName());
                        Intent intent = new Intent(v.getContext(), AllVideosTabsActivity.class);
                        v.getContext().startActivity(intent);
                    } else {
                        showPaymentAlert();
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return VedioSubDataModule.getInstance().getArrayListAA().size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



    private void showPaymentAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
        View view2 = layoutInflaterAndroid.inflate(R.layout.my_custom_dialog_view_plans, null);
        builder.setView(view2);
        builder.setCancelable(false);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button ok = alertDialog.findViewById(R.id.buttonOk);
        TextView mayBeLater = alertDialog.findViewById(R.id.may_be_later);

        ok.setText("View Plans");
        mayBeLater.setText("Cancel");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Intent i2 = new Intent(context, SubscriptionActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("screen_name", "VC");
                i2.putExtras(bundle);
                context.startActivity(i2);
            }
        });
        mayBeLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

}
