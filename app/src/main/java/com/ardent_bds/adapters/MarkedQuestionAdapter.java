package com.ardent_bds.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ardent_bds.R;
import com.ardent_bds.ui.LiveTest.GrandTest;
import com.ardent_bds.ui.home.GrantTestDataModel;

import java.util.HashMap;

public class MarkedQuestionAdapter extends RecyclerView.Adapter<MarkedQuestionAdapter.ViewHolder> {

    String[] values;

    HashMap<Integer, Integer> listDataMarked;
    HashMap<Integer, Integer> listSkippedData;
    HashMap<Integer, Integer> listSelected;
    int tCount;
    Context context1;

    String[] alreadySelectedAnswers;

    public MarkedQuestionAdapter(Context context, HashMap<Integer, Integer> arrayListDataMarked, int totaoArrayListCount, HashMap<Integer, Integer> arrayListDataSkipped, HashMap<Integer, Integer> getArrayListDataAttempted) {

        listDataMarked = arrayListDataMarked;
        tCount = totaoArrayListCount;
        context1 = context;
        listSkippedData = arrayListDataSkipped;
        listSelected = getArrayListDataAttempted;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public CardView cardview1;

        public ViewHolder(View v) {
            super(v);
            textView = (TextView) v.findViewById(R.id.textview1);
            cardview1 = (CardView) v.findViewById(R.id.cardview1);
        }
    }

    @Override
    public MarkedQuestionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view1 = LayoutInflater.from(context1).inflate(R.layout.list_marked_items, parent, false);

        ViewHolder viewHolder1 = new ViewHolder(view1);

        return viewHolder1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Vholder, final int position) {
        // for (int i = 0; i < tCount - 1; i++) {


        for (int i = 0; i < tCount; i++) {
            int po = position + 1;
            Vholder.textView.setText("" + po);


            if (listDataMarked.containsKey(position)) {
                Vholder.textView.setBackgroundColor(context1.getResources().getColor(R.color.vedantu_orange));
                Vholder.textView.setTextColor(Color.WHITE);

            } /*else if (listSkippedData.containsKey(position)) {
                Vholder.textView.setBackgroundColor(context1.getResources().getColor(R.color.vedantu_orange));
                Vholder.textView.setTextColor(Color.WHITE);

            }*/ else if (listSelected.containsKey(position)) {
                Vholder.textView.setBackgroundColor(context1.getResources().getColor(R.color.blue));
                Vholder.textView.setTextColor(Color.WHITE);
            } /*else if (listSelected.containsKey(position) && listDataMarked.containsKey(position)) {
                Vholder.textView.setBackgroundColor(context1.getResources().getColor(R.color.green));
                Vholder.textView.setTextColor(Color.BLACK);
            }*/
        }


        Vholder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedQuestionID = Vholder.getAdapterPosition();
                Log.i("getId", "" + selectedQuestionID);
                Intent selctedQuestionPos = new Intent(new Intent(context1, GrandTest.class));
                selctedQuestionPos.putExtra("QP", selectedQuestionID);
                selctedQuestionPos.putExtra("QNO", Integer.parseInt(Vholder.textView.getText().toString()));
                selctedQuestionPos.putExtra("from_module_screen", "FMS1");

                selctedQuestionPos.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context1.startActivity(selctedQuestionPos);


/*
                if (listSelected.size() == 0) {
                    GrantTestDataModel.getInstance().setNothingSelected(true);
                }*/

            }
        });
    }

    @Override
    public int getItemCount() {
        return tCount;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}