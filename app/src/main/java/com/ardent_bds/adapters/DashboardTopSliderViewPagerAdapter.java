package com.ardent_bds.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.ardent_bds.R;
import com.bumptech.glide.Glide;


public class DashboardTopSliderViewPagerAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private Integer[] images = {R.drawable.ads, R.drawable.ads, R.drawable.ads};
    //private Integer[] images = {};
    private Integer[] dummyImages = {R.drawable.onboard1};
    private String[] slideUrlArray;

    int currentIndex;
    int currentPage = 0;

    public DashboardTopSliderViewPagerAdapter(Context context, String[] slideImageUrlList) {
        this.context = context;
        this.slideUrlArray = slideImageUrlList;

    }

    @Override
    public int getCount() {
        return slideUrlArray.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(R.layout.custom_layout, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        final ViewPager vp = (ViewPager) container;
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (vp.getCurrentItem() + 1 == slideUrlArray.length - 1) {
                    vp.setCurrentItem(slideUrlArray.length, true);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            vp.setCurrentItem(position - slideUrlArray.length - 1, true);
                        }
                    }, 3000);
                } else {
                    vp.setCurrentItem(position, true);
                }
            }
        }, 3000);

        Glide.with(context)
                .load(slideUrlArray[position].replace("\r\n", ""))
                .into(imageView);
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }
}
