package com.ardent_bds.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.ardent_bds.R;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {

    private ArrayList<QuestionDataModel> all_folder;
    private LayoutInflater mInflater;
    private ViewPager2 viewPager2;
    Context context;
    HashMap<Integer, Integer> hashMap;
    int correctAnswerPosition;
    ArrayList<Integer> hmp1 = new ArrayList<>();
    ImageView imageViewAnswerImage;
    TextView txt_description_answer;

    private int[] colorArray = new int[]{android.R.color.black, android.R.color.holo_blue_dark, android.R.color.holo_green_dark, android.R.color.holo_red_dark};
    int page = 0;

    public ReviewAdapter(Context context, ArrayList<QuestionDataModel> data, HashMap<Integer, Integer> hashMap, ViewPager2 viewPager2) {
        this.mInflater = LayoutInflater.from(context);
        this.all_folder = data;
        this.viewPager2 = viewPager2;
        this.context = context;
        this.hashMap = hashMap;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.review_layout, parent, false);

        imageViewAnswerImage =  view.findViewById(R.id.imageViewAnswerImage);
        txt_description_answer =  view.findViewById(R.id.txt_description_answer);

        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        //holder.myTextView.setText(String.valueOf(mData.get(position).getQuestionId()));
        // page = position;
        String ques = all_folder.get(position).getMcqQuestionDesc();

        if (all_folder.get(position).getMcqAnswerImage() != null) {
            Glide.with(context)
                    .load(all_folder.get(position).getMcqAnswerImage())
                    .into(imageViewAnswerImage);
        }

        correctAnswerPosition = all_folder.get(position).getMcqCorrectAnswer();

        int selectedQn = hashMap.get(all_folder.get(position).getQuestionId());

        holder.question_txt.setText("" + ques + selectedQn + correctAnswerPosition + position);
        holder.answer_txt1.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[0].replace("\r\n", ""));
        holder.answer_txt2.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[1].replace("\r\n", ""));
        holder.answer_txt3.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[2].replace("\r\n", ""));
        holder.answer_txt4.setText("" + all_folder.get(position).getMcqAnswerList().split(",")[3].replace("\r\n", ""));

        holder.answer_txt1.setTextColor(R.color.text_normal_black);
        holder.answer_txt2.setTextColor(R.color.text_normal_black);
        holder.answer_txt3.setTextColor(R.color.text_normal_black);
        holder.answer_txt4.setTextColor(R.color.text_normal_black);

        holder.a.setBackgroundTintList(context.getResources().getColorStateList(R.color.text_normal_black));
        holder.b.setBackgroundTintList(context.getResources().getColorStateList(R.color.text_normal_black));
        holder.c.setBackgroundTintList(context.getResources().getColorStateList(R.color.text_normal_black));
        holder.d.setBackgroundTintList(context.getResources().getColorStateList(R.color.text_normal_black));

        holder.imv_tick_cross1.setVisibility(View.GONE);
        holder.imv_tick_cross2.setVisibility(View.GONE);
        holder.imv_tick_cross3.setVisibility(View.GONE);
        holder.imv_tick_cross4.setVisibility(View.GONE);

        if (correctAnswerPosition == 1) {
            holder.imv_tick_cross1.setImageResource(R.drawable.green_tick);
            holder.answer_txt1.setTextColor(context.getResources().getColorStateList(R.color.main_green_color));
            holder.a.setBackgroundTintList(context.getResources().getColorStateList(R.color.main_green_color));
            holder.imv_tick_cross1.setVisibility(View.VISIBLE);
        } else if (correctAnswerPosition == 2) {
            holder.imv_tick_cross2.setImageResource(R.drawable.green_tick);
            holder.answer_txt2.setTextColor(context.getResources().getColorStateList(R.color.main_green_color));
            holder.b.setBackgroundTintList(context.getResources().getColorStateList(R.color.main_green_color));
            holder.imv_tick_cross2.setVisibility(View.VISIBLE);
        } else if (correctAnswerPosition == 3) {
            holder.imv_tick_cross3.setImageResource(R.drawable.green_tick);
            holder.answer_txt3.setTextColor(context.getResources().getColorStateList(R.color.main_green_color));
            holder.c.setBackgroundTintList(context.getResources().getColorStateList(R.color.main_green_color));
            holder.imv_tick_cross3.setVisibility(View.VISIBLE);
        } else if (correctAnswerPosition == 4) {
            holder.imv_tick_cross4.setImageResource(R.drawable.green_tick);
            holder.answer_txt4.setTextColor(context.getResources().getColorStateList(R.color.main_green_color));
            holder.d.setBackgroundTintList(context.getResources().getColorStateList(R.color.main_green_color));
            holder.imv_tick_cross4.setVisibility(View.VISIBLE);
        }


        if (correctAnswerPosition != selectedQn) {
            if (selectedQn == 1) {
                holder.imv_tick_cross1.setImageResource(R.drawable.red_cross);
                holder.answer_txt1.setTextColor(context.getResources().getColorStateList(R.color.red_btn_bg_pressed_color));
                holder.a.setBackgroundTintList(context.getResources().getColorStateList(R.color.red_btn_bg_pressed_color));
                holder.imv_tick_cross1.setVisibility(View.VISIBLE);
            } else if (selectedQn == 2) {
                holder.imv_tick_cross2.setImageResource(R.drawable.red_cross);
                holder.answer_txt2.setTextColor(context.getResources().getColorStateList(R.color.red_btn_bg_pressed_color));
                holder.b.setBackgroundTintList(context.getResources().getColorStateList(R.color.red_btn_bg_pressed_color));
                holder.imv_tick_cross2.setVisibility(View.VISIBLE);
            } else if (selectedQn == 3) {
                holder.imv_tick_cross3.setImageResource(R.drawable.red_cross);
                holder.answer_txt3.setTextColor(context.getResources().getColorStateList(R.color.red_btn_bg_pressed_color));
                holder.c.setBackgroundTintList(context.getResources().getColorStateList(R.color.red_btn_bg_pressed_color));
                holder.imv_tick_cross3.setVisibility(View.VISIBLE);
            } else if (selectedQn == 4) {
                holder.imv_tick_cross4.setImageResource(R.drawable.red_cross);
                holder.answer_txt4.setTextColor(context.getResources().getColorStateList(R.color.red_btn_bg_pressed_color));
                holder.d.setBackgroundTintList(context.getResources().getColorStateList(R.color.red_btn_bg_pressed_color));
                holder.imv_tick_cross4.setVisibility(View.VISIBLE);
            }

        }
        hmp1.add(position, correctAnswerPosition);
    }


    @Override
    public int getItemCount() {
        return all_folder.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView option_id, a, b, c, d, answer_txt1, answer_txt2, answer_txt3, answer_txt4, question_txt, txt_description_answer, proceed_tv;
        ImageView imv_tick_cross1, imv_tick_cross2, imv_tick_cross3, imv_tick_cross4, next, prev;
        LinearLayout ll1, ll2, ll3, ll4, ll_answer_desc, ans_li_ly;

        ViewHolder(View viewLayout) {
            super(viewLayout);
            ans_li_ly = viewLayout.findViewById(R.id.answer_linear_layout);
            question_txt = viewLayout.findViewById(R.id.question_txt);
            ll_answer_desc = viewLayout.findViewById(R.id.ll_answer_desc);

            a = viewLayout.findViewById(R.id.A);
            b = viewLayout.findViewById(R.id.B);
            c = viewLayout.findViewById(R.id.C);
            d = viewLayout.findViewById(R.id.D);

            answer_txt1 = viewLayout.findViewById(R.id.answer1);
            answer_txt2 = viewLayout.findViewById(R.id.answer2);
            answer_txt3 = viewLayout.findViewById(R.id.answer3);
            answer_txt4 = viewLayout.findViewById(R.id.answer4);
            txt_description_answer = viewLayout.findViewById(R.id.txt_description_answer);
            proceed_tv = viewLayout.findViewById(R.id.proceed_tv);

            ll1 = viewLayout.findViewById(R.id.ll1);
            ll2 = viewLayout.findViewById(R.id.ll2);
            ll3 = viewLayout.findViewById(R.id.ll3);
            ll4 = viewLayout.findViewById(R.id.ll4);

            imv_tick_cross1 = viewLayout.findViewById(R.id.imv_tick_cross1);
            imv_tick_cross2 = viewLayout.findViewById(R.id.imv_tick_cross2);
            imv_tick_cross3 = viewLayout.findViewById(R.id.imv_tick_cross3);
            imv_tick_cross4 = viewLayout.findViewById(R.id.imv_tick_cross4);

        }
    }

}