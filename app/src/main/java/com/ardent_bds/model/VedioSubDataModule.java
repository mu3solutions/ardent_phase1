package com.ardent_bds.model;

import java.util.ArrayList;

public class VedioSubDataModule {
    private int moduleVid;
    private int subModuleVid;
    private String subModuleName;
    private int childID;
    private String childName;
    private String childImage;
    private String childDesc;
    private String time;
    private String rating;
    private int accesslevel;
    private String smvVedioId;
    private String smvSlideId;
    private String smvNoteId;
    private int status;
    private int totalCount;
    public int selectedChildId;


    public String getSelectedModuleName() {
        return selectedModuleName;
    }

    public void setSelectedModuleName(String selectedModuleName) {
        this.selectedModuleName = selectedModuleName;
    }

    public String selectedModuleName;

    private static VedioSubDataModule instance = null;
    public ArrayList<VedioSubDataModule> arrayListAA = new ArrayList<>();


    public static VedioSubDataModule getInstance() {
        if (instance == null) {
            instance = new VedioSubDataModule();
        }
        return instance;
    }


    public VedioSubDataModule() {
    }

    public VedioSubDataModule(int moduleVid, int subModuleVid, String subModuleName, int childID, String childName, String childImage, String childDesc, String time, String rating, int accesslevel, String smvVedioId, String smvSlideId, String smvNoteId, int status, int totalCount, int selectedChildId, ArrayList<VedioSubDataModule> arrayListAA) {
        this.moduleVid = moduleVid;
        this.subModuleVid = subModuleVid;
        this.subModuleName = subModuleName;
        this.childID = childID;
        this.childName = childName;
        this.childImage = childImage;
        this.childDesc = childDesc;
        this.time = time;
        this.rating = rating;
        this.accesslevel = accesslevel;
        this.smvVedioId = smvVedioId;
        this.smvSlideId = smvSlideId;
        this.smvNoteId = smvNoteId;
        this.status = status;
        this.totalCount = totalCount;
        this.selectedChildId = selectedChildId;
        this.arrayListAA = arrayListAA;
    }

    public int getModuleVid() {
        return moduleVid;
    }

    public void setModuleVid(int moduleVid) {
        this.moduleVid = moduleVid;
    }

    public int getSubModuleVid() {
        return subModuleVid;
    }

    public void setSubModuleVid(int subModuleVid) {
        this.subModuleVid = subModuleVid;
    }

    public String getSubModuleName() {
        return subModuleName;
    }

    public void setSubModuleName(String subModuleName) {
        this.subModuleName = subModuleName;
    }

    public int getChildID() {
        return childID;
    }

    public void setChildID(int childID) {
        this.childID = childID;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getChildImage() {
        return childImage;
    }

    public void setChildImage(String childImage) {
        this.childImage = childImage;
    }

    public String getChildDesc() {
        return childDesc;
    }

    public void setChildDesc(String childDesc) {
        this.childDesc = childDesc;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getAccesslevel() {
        return accesslevel;
    }

    public void setAccesslevel(int accesslevel) {
        this.accesslevel = accesslevel;
    }

    public String getSmvVedioId() {
        return smvVedioId;
    }

    public void setSmvVedioId(String smvVedioId) {
        this.smvVedioId = smvVedioId;
    }

    public String getSmvSlideId() {
        return smvSlideId;
    }

    public void setSmvSlideId(String smvSlideId) {
        this.smvSlideId = smvSlideId;
    }

    public String getSmvNoteId() {
        return smvNoteId;
    }

    public void setSmvNoteId(String smvNoteId) {
        this.smvNoteId = smvNoteId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getSelectedChildId() {
        return selectedChildId;
    }

    public void setSelectedChildId(int selectedChildId) {
        this.selectedChildId = selectedChildId;
    }

    public static void setInstance(VedioSubDataModule instance) {
        VedioSubDataModule.instance = instance;
    }

    public ArrayList<VedioSubDataModule> getArrayListAA() {
        return arrayListAA;
    }

    public void setArrayListAA(ArrayList<VedioSubDataModule> arrayListAA) {
        this.arrayListAA = arrayListAA;
    }
}
