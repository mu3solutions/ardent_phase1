package com.ardent_bds.model;

public class VedioContentItem  extends ListItem{
    private int smChildId;
    private String smChildName;
    private String smChildDesc;
    private String smChildRating;
    private String smChildimage;
    private int status;
    private int accessLevel;
    private int qnCount;

    public VedioContentItem() {
    }

    public VedioContentItem(int smChildId, String smChildName, String smChildDesc, String smChildRating, String smChildimage, int status, int accessLevel, int qnCount) {
        this.smChildId = smChildId;
        this.smChildName = smChildName;
        this.smChildDesc = smChildDesc;
        this.smChildRating = smChildRating;
        this.smChildimage = smChildimage;
        this.status = status;
        this.accessLevel = accessLevel;
        this.qnCount = qnCount;
    }

    public int getSmChildId() {
        return smChildId;
    }

    public void setSmChildId(int smChildId) {
        this.smChildId = smChildId;
    }

    public String getSmChildName() {
        return smChildName;
    }

    public void setSmChildName(String smChildName) {
        this.smChildName = smChildName;
    }

    public String getSmChildDesc() {
        return smChildDesc;
    }

    public void setSmChildDesc(String smChildDesc) {
        this.smChildDesc = smChildDesc;
    }

    public String getSmChildRating() {
        return smChildRating;
    }

    public void setSmChildRating(String smChildRating) {
        this.smChildRating = smChildRating;
    }

    public String getSmChildimage() {
        return smChildimage;
    }

    public void setSmChildimage(String smChildimage) {
        this.smChildimage = smChildimage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public int getQnCount() {
        return qnCount;
    }

    public void setQnCount(int qnCount) {
        this.qnCount = qnCount;
    }
}
