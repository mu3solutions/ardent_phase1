package com.ardent_bds.model;

public class StatesAndCollegeModule {
    private int id;
    private String statecode;
    private String stateName;
    private String collegeName;

    public StatesAndCollegeModule() {
    }

    public StatesAndCollegeModule(int id, String statecode, String stateName, String collegeName) {
        this.id = id;
        this.statecode = statecode;
        this.stateName = stateName;
        this.collegeName = collegeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatecode() {
        return statecode;
    }

    public void setStatecode(String statecode) {
        this.statecode = statecode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }
}
