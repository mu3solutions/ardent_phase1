package com.ardent_bds.model;


public class McqSubModleData {
    private int moduleId;
    private int subModuleId;
    private String subModuleName;
    private int childModuleId;
    private String childModuleName;
    private String childModuleDesc;
    private String childModuleImage;
    private String childModuleRating;
    private int accessLevel;
    private int score;
    private int scorePercent;
    private String qnArray;
    private String status;
    private int qnCount;



    private static McqSubModleData instance = null;

    public static McqSubModleData getInstance() {
        if (instance == null) {
            instance = new McqSubModleData();
        }
        return instance;
    }
/*
    public static void setInstance(McqSubModleData instance) {
        McqSubModleData.instance = instance;
    }*/

    public McqSubModleData() {
    }

    public McqSubModleData(int moduleId, int subModuleId, String subModuleName, int childModuleId, String childModuleName, String childModuleDesc, String childModuleImage, String childModuleRating, int accessLevel, int score, int scorePercent, String qnArray, String status, int qnCount) {
        this.moduleId = moduleId;
        this.subModuleId = subModuleId;
        this.subModuleName = subModuleName;
        this.childModuleId = childModuleId;
        this.childModuleName = childModuleName;
        this.childModuleDesc = childModuleDesc;
        this.childModuleImage = childModuleImage;
        this.childModuleRating = childModuleRating;
        this.accessLevel = accessLevel;
        this.score = score;
        this.scorePercent = scorePercent;
        this.qnArray = qnArray;
        this.status = status;
        this.qnCount = qnCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScorePercent() {
        return scorePercent;
    }

    public void setScorePercent(int scorePercent) {
        this.scorePercent = scorePercent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChildModuleDesc() {
        return childModuleDesc;
    }

    public void setChildModuleDesc(String childModuleDesc) {
        this.childModuleDesc = childModuleDesc;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public int getSubModuleId() {
        return subModuleId;
    }

    public void setSubModuleId(int subModuleId) {
        this.subModuleId = subModuleId;
    }

    public String getSubModuleName() {
        return subModuleName;
    }

    public void setSubModuleName(String subModuleName) {
        this.subModuleName = subModuleName;
    }

    public int getChildModuleId() {
        return childModuleId;
    }

    public void setChildModuleId(int childModuleId) {
        this.childModuleId = childModuleId;
    }

    public String getChildModuleName() {
        return childModuleName;
    }

    public void setChildModuleName(String childModuleName) {
        this.childModuleName = childModuleName;
    }

    public String getChildModuleImage() {
        return childModuleImage;
    }

    public void setChildModuleImage(String childModuleImage) {
        this.childModuleImage = childModuleImage;
    }

    public String getChildModuleRating() {
        return childModuleRating;
    }

    public void setChildModuleRating(String childModuleRating) {
        this.childModuleRating = childModuleRating;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getQnArray() {
        return qnArray;
    }

    public void setQnArray(String qnArray) {
        this.qnArray = qnArray;
    }

    public int getQnCount() {
        return qnCount;
    }

    public void setQnCount(int qnCount) {
        this.qnCount = qnCount;
    }
}
