package com.ardent_bds.model;

public class McqSMContentItem extends ListItem {
    private int smChildId;
    private String smChildName;
    private String smChildDesc;
    private String smChildRating;
    private String smChildimage;
    private String status;
    private int accessLevel;
    private int qnConut;

    public McqSMContentItem() {
    }

    public McqSMContentItem(int smChildId, String smChildName, String smChildDesc, String smChildRating, String smChildimage, String status, int accessLevel, int qnConut) {
        this.smChildId = smChildId;
        this.smChildName = smChildName;
        this.smChildDesc = smChildDesc;
        this.smChildRating = smChildRating;
        this.smChildimage = smChildimage;
        this.status = status;
        this.accessLevel = accessLevel;
        this.qnConut = qnConut;
    }

    public int getSmChildId() {
        return smChildId;
    }

    public void setSmChildId(int smChildId) {
        this.smChildId = smChildId;
    }

    public String getSmChildName() {
        return smChildName;
    }

    public void setSmChildName(String smChildName) {
        this.smChildName = smChildName;
    }

    public String getSmChildDesc() {
        return smChildDesc;
    }

    public void setSmChildDesc(String smChildDesc) {
        this.smChildDesc = smChildDesc;
    }

    public String getSmChildRating() {
        return smChildRating;
    }

    public void setSmChildRating(String smChildRating) {
        this.smChildRating = smChildRating;
    }

    public String getSmChildimage() {
        return smChildimage;
    }

    public void setSmChildimage(String smChildimage) {
        this.smChildimage = smChildimage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public int getQnConut() {
        return qnConut;
    }

    public void setQnConut(int qnConut) {
        this.qnConut = qnConut;
    }
}

