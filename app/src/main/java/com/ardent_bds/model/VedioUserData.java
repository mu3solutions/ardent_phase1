package com.ardent_bds.model;

import java.util.ArrayList;

public class VedioUserData {
    private int vedioModuleId;
    private int vedioChildId;
    private String returnTime;
    private int status;

    private static ArrayList<VedioUserData> instance = null;

    public static ArrayList<VedioUserData> getInstance() {
        if (instance == null) {
            instance = new ArrayList<VedioUserData>();
        }
        return instance;
    }

    public VedioUserData() {
    }

    public VedioUserData(int vedioModuleId, int vedioChildId, String returnTime, int status) {
        this.vedioModuleId = vedioModuleId;
        this.vedioChildId = vedioChildId;
        this.returnTime = returnTime;
        this.status = status;
    }

    public int getVedioModuleId() {
        return vedioModuleId;
    }

    public void setVedioModuleId(int vedioModuleId) {
        this.vedioModuleId = vedioModuleId;
    }

    public int getVedioChildId() {
        return vedioChildId;
    }

    public void setVedioChildId(int vedioChildId) {
        this.vedioChildId = vedioChildId;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static void setInstance(ArrayList<VedioUserData> instance) {
        VedioUserData.instance = instance;
    }
}
