package com.ardent_bds.model;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class StatusDataModel {
    private String name;
    private String desc;
    private String image;
    private String db_suggested_videos;
    private String db_suggested_videos_txt;

    public String getNug_5_mins_video() {
        return nug_5_mins_video;
    }

    public void setNug_5_mins_video(String nug_5_mins_video) {
        this.nug_5_mins_video = nug_5_mins_video;
    }

    private String nug_5_mins_video;

    public String getNug_5_mins_txt() {
        return nug_5_mins_txt;
    }


    public void setNug_5_mins_txt(String nug_5_mins_txt) {
        this.nug_5_mins_txt = nug_5_mins_txt;
    }

    private String nug_5_mins_txt;

    public String getNug_concepts_video() {
        return nug_concepts_video;
    }

    public void setNug_concepts_video(String nug_concepts_video) {
        this.nug_concepts_video = nug_concepts_video;
    }

    private String nug_concepts_video;

    public String getDb_suggested_videos() {
        return db_suggested_videos;
    }

    public void setDb_suggested_videos(String db_suggested_videos) {
        this.db_suggested_videos = db_suggested_videos;
    }

    public String getDb_suggested_videos_txt() {
        return db_suggested_videos_txt;
    }

    public void setDb_suggested_videos_txt(String db_suggested_videos_txt) {
        this.db_suggested_videos_txt = db_suggested_videos_txt;
    }

    public String getSliderImageList() {
        return sliderImageList;
    }

    public void setSliderImageList(String sliderImageList) {
        this.sliderImageList = sliderImageList;
    }

    private String sliderImageList;

    public String getNuggetsSliderImageList() {
        return nuggetsSliderImageList;
    }

    public void setNuggetsSliderImageList(String nuggetsSliderImageList) {
        this.nuggetsSliderImageList = nuggetsSliderImageList;
    }

    private String nuggetsSliderImageList;
    private Boolean highlighted;
    private static StatusDataModel instance = null;

    public StatusDataModel() {
    }

    public StatusDataModel(String name, String desc, String image, Boolean highlighted) {
        this.name = name;
        this.desc = desc;
        this.image = image;
        this.highlighted = highlighted;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getHighlighted() {
        return highlighted;
    }

    public void setHighlighted(Boolean highlighted) {
        this.highlighted = highlighted;
    }



    public static StatusDataModel getInstance() {
        if (instance == null) {
            instance = new StatusDataModel();
        }
        return instance;
    }

}

