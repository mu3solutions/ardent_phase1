package com.ardent_bds.model;

import java.util.HashMap;

public class DataModelTemp {


    private static DataModelTemp instance = null;


    public static DataModelTemp getInstance() {
        if (instance == null) {
            instance = new DataModelTemp();
        }
        return instance;
    }


    private HashMap<Integer, Integer> getTempMarkedAnswers;

    public HashMap<Integer, Integer> getGetTempMarkedAnswers() {
        return getTempMarkedAnswers;
    }

    public void setGetTempMarkedAnswers(HashMap<Integer, Integer> getTempMarkedAnswers) {
        this.getTempMarkedAnswers = getTempMarkedAnswers;
    }

    public HashMap<Integer, Integer> getGetTempSkippedAnswers() {
        return getTempSkippedAnswers;
    }

    public void setGetTempSkippedAnswers(HashMap<Integer, Integer> getTempSkippedAnswers) {
        this.getTempSkippedAnswers = getTempSkippedAnswers;
    }

    public HashMap<Integer, Integer> getGetTempSelecetdAnswers() {
        return getTempSelecetdAnswers;
    }

    public void setGetTempSelecetdAnswers(HashMap<Integer, Integer> getTempSelecetdAnswers) {
        this.getTempSelecetdAnswers = getTempSelecetdAnswers;
    }

    private HashMap<Integer, Integer> getTempSkippedAnswers;
    private HashMap<Integer, Integer> getTempSelecetdAnswers;

    public HashMap<Integer, Integer> getGetTempCorrectAnswers() {
        return getTempCorrectAnswers;
    }

    public void setGetTempCorrectAnswers(HashMap<Integer, Integer> getTempCorrectAnswers) {
        this.getTempCorrectAnswers = getTempCorrectAnswers;
    }

    private HashMap<Integer, Integer> getTempCorrectAnswers;


}
