package com.ardent_bds.model;

public class VedioDataModule {
    private int moduleId;
    private String moduleName;
    private String moduleIcons;
    private String desc;
    private String totalVedio;
    private String completedVedio;
    private String time;
    private String heading;

    private static VedioDataModule instance = null;

    public static VedioDataModule getInstance() {
        if (instance == null) {
            instance = new VedioDataModule();
        }
        return instance;
    }

    public VedioDataModule() {
    }

    public VedioDataModule(String heading, String desc, String totalVedio, String completedVedio, String time) {
        this.heading = heading;
        this.desc = desc;
        this.totalVedio = totalVedio;
        this.completedVedio = completedVedio;
        this.time = time;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }


    public String getModuleIcons() {
        return moduleIcons;
    }

    public void setModuleIcons(String moduleIcons) {
        this.moduleIcons = moduleIcons;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTotalVedio() {
        return totalVedio;
    }

    public void setTotalVedio(String totalVedio) {
        this.totalVedio = totalVedio;
    }

    public String getCompletedVedio() {
        return completedVedio;
    }

    public void setCompletedVedio(String completedVedio) {
        this.completedVedio = completedVedio;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
