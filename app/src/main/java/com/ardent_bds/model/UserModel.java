package com.ardent_bds.model;

public class UserModel {

    private static UserModel instance = null;
    private String userId;
    private String userName;
    private String userEmailId;
    private String userPassword;
    private String userMobileNo;
    private String userProfilePic;
    private int userAccessLevel;
    private String userState;
    private String userAcadamicYear;
    private String userCollegeName;
    private boolean userOnlineStatus;
    private String userLoggedInChannel;
    private String userStatus;
    private String userlastLoginTime;

    public static UserModel getInstance() {
        if (instance == null) {
            instance = new UserModel();
        }
        return instance;
    }

    public String getUserLoggedInChannel() {
        return userLoggedInChannel;
    }

    public void setUserLoggedInChannel(String userLoggedInChannel) {
        this.userLoggedInChannel = userLoggedInChannel;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserlastLoginTime() {
        return userlastLoginTime;
    }

    public void setUserlastLoginTime(String userlastLoginTime) {
        this.userlastLoginTime = userlastLoginTime;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }

    public String getUserProfilePic() {
        return userProfilePic;
    }

    public void setUserProfilePic(String userProfilePic) {
        this.userProfilePic = userProfilePic;
    }

    public int getUserAccessLevel() {
        return userAccessLevel;
    }

    public void setUserAccessLevel(int userAccessLevel) {
        this.userAccessLevel = userAccessLevel;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public String getUserAcadamicYear() {
        return userAcadamicYear;
    }

    public void setUserAcadamicYear(String userAcadamicYear) {
        this.userAcadamicYear = userAcadamicYear;
    }

    public String getUserCollegeName() {
        return userCollegeName;
    }

    public void setUserCollegeName(String userCollegeName) {
        this.userCollegeName = userCollegeName;
    }

    public boolean isUserOnlineStatus() {
        return userOnlineStatus;
    }

    public void setUserOnlineStatus(boolean userOnlineStatus) {
        this.userOnlineStatus = userOnlineStatus;
    }

}
