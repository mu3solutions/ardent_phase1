package com.ardent_bds.model;

public class McqModuleData {
    private int moduleId;
    private String moduleName;
    private String moduleIcon;
    private int totalModule;
    private int completedModule;

    private static McqModuleData instance = null;

    public static McqModuleData getInstance() {
        if (instance == null) {
            instance = new McqModuleData();
        }
        return instance;
    }

    public McqModuleData() {
    }

    public McqModuleData(int moduleId, String moduleName, String moduleIcon, int totalModule, int completedModule) {
        this.moduleId = moduleId;
        this.moduleName = moduleName;
        this.moduleIcon = moduleIcon;
        this.totalModule = totalModule;
        this.completedModule = completedModule;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleIcon() {
        return moduleIcon;
    }

    public void setModuleIcon(String moduleIcon) {
        this.moduleIcon = moduleIcon;
    }

    public int getTotalModule() {
        return totalModule;
    }

    public void setTotalModule(int totalModule) {
        this.totalModule = totalModule;
    }

    public int getCompletedModule() {
        return completedModule;
    }

    public void setCompletedModule(int completedModule) {
        this.completedModule = completedModule;
    }

    public static void setInstance(McqModuleData instance) {
        McqModuleData.instance = instance;
    }
}
