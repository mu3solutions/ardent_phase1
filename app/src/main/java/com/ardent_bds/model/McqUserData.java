package com.ardent_bds.model;

import java.util.ArrayList;

public class McqUserData {
    private int moduleId;
    private int childId;
    private String childTestScore;
    private String childTestScorePercentage;
    private String time;
    private String status;
    private String qnSelectedAnswer;

    private static ArrayList<McqUserData> instance = null;

    public static void setInstance(ArrayList<McqUserData> instance) {
        McqUserData.instance = instance;
    }

    public static ArrayList<McqUserData> getInstance() {
        if (instance == null) {
            instance = new ArrayList<McqUserData>();
        }
        return instance;
    }
    public static void setDataInstance(ArrayList<McqUserData> instance) {
        McqUserData.instance = instance;
    }

    public static ArrayList<McqUserData> getDataInstance() {
        if (instance == null) {
            instance = new ArrayList<McqUserData>();
        }
        return instance;
    }

    public McqUserData() {
    }

    public McqUserData(int moduleId, int childId, String childTestScore, String childTestScorePercentage, String time, String status, String qnSelectedAnswer) {
        this.moduleId = moduleId;
        this.childId = childId;
        this.childTestScore = childTestScore;
        this.childTestScorePercentage = childTestScorePercentage;
        this.time = time;
        this.status = status;
        this.qnSelectedAnswer = qnSelectedAnswer;
    }

    public int getModuleId() {
        return moduleId;
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public String getChildTestScore() {
        return childTestScore;
    }

    public void setChildTestScore(String childTestScore) {
        this.childTestScore = childTestScore;
    }

    public String getChildTestScorePercentage() {
        return childTestScorePercentage;
    }

    public void setChildTestScorePercentage(String childTestScorePercentage) {
        this.childTestScorePercentage = childTestScorePercentage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQnSelectedAnswer() {
        return qnSelectedAnswer;
    }

    public void setQnSelectedAnswer(String qnSelectedAnswer) {
        this.qnSelectedAnswer = qnSelectedAnswer;
    }
}
