package com.ardent_bds;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.ardent_bds.constants.Constants;
import com.ardent_bds.interfaces.IOnBackPressed;
import com.ardent_bds.model.VedioDataModule;
import com.ardent_bds.model.VedioSubDataModule;
import com.ardent_bds.project_utils.HttpPostHandlerVideoUrl;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.VedioUrlModule;

import java.util.ArrayList;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class AllVideosFragment extends Fragment implements IOnBackPressed {

    String HttpPostVideoURL = Constants.BaseUrl + "/ArdentDB/getVideoFromChildId.php";
    String HttpPostVideoURL1 = Constants.BaseUrl + "/ArdentDB/vedioUserCompletedUpdate.php";

    ArrayList<VedioUrlModule> vedioUrlModulesArrayList = new ArrayList<>();

    //MxVideoPlayerWidget video_player;

    JCVideoPlayerStandard jcVideoPlayerStandard;

    TextView video_description, tv_header_videos;
    VedioUrlModule vedioUrlModule;
    private ProgressDialog progress_bar_grandTest;
    VedioDataModule vedioDataModule = VedioDataModule.getInstance();
    Button completeButton;
    private String lastTime;

    int value;
    String orientation;

    public AllVideosFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View rootVideosViews = inflater.inflate(R.layout.fragment_tb_videos, container, false);
        jcVideoPlayerStandard = (JCVideoPlayerStandard) rootVideosViews.findViewById(R.id.videoplayer);
        completeButton = rootVideosViews.findViewById(R.id.mark_completed);
        video_description = (TextView) rootVideosViews.findViewById(R.id.video_description);
        vedioUrlModule = VedioUrlModule.getInstance();
        tv_header_videos = rootVideosViews.findViewById(R.id.tv_header_videos);


        /*jcVideoPlayerStandard.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (getResources().getConfiguration().orientation == 0) {
                    View decorView = getActivity().getWindow().getDecorView();
                    int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN;
                    decorView.setSystemUiVisibility(uiOptions);
                }
                return false;
            }
        });*/

        if (VedioSubDataModule.getInstance().getStatus() == 1) {
            completeButton.setText("Marked");
        }


        completeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (completeButton.getText().equals("Marked")) {

                } else {
                    updateVedioCompleted();
                    completeButton.setText("Marked");
                }
            }
        });

        jcVideoPlayerStandard.tinyBackImageView.setVisibility(View.GONE);
        jcVideoPlayerStandard.battery_level.setVisibility(View.GONE);

        ConnectivityManager ConnectionManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getVideoOfSelectedChildIDData();
        } else {
            Toast.makeText(getActivity(), "Network Not Available", Toast.LENGTH_LONG).show();
        }


        return rootVideosViews;
    }

    private void updateVedioCompleted() {
        new GetupdateVedioCompleted(getActivity()).execute();
    }

    private void getVideoOfSelectedChildIDData() {

        new GetVideoOfSelectedChildIdFromServer(getActivity()).execute();

    }

    @Override
    public boolean OnBackPressed() {
        jcVideoPlayerStandard.resetProgressAndTime();
        return false;
    }


    public class GetVideoOfSelectedChildIdFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetVideoOfSelectedChildIdFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_bar_grandTest = new ProgressDialog(getActivity());
            progress_bar_grandTest.setMessage("Please wait...");
            progress_bar_grandTest.setCancelable(false);
            progress_bar_grandTest.setIndeterminate(true);
            progress_bar_grandTest.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {


            HttpPostHandlerVideoUrl httpPostHandlerVideoUrl = new HttpPostHandlerVideoUrl();
            String responseFromDB = httpPostHandlerVideoUrl.makeServicePostVideoUrlCall(HttpPostVideoURL);
            vedioUrlModulesArrayList.clear();
            if (responseFromDB != null) {
                Log.e("Sub module data ", "Submodule data responseFromDB: " + responseFromDB);
                vedioUrlModulesArrayList = BaseActivity.getVideoListFromChildID(responseFromDB);
            }


            return null;
        }

        protected void onPostExecute(Void result) {
            progress_bar_grandTest.dismiss();
            if (vedioUrlModulesArrayList.size() > 0) {

                //tv_header_videos.setText("" + vedioUrlModulesArrayList.get(0).getModuleName());
                tv_header_videos.setText("" + VedioSubDataModule.getInstance().getSelectedModuleName());
                Log.i("videos list ", "" + vedioUrlModulesArrayList.get(0).getVideoUrl());
             /*   jcVideoPlayerStandard.setUp(vedioUrlModulesArrayList.get(0).getVideoUrl()
                        , JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, "");
                jcVideoPlayerStandard.setAllControlsVisible(0, 0, 0, 0, 0, 0, 0);
                video_description.setText("" + vedioUrlModulesArrayList.get(0).getVideoDescription());*/


                jcVideoPlayerStandard.setUp(vedioUrlModulesArrayList.get(0).getVideoUrl(), JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, "");
                jcVideoPlayerStandard.setAllControlsVisible(1, 1, 1, 1, R.drawable.dummy_user, R.drawable.dummy_user, 1);
                video_description.setText("" + vedioUrlModulesArrayList.get(0).getVideoDescription());

                jcVideoPlayerStandard.loadingProgressBar.setVisibility(View.GONE);
                jcVideoPlayerStandard.battery_level.setVisibility(View.GONE);
            /*jcVideoPlayerStandard.thumbImageView.setImageURI(Uri.parse("http://images.google.com/images?q=tbn:HhcYCoWTN6y3IM:http://msucares.com/news/print/sgnews/sg04/images/sg041007_200.jpg"));
            jcVideoPlayerStandard.tinyBackImageView.setImageURI(Uri.parse("http://images.google.com/images?q=tbn:HhcYCoWTN6y3IM:http://msucares.com/news/print/sgnews/sg04/images/sg041007_200.jpg"));
*/

                jcVideoPlayerStandard.fullscreenButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        changeConfigurationBasedOnCurrentConfiguration();
                    }
                });
                jcVideoPlayerStandard.startButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        jcVideoPlayerStandard.startVideo();
                    }
                });
                jcVideoPlayerStandard.currentTimeTextView.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });


            }
        }
    }

    public class GetupdateVedioCompleted extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetupdateVedioCompleted(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {


            HttpPostHandlerVideoUrl httpPostHandlerVideoUrl = new HttpPostHandlerVideoUrl();
            String responseFromDB = httpPostHandlerVideoUrl.makeServiceUpdateCompleteStatus(HttpPostVideoURL1);
            Log.e("Sub module data ", "Submodule data responseFromDB: " + responseFromDB);

            vedioUrlModulesArrayList.clear();


            return null;
        }

        protected void onPostExecute(Void result) {
        }
    }

    private void changeConfigurationBasedOnCurrentConfiguration() {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            lastTime = jcVideoPlayerStandard.currentTimeTextView.getText().toString();
            //jcVideoPlayerStandard.progressBar.setProgress(Integer.parseInt(lastTime));
            jcVideoPlayerStandard.startWindowFullscreen();
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

            Log.i("Last Time", "" + lastTime);

        } else {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        }

    }

    private void updateLayout(boolean isLandscape) {
        if (isLandscape) {
            video_description.setVisibility(View.GONE);
            tv_header_videos.setVisibility(View.GONE);
            //jcVideoPlayerStandard.setPadding(0,5,0,0);
            LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 0);
            jcVideoPlayerStandard.setLayoutParams(ll);
        } else {
            video_description.setVisibility(View.VISIBLE);
            tv_header_videos.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, (float) 0.5);
            jcVideoPlayerStandard.setLayoutParams(ll);

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        jcVideoPlayerStandard.onStatePause();
        JCVideoPlayer.releaseAllVideos();
    }

    @Override
    public void onStop() {
        super.onStop();
        JCVideoPlayer.releaseAllVideos();
        JCVideoPlayer.backPress();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateLayout(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}