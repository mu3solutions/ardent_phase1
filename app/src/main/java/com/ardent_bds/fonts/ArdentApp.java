package com.ardent_bds.fonts;

import android.app.Application;

public class ArdentApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CustomFontsOverride.setDefaultFont(this, "DEFAULT", "fonts/rubik_regular.ttf");
        CustomFontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/rubik_regular.ttf");
        CustomFontsOverride.setDefaultFont(this, "SERIF", "fonts/rubik_regular.ttf");
        CustomFontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/rubik_regular.ttf");
    }
}

