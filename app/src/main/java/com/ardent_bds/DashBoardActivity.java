package com.ardent_bds;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.ardent_bds.adapters.DashboardTopSliderViewPagerAdapter;
import com.ardent_bds.adapters.StatusRecyclerAdapter;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.StatusDataModel;
import com.ardent_bds.model.UserModel;
import com.ardent_bds.project_utils.HttpHandler;
import com.ardent_bds.project_utils.HttpPostHandlerSubModuleData;
import com.ardent_bds.ui.BaseActivity;
import com.ardent_bds.ui.LiveTest.GrandTestLandingActivity;
import com.ardent_bds.ui.MainActivity;
import com.ardent_bds.ui.gallery.AboutUsActivity;
import com.ardent_bds.ui.gallery.AllVideosSlidesNotesActivity;
import com.ardent_bds.ui.gallery.AllViedosSlidesFragment;
import com.ardent_bds.ui.gallery.ContactUsActivity;
import com.ardent_bds.ui.gallery.PaymentHistory;
import com.ardent_bds.ui.gallery.SubscriptionActivity;
import com.ardent_bds.ui.home.GrantTestDataModel;
import com.ardent_bds.ui.home.HomeViewModel;
import com.ardent_bds.ui.home.SuggestedRecyclerAdapter;
import com.ardent_bds.ui.mcq_of_day.Mcq;
import com.ardent_bds.ui.mcq_of_day.QuestionDataModel;
import com.ardent_bds.ui.mcq_test_module.MCQTest;
import com.bumptech.glide.Glide;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class DashBoardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    MeowBottomNavigation bottomNavigation;

    AllViedosSlidesFragment allViedosSlidesFragment;
    Fragment selectedFragment;

    ImageView menu;
    DrawerLayout drawer;


    TextView userName, userEmailId;
    ImageView userImage;

    ArrayList<QuestionDataModel> questionsDataModules;
    private HomeViewModel homeViewModel;
    ViewPager viewPager;
    TabLayout tabLayout;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    ShimmerFrameLayout shimmerFrameLayout;
    TextView tv_questionHint;
    TextView username, tv_grant_test_name, expires_on, total_questions_time,usernameDashbaord;
    UserModel userModel;
    RecyclerView recyclerView;
    RecyclerView recycler_view_suggested_videos;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager GTlayoutManager;
    RecyclerView.Adapter recyclerAdapter;
    SuggestedRecyclerAdapter suggestedRecyclerAdapter;

    LinearLayout proceed_tv;
    ImageView grand_test_icon;
    ArrayList<StatusDataModel> models;
    String sliderImageList;
    StatusDataModel statusDataModel;

    String[] slideImageUrlList = null;
    ArrayList<StatusDataModel> statusDataModelArrayList;
    ArrayList<GrantTestDataModel> grantTestDataModelList;
    String HttpGetURL = Constants.BaseUrl + "/ArdentDB/getDashboardData.php";
    String HttpGetURL_Grant_Test = Constants.BaseUrl + "/ArdentDB/suggested_test.php";
    String HttpGetUrlQuestion = Constants.BaseUrl + "/ArdentDB/getAllQuestions.php";


    String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_slider_screen);

        drawer = findViewById(R.id.drawer_layout);
        menu = findViewById(R.id.menu);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        userModel = UserModel.getInstance();

        View nav = navigationView.getHeaderView(0);


        userName = nav.findViewById(R.id.userName);
        userEmailId = nav.findViewById(R.id.userEmailId);
        userImage = nav.findViewById(R.id.userImage);


        userName.setText("" + userModel.getUserName());
        userEmailId.setText("" + userModel.getUserEmailId());

      /*  if (userModel.getUserProfilePic().equalsIgnoreCase("")) {
        } else {
            userImage.setImageURI(Uri.parse(userModel.getUserProfilePic()));
            //userImage.setImageBitmap(BitmapFactory.decodeFile(userModel.getUserProfilePic()));
            Uri userProfilePic = Uri.parse(userModel.getUserProfilePic());
            if (userProfilePic != null) {
                new BaseActivity.ImageLoadTask(userProfilePic.toString(), userImage).execute();
            }
        }*/


        if (userModel.getUserProfilePic().equalsIgnoreCase("")) {
        } else {
            Glide.with(DashBoardActivity.this)
                    .load(userModel.getUserProfilePic())
                    .into(userImage);
        }

        tv_questionHint = findViewById(R.id.tv_questionHint);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT, true);
            }
        });


        bottomNavigation = findViewById(R.id.bottom_nav);
        bottomViewNavigation();

        //dashboard content

        userModel = UserModel.getInstance();
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);

        usernameDashbaord = findViewById(R.id.usename);

        viewPager = findViewById(R.id.photos_viewpager);
        sliderDotspanel = (LinearLayout) findViewById(R.id.SliderDots);
        recyclerView = findViewById(R.id.recycler_view_nuggets);
        proceed_tv = findViewById(R.id.tv_proceed);
        recycler_view_suggested_videos = findViewById(R.id.recycler_view_suggested_videos);

        tv_grant_test_name = findViewById(R.id.tv_grant_test_name);
        expires_on = findViewById(R.id.expires_on);
        total_questions_time = findViewById(R.id.total_questions_time);
        grand_test_icon = findViewById(R.id.grand_test_icon);

        //usernameDashbaord.setText("" + Html.fromHtml("<font color=##676767>Welcome,</font>") + userModel.getUserName());


        String name = getColoredSpanned("Welcome,", "#000000");
        String surName = getColoredSpanned(userModel.getUserName(),"##ff6201");

        usernameDashbaord.setText(Html.fromHtml(name+" "+surName));

        //getUserVisibleHint();
        models = new ArrayList<>();


        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getData();
            getGrantTestData();
            getAllMCQQuestions();
        } else {
            Toast.makeText(DashBoardActivity.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


        //insertStatusData();


        Log.i("TAG in SlideMenu screen", "" + TAG);

        proceed_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(0, 0);
                startActivity(new Intent(DashBoardActivity.this, Mcq.class));
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(DashBoardActivity.this, R.drawable.non_active_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(DashBoardActivity.this, R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void getGrantTestData() {

        new GetGrantTestData(DashBoardActivity.this).execute();
    }

    private void getData() {

        new GetAllImages(DashBoardActivity.this).execute();
    }


    public class GetAllImages extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllImages(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerSubModuleData httpPostHandler = new HttpPostHandlerSubModuleData();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL);

            if(responseFromDB!=null) {
                Log.e("Sub module data ", "Submodule data responseFromDB: " + responseFromDB);
                statusDataModelArrayList = BaseActivity.getDashboardImagesData(responseFromDB);
            }
            //sliderImageList = statusDataModel.getSliderImageList();

            return null;
        }

        protected void onPostExecute(Void result) {


            if (statusDataModelArrayList.size() > 0) {

                recyclerView.setHasFixedSize(true);

                layoutManager = new LinearLayoutManager(DashBoardActivity.this, LinearLayoutManager.HORIZONTAL, false);
                recyclerView.setLayoutManager(layoutManager);


                recycler_view_suggested_videos.setHasFixedSize(true);
                GTlayoutManager = new LinearLayoutManager(DashBoardActivity.this, LinearLayoutManager.HORIZONTAL, false);
                recycler_view_suggested_videos.setLayoutManager(GTlayoutManager);


                String[] slideUrlArray1 = null;
                String[] slideUrlArray2 = null;

                slideUrlArray1 = statusDataModelArrayList.get(0).getImage().split(",");
                slideUrlArray2 = statusDataModelArrayList.get(0).getName().split(",");
                slideImageUrlList = statusDataModelArrayList.get(0).getSliderImageList().split(",");


                String[] grandTestArrayImages = null;
                String[] grandTestArrayNames = null;
                String[] SuggestedVideosArrayNames = null;
                String Min5VideosNuggets = null;
                String Min5VideosNuggetsTxt = null;

                grandTestArrayImages = statusDataModelArrayList.get(0).getDb_suggested_videos().split(",");
                grandTestArrayNames = statusDataModelArrayList.get(0).getDb_suggested_videos_txt().split(",");
                SuggestedVideosArrayNames = statusDataModelArrayList.get(0).getNug_concepts_video().split(",");
                Min5VideosNuggets = statusDataModelArrayList.get(0).getNug_5_mins_video();
                Min5VideosNuggetsTxt = statusDataModelArrayList.get(0).getNug_5_mins_txt();


                recyclerAdapter = new StatusRecyclerAdapter(DashBoardActivity.this, slideUrlArray1, slideUrlArray2, statusDataModelArrayList, Min5VideosNuggets,Min5VideosNuggetsTxt);
                recyclerView.setAdapter(recyclerAdapter);


                DashboardTopSliderViewPagerAdapter viewPagerAdapter = new DashboardTopSliderViewPagerAdapter(DashBoardActivity.this, slideImageUrlList);
                viewPager.setAdapter(viewPagerAdapter);


                //sugestted videos

                suggestedRecyclerAdapter = new SuggestedRecyclerAdapter(DashBoardActivity.this, grandTestArrayImages, grandTestArrayNames, SuggestedVideosArrayNames,Min5VideosNuggetsTxt);
                recycler_view_suggested_videos.setAdapter(suggestedRecyclerAdapter);


                dotscount = viewPagerAdapter.getCount();
                dots = new ImageView[dotscount];
                for (int i = 0; i < dotscount; i++) {
                    dots[i] = new ImageView(DashBoardActivity.this);
                    dots[i].setImageDrawable(ContextCompat.getDrawable(DashBoardActivity.this, R.drawable.non_active_dot));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(8, 0, 8, 0);
                    sliderDotspanel.addView(dots[i], params);

                }

                dots[0].setImageDrawable(ContextCompat.getDrawable(DashBoardActivity.this, R.drawable.active_dot));


            }
        }
    }

    public class GetGrantTestData extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetGrantTestData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerSubModuleData httpPostHandler = new HttpPostHandlerSubModuleData();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL_Grant_Test);
            Log.e("Grant Test>>", "Grant Test data responseFromDBBBBBBBBBB123456: " + responseFromDB);
            if(responseFromDB!=null) {
                grantTestDataModelList = BaseActivity.getDashboardGrantTestData(responseFromDB);
            }
            //sliderImageList = statusDataModel.getSliderImageList();

            return null;
        }

        protected void onPostExecute(Void result) {

            if (grantTestDataModelList.size() > 0) {

                Glide.with(getApplicationContext())
                        .load(grantTestDataModelList.get(0).getTesticon())
                        .into(grand_test_icon);
                tv_grant_test_name.setText("" + grantTestDataModelList.get(0).getTestName());
                expires_on.setText("Expires On " + grantTestDataModelList.get(0).getTestexpiryDate());
                total_questions_time.setText("" + grantTestDataModelList.get(0).getTestTotalQuestions() + "Questions  | " + grantTestDataModelList.get(0).getTestTotalTiming() + " mins");

            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            //  super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_payment) {
            overridePendingTransition(0, 0);
            Intent i1 = new Intent(this, PaymentHistory.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i1.putExtras(bundle);
            startActivity(i1);

        } else if (id == R.id.nav_subscribe) {
            overridePendingTransition(0, 0);
            //startActivity(new Intent(this, SubscriptionActivity.class));
            Intent i2 = new Intent(this, SubscriptionActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i2.putExtras(bundle);
            startActivity(i2);

        } else if (id == R.id.nav_bookmark) {

        } else if (id == R.id.nav_share_app) {
            shareApp();

        } else if (id == R.id.nav_help) {
            overridePendingTransition(0, 0);
            //startActivity(new Intent(this, ContactUsActivity.class));
            Intent i3 = new Intent(this, ContactUsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i3.putExtras(bundle);
            startActivity(i3);


        } else if (id == R.id.nav_menu_about_us) {
            //startActivity(new Intent(this, AboutUsActivity.class));
            overridePendingTransition(0, 0);
            Intent i4 = new Intent(this, AboutUsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("screen_name", TAG);
            i4.putExtras(bundle);
            startActivity(i4);


        } else if (id == R.id.nav_menu_settings) {

        } else if (id == R.id.nav_menu_logout) {

            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to Logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(new Intent(DashBoardActivity.this, MainActivity.class));
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    private void shareApp() {

        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "ArDent BDS Application");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + "com.ardent_bds" + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public void bottomViewNavigation() {
        bottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.ic_account_balance_black_24dp));
        bottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.ic_baseline_library_add_check_24));
        bottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.ic_assignment_black_24dp));
        bottomNavigation.add(new MeowBottomNavigation.Model(4, R.drawable.ic_live_tv_black_24dp));

        bottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                /* Toast.makeText(NavigationDrawerActivity.this, "clicked item : " + item.getId(), Toast.LENGTH_SHORT).show();*/
                if (item.getId() == 1) {

                    //SlideMenu fragment = new SlideMenu();
                    /*startActivity(new Intent(SlideMenu.this(), SlideMenu.class));*/
                    Intent i1 = new Intent(DashBoardActivity.this, DashBoardActivity.class);
                    i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i1);
                    overridePendingTransition(0, 0);
                } else if (item.getId() == 2) {

                    //startActivity(new Intent(SlideMenu.this(), MCQTest.class));
                    Intent i2 = new Intent(DashBoardActivity.this, MCQTest.class);
                    i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i2);
                    overridePendingTransition(0, 0);
                } else if (item.getId() == 3) {

                    //startActivity(new Intent(SlideMenu.this(), GrandTestLandingActivity.class));
                    Intent i3 = new Intent(DashBoardActivity.this, GrandTestLandingActivity.class);
                    i3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i3);
                    overridePendingTransition(0, 0);
                } else if (item.getId() == 4) {

                    Intent i4 = new Intent(DashBoardActivity.this, AllVideosSlidesNotesActivity.class);
                    i4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i4);
                    overridePendingTransition(0, 0);
                }
            }
        });

        bottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {

                String name;
                switch (item.getId()) {
                    case 1:
                        name = "HOME";
                        break;
                    case 2:
                        name = "EXPLORE";
                        break;
                    case 3:
                        name = "MESSAGE";
                        break;
                    case 4:
                        name = "VIDEOS";
                        break;
                    default:
                        name = "";
                }
            }
        });

        bottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {
                /*  Toast.makeText(NavigationDrawerActivity.this, "reselected item : " + item.getId(), Toast.LENGTH_SHORT).show();*/
            }
        });


        //bottomNavigation.setCount(1, "115");
        bottomNavigation.show(1, true);
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }


    protected void onDestroy() {
        super.onDestroy();
    }

    private void getAllMCQQuestions() {

        new GetMCQQuestionsForTheDayFromServer(DashBoardActivity.this).execute();
    }

    public class GetMCQQuestionsForTheDayFromServer extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetMCQQuestionsForTheDayFromServer(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {


            HttpHandler httpPostHandler = new HttpHandler();
            String responseFromDB = httpPostHandler.makeServiceCall(HttpGetUrlQuestion);
            Log.e("Questions data ", "Question responseFromDB: " + responseFromDB);

            if(responseFromDB!=null) {
                questionsDataModules = BaseActivity.getMCQQuestions(responseFromDB);
            }

            return null;
        }

        protected void onPostExecute(Void result) {


            if (questionsDataModules.size() > 0) {
                tv_questionHint.setText(questionsDataModules.get(0).getMcqAnswerDesc().substring(0, 35) + " ?");
            }


        }
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }
}
