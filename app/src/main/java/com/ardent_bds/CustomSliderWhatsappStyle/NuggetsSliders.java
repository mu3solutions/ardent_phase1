package com.ardent_bds.CustomSliderWhatsappStyle;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.ardent_bds.R;
import com.ardent_bds.DashBoardActivity;
import com.ardent_bds.constants.Constants;
import com.ardent_bds.model.StatusDataModel;
import com.ardent_bds.project_utils.HttpPostHandlerSubModuleData;
import com.ardent_bds.ui.BaseActivity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class NuggetsSliders extends AppCompatActivity implements StoriesProgressView.StoriesListener {


    private StoriesProgressView storiesProgressView;
    private ImageView image;
    ArrayList<StatusDataModel> statusDataModelArrayList;
    private int counter = 0;
    private int PROGRESS_COUNT;
    Button btn_bottom_nuggets_slider;
    long pressTime = 0L;
    long limit = 500L;
    String HttpGetURL = Constants.BaseUrl + "/ArdentDB/getDashboardData.php";
    ArrayList<String> allDrawables = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.custom_whatsapp_slider);
        storiesProgressView = (StoriesProgressView) findViewById(R.id.stories);
        btn_bottom_nuggets_slider = findViewById(R.id.btn_bottom_nuggets_slider);

        statusDataModelArrayList = new ArrayList<>();

        image = (ImageView) findViewById(R.id.image);
        //image.setImageResource(resources[counter]);


        View.OnTouchListener onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        pressTime = System.currentTimeMillis();
                        storiesProgressView.pause();
                        return false;
                    case MotionEvent.ACTION_UP:
                        long now = System.currentTimeMillis();
                        storiesProgressView.resume();
                        return limit < now - pressTime;
                }
                return false;
            }
        };

        // bind reverse view
        View reverse = findViewById(R.id.reverse);
        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.reverse();
            }
        });
        reverse.setOnTouchListener(onTouchListener);
        // bind skip view
        View skip = findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storiesProgressView.skip();
            }
        });
        skip.setOnTouchListener(onTouchListener);

        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            getAllImageNuggetsData();
        }
        else{
            Toast.makeText(NuggetsSliders.this, "Network Not Available", Toast.LENGTH_LONG).show();
        }


        btn_bottom_nuggets_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // startActivity(new Intent(NuggetsSliders.this, RegisterForCourse.class));
            }
        });

    }

    @Override
    public void onNext() {
        //  image.setImageResource(resources[++counter]);

        //image.setImageURI(Uri.parse(allDrawables.get(++counter)));
        Glide.with(this)
                .load(allDrawables.get(++counter))
                .into(image);

       /* if (counter % 2 == 0) {
            btn_bottom_nuggets_slider.setText("Enroll now");
        } else {
            btn_bottom_nuggets_slider.setText("Register now");
        }*/
    }

    @Override
    public void onPrev() {
        if ((counter - 1) < 0) return;
        //image.setImageResource(resources[--counter]);
        //image.setImageURI(Uri.parse(allDrawables.get(--counter)));

        Glide.with(this)
                .load(allDrawables.get(--counter))
                .into(image);

/*
        if (counter % 2 == 0) {
            btn_bottom_nuggets_slider.setText("Enroll now");
        } else {
            btn_bottom_nuggets_slider.setText("Register now");
        }*/
    }

    @Override
    public void onComplete() {
        storiesProgressView.destroy();
        overridePendingTransition(0,0);
        startActivity(new Intent(this, DashBoardActivity.class));
    }

    @Override
    protected void onDestroy() {
        storiesProgressView.destroy();
        super.onDestroy();
    }


    private void getAllImageNuggetsData() {

        new GetAllImages(this).execute();
    }


    public class GetAllImages extends AsyncTask<Void, Void, Void> {
        public Context context;


        public GetAllImages(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpPostHandlerSubModuleData httpPostHandler = new HttpPostHandlerSubModuleData();
            String responseFromDB = httpPostHandler.makeServicePostCall(HttpGetURL);

            if(responseFromDB!=null) {
                Log.e("Sub module data ", "Submodule data responseFromDB: " + responseFromDB);
                statusDataModelArrayList = BaseActivity.getDashboardImagesData(responseFromDB);
            }
            return null;
        }

        @SuppressLint("StaticFieldLeak")
        protected void onPostExecute(Void result) {
            if (statusDataModelArrayList.size() > 0) {
                getImageFromServer();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public void getImageFromServer() {
        int tot = statusDataModelArrayList.get(0).getNuggetsSliderImageList().split(",").length;
        PROGRESS_COUNT = tot;
        for (int i = 0; i < tot; i++) {
            final String urls = statusDataModelArrayList.get(0).getNuggetsSliderImageList().split(",")[i];
            allDrawables.add(urls);
            // btn_bottom_nuggets_slider.setText("Enroll now");
        }


        storiesProgressView.setStoriesCount(PROGRESS_COUNT);
        storiesProgressView.setStoryDuration(3000L);
        storiesProgressView.setStoriesListener(this);
        counter = 0;
        storiesProgressView.startStories(counter);


        Glide.with(this)
                .load(allDrawables.get(counter))
                .into(image);
    }


}
