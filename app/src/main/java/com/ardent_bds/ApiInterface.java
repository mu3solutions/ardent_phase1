/*
package com.example.webcaller;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {



    String BASEURL = "http://test6.mu3.technology/api/";
    String BASEURL1 = "http://test6.mu3.technology/api/";
    String API_ROUTE = "posts";

    @Headers({
            "Content-type: application/json"
    })

    @POST("push_user_token.php")
*/
/*
    Call<PostModel> sendPosts(@Body PostModel posts);
*//*


    @FormUrlEncoded
    Call<PostModel> sendPosts(@Field("user_id") String title,@Field("user_token") String userId);

    @Headers({
            "Content-type: application/json"
    })
    @POST("send_notification.php")
    @FormUrlEncoded
    Call<StartTest> startTest(@Field("start_test") String start_test);

}
*/


package com.ardent_bds;

import com.ardent_bds.constants.Constants;

import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {


    String API_ROUTE = "posts";

    String BaseUrl = "http://test9.mu3.technology/api/";

    @Headers({
            "Content-type: application/json"
    })

    @POST("ArdentDB/getUpdatedUserDetails.php")
    Call<SubscriptionModel> getSubscriptionData(@Query("user_id") String title);



}

